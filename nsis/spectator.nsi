!define PROGRAM_NAME "Spectator"
!define COMAPANY_NAME "CZ.NIC"
!define /file VERSION "../VERSION"
!define QUADVERSION "${VERSION}.0.0"

!include "MUI2.nsh"
!include "FileFunc.nsh"

Name "${PROGRAM_NAME}" "${PROGRAM_NAME} ${VERSION}"

# installer name
OutFile "spectator-${VERSION}-install.exe"

# set desktop as install directory
installDir "$PROGRAMFILES\${COMAPANY_NAME}\${PROGRAM_NAME}"

VIAddVersionKey "ProductName" "${PROGRAM_NAME} ${VERSION}"
VIAddVersionKey "CompanyName" "CZ.NIC, z. s. p. o."
VIAddVersionKey "FileDescription" "install the ${PROGRAM_NAME} ${VERSION}"
VIAddVersionKey "LegalCopyright" "Copyright 2014 - 2016, CZ.NIC, z. s. p. o."
VIAddVersionKey "FileVersion" "${QUADVERSION}"
VIAddVersionKey "ProductVersion" "${QUADVERSION}"
VIProductVersion "${QUADVERSION}"


!define MUI_ABORTWARNING

!define MUI_ICON "../gui/img/spectator.ico"
!define MUI_UNICON "../gui/img/spectator-gray.ico"
!insertmacro MUI_PAGE_LICENSE "../COPYING"
!insertmacro MUI_PAGE_DIRECTORY

Var StartMenuFolder

!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${COMAPANY_NAME}\${PROGRAM_NAME}"
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

# Languages
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Czech"

# default section start
Section

# define output path
SetOutPath $INSTDIR

# specify file to go in output path
File ./../gui/img/spectator.ico
File ./../gui/img/spectator-gray.ico
File ./../bin/turris-spectator.exe
File /r ./../dlls/*


# define uninstaller name
WriteUninstaller $INSTDIR\uninst.exe


!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\uninst.exe" "" "$INSTDIR\spectator-gray.ico" 0
CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${PROGRAM_NAME}.lnk" "$INSTDIR\turris-spectator.exe" "" "$INSTDIR\spectator.ico" 0
!insertmacro MUI_STARTMENU_WRITE_END

CreateShortCut "$DESKTOP\${PROGRAM_NAME}.lnk" "$INSTDIR\turris-spectator.exe" "" "$INSTDIR\spectator.ico" 0


SectionEnd


Section "Uninstall"

Delete "$INSTDIR\uninst.exe"   # delete self

RMDir /r "$INSTDIR\*" 

Delete "$DESKTOP\${PROGRAM_NAME}.lnk"
RMDir "$PROGRAMFILES\${COMAPANY_NAME}\${PROGRAM_NAME}"
RMDir "$PROGRAMFILES\${COMAPANY_NAME}"
RMDir "$INSTDIR"

!insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
SetShellVarContext current
Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
Delete "$SMPROGRAMS\$StartMenuFolder\${PROGRAM_NAME}.lnk"
RMDir "$SMPROGRAMS\$StartMenuFolder"
RMDir "$SMPROGRAMS\${COMAPANY_NAME}"

SetShellVarContext all
Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
Delete "$SMPROGRAMS\$StartMenuFolder\${PROGRAM_NAME}.lnk"
RMDir "$SMPROGRAMS\$StartMenuFolder"
RMDir "$SMPROGRAMS\${COMAPANY_NAME}"

SectionEnd
