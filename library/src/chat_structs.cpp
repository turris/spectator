/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spectator/chat_structs.h"

#include <QUrl>
#include <QUrlQuery>

Capability::Capability(const QString &url)
{
	// parse using url
	QUrl capabilityUrl(url);
	this->base = capabilityUrl.scheme() + "://" + capabilityUrl.host() + capabilityUrl.path();
	QUrlQuery urlQuery(capabilityUrl);
	this->revision = urlQuery.hasQueryItem("revision") ? urlQuery.queryItemValue("revision") : QString();
}


bool Capability::covers(const Capability &capability) const
{
	// base should always match
	if (this->base != capability.base)
		return false;

	// NULL revision means any revision
	if (capability.revision.isNull())
		return true;

	// check the revsions
	if (this->revision != capability.revision)
		return false;

	return true;
}
