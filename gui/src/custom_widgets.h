/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_CUSTOM_WIDGETS_H
#define SPECTATOR_CUSTOM_WIDGETS_H

#include <spectator/chat.h>

#include "qcustomplot.h"

namespace Ui {
	class NotificationWidget;
	class InterfaceWidget;
	class UpdaterUserListsWidget;
	class UpdaterActivitiesWidget;
}

struct InterfaceWidgetRecord {
	QVector<double> xTimeAxis;
	QVector<double> yRxAxis;
	QVector<double> yTxAxis;
};

class InterfaceWidget: public QWidget {
	Q_OBJECT

public:
	QString prefferedInterface;

	InterfaceWidget(QWidget *parent, QWidget *mainWindow);
	~InterfaceWidget();
	void setData(const QMap<QString, InterfaceWidgetRecord> &data);
	QString getCurrentInterface();
	void updateTexts();

private slots:
	void forward();
	void backward();

private:
	QMap<QString, InterfaceWidgetRecord> data;
	QList<QString> interfaces;
	int currentIndex;
	QWidget *mainWindow;

	QLabel *deviceText;
	QLabel *txText;
	QLabel *rxText;
	QLabel *deviceLabel;
	QLabel *txLabel;
	QLabel *rxLabel;
	QPushButton *backwardButton;
	QPushButton *forwardButton;
	QGridLayout *innerLayout;
	QWidget *buttonWidget;
	QHBoxLayout *buttonLayout;
	QCustomPlot *interfacePlot;

	void setIndex(int index);
};

class UpdaterUserListsWidget: public QWidget
{
	Q_OBJECT

public:
	UpdaterUserListsWidget(QWidget *parent, QWidget *mainWindow);
	~UpdaterUserListsWidget();

	void setUserListData(const QList<PkgListRecord> &data);
	int getLenght();
	QString *getName(int index);
	QCheckBox *getCheckbox(int index);
	void updateTexts();

	bool isInEditMode() { return this->editMode; }

private:
	QList<QCheckBox *> checkboxes;
	QList<QLabel *> titles;
	QList<QLabel *> descriptions;
	QList<QString *> names;
	bool editMode;
	QWidget *mainWindow;

	QGridLayout *mainLayout;
	QSpacerItem *hSpacer;
	QSpacerItem *vSpacer;
	QPushButton *setListsButton;

	void disposeUserListsWidgets();

private slots:
	void enterEditMode();
	void exitEditMode();
};

class UpdaterActivitiesWidget : public QWidget
{

public:
	void setActivitiesData(const QList<UpdaterActivityRecord> &data);
	UpdaterActivitiesWidget(QWidget *parent);
	~UpdaterActivitiesWidget();

private:
	QList<QLabel *> labels;
	QGridLayout *mainLayout;
	QSpacerItem *spacer;

	void disposeWidgets();
};

class NotificationWidget: public QWidget
{
	Q_OBJECT

signals:
	void closedCalled(const QString &messageId);

private slots:
	void callClose();

public:
	enum SEVERITIES {
		S_NEWS,
		S_ERROR,
		S_RESTART,
		S_UPDATE,
		S_UNKNOWN,
	};

	QString id;

	void setEnabledChildren(bool enabled);
	void updateTexts(const QString &body);
	NotificationWidget(
		QWidget *parent, QWidget *mainWindow, const QString &id, const QString &severity, const QDateTime &time,
		const QString &text = ""
	);
	~NotificationWidget();

private:
	QGroupBox *groupBox;
	QLabel *textWidget;
	QHBoxLayout *outerLayout;
	QHBoxLayout *innerLayout;
	QPushButton *closeButton;
	QPushButton *rebootButton;

	QWidget *mainWindow;
	QDateTime time;
	SEVERITIES severity;

	QString getTitle();
};

class NeighbourWidget: public QWidget
{
	Q_OBJECT

public slots:
	void markRead();

public:

	QString mac;
	QString dev;

	void markUnread();
	void setMacMapping(QHash<QString, QString> *macToNameMapping);
	void setContent(NeighbourRecord const *neighbour);
	void setBodyText(bool extra);
	void resetEditState();
	QString getAlias(const QString &macAddress);

	NeighbourWidget(QWidget *parent, QWidget *mainWindow, NeighbourRecord const *neighbour,
			QHash<QString, QString> *macToNameMapping);
	virtual ~NeighbourWidget();

signals:
	void editStatusChanged(bool toEditMode);

private slots:
	void switchMacEdit();

private:
	QLabel *textWidget;
	QLabel *appearedLabel;
	QPushButton *markReadButton;
	QPushButton *aliasEditButton;
	QLineEdit *aliasLineEdit;
	QLabel *headerLabel;
	QVBoxLayout *innerLayout;
	QHBoxLayout *headerLayout;
	QHBoxLayout *outerLayout;
	QWidget *innerWidget;
	QWidget *headerWidget;

	QWidget *mainWindow;
	QHash<QString, QString> *macToNameMapping;
	QString basicBodyText;
	QString extraBodyText;
	QString basicHeaderText;
	QString extraHeaderText;

	void setHeaderLabel(const QString &interfaceName, const QString &macAddress);
};

class SpectatorSystemTray : public QSystemTrayIcon {

public:
	SpectatorSystemTray(const QIcon &icon, QObject *parent) : QSystemTrayIcon(icon, parent) { }
	SpectatorSystemTray(QObject *parent) : QSystemTrayIcon(parent) { }

	QScreen *getScreen();

private:
	Q_DISABLE_COPY(SpectatorSystemTray)
};

#endif
