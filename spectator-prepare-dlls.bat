@ECHO OFF

REM Copy required dlls to a dlls directory so it can be packed later
REM
REM To run this script from wine use 'wine cmd < spectator-prepare-dlls.bat'
REM


IF NOT EXIST dlls MKDIR dlls

SET QT_BIN_DLLS_PATH=C:\Qt\Qt5.5.0\5.5\mingw492_32\bin
SET QT_PLUGIN_DLLS_PATH=C:\Qt\Qt5.5.0\5.5\mingw492_32\plugins
SET OPENSSL_DLLS_PATH=C:\OpenSSL\bin

FOR %f IN (Qt5Core.dll Qt5Gui.dll Qt5Network.dll Qt5PrintSupport.dll Qt5Widgets.dll Qt5Xml.dll libgcc_s_dw2-1.dll libstdc++-6.dll libwinpthread-1.dll) DO (
	REM echo "%QT_BIN_DLLS_PATH%\%f"
	XCOPY /Y "%QT_BIN_DLLS_PATH%\%f" "dlls"
)

IF NOT EXIST dlls\platforms MKDIR dlls\platforms
FOR %f IN (platforms\qwindows.dll) DO (
	REM echo "%QT_BIN_DLLS_PATH%\%f"
	XCOPY /Y "%QT_PLUGIN_DLLS_PATH%\%f" "dlls\platforms"
)

FOR %f IN (ssleay32.dll libeay32.dll msvcr120.dll) DO (
	REM echo "%OPENSSL_DLLS_PATH%\%f"
	XCOPY /Y "%OPENSSL_DLLS_PATH%\%f" "dlls"
)

REM echo "lib\turris-spectator.dll"
XCOPY /Y "lib\turris-spectator.dll" "dlls"
