/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_MAINWINDOW_H
#define SPECTATOR_MAINWINDOW_H

#include <spectator/communicator.h>

#include "settings.h"
#include "custom_widgets.h"

namespace Ui {

class MainWindow;

}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public slots:
	void toggleWindow(QSystemTrayIcon::ActivationReason);

public:
	void updateSettings();  // updates this->settings according to gui status
	void useSettings(SpectatorSettings &settings);  // load settings structure to this->settings
	bool isInSystrayMode();
	bool isInDockMode();
	void setSystrayMode(bool active);
	bool testDockMode();
	bool testSystrayMode();
	void showRunningInSystrayPopup();
	void show();
	explicit MainWindow(QWidget *parent, SpectatorSystemTray *trayIcon);
	QMenu *getMenu();
	~MainWindow();

protected:
	void contextMenuEvent(QContextMenuEvent *event) Q_DECL_OVERRIDE;
	void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
	void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;
	void changeEvent(QEvent* event) Q_DECL_OVERRIDE;

private slots:
	void storeSettingsAndQuit();
	void activateNotificationTab();
	void activateStatisticsTab();
	void activateDeviceTab();
	void activateUpdaterTab();
	void activateNeighboursTab();

	void displaySettings();
	void displayAbout();
	void displayBackupBrowseDialog();

	void updateStatusBar(Communicator::ReportType type, const QString &msg);
	void updateStatusBar(const QString &msg);
	void updateNotificationList(const QList<ParsedNotification> &list);
	void updateNethist(const QList<NethistRecord> &list);
	void updateStats(const StatsRecord &record);
	void updateSerial(const QString &serial);
	void updateUpdaterData(const UpdaterDataRecord &record);
	void updateNeighbours(const QList<NeighbourRecord> &list, bool popup=true, bool markUnread=true);
	void updateNeighboursInit(const QList<NeighbourRecord> &list);
	void discardNotification(const QString &messageId);

	void setUpdaterFinished(bool success);
	void setPasswordFinished(bool success);
	void updaterCheckFinished(bool soccess);
	void storeBackupFinished(const QByteArray &data);
	void restoreBackupFinished(const QString &newIp);

	void callPerformLoopChats();
	void callNeighbourChat();
	void callNeighbourMacEditDisable();
	void callStoreBackup();
	void callRestoreBackup();
	void callCloseNotification(const QString &messageId);
	void callSetPassword();
	void callRebootRouter();
	void callCheckUpdater();
	void callSetUpdater();

	void handleRebootingRouter();
	void handleConnectorConnected();
	void handleConnectorDisconnected();
	void handleReconnect();
	void handleSystrayNotificationMessageClicked();
	void handleSystrayNeighbourClicked();
	void handleCapatibilitiesLoaded(const QList<Capability> &capabilities);
	void handleNeighbourMenuItemToggled(bool checked);
	void handleNeighbourMacEdit(bool toEditMode);
	void handleNeighbourAdvancedViewToggled(bool advanced);
	void handleGuardListenerConnected();

private:
	void setNoNotificationsVisible(bool visible);
	void setNoNeighboursVisible(bool visible);
	void createMenus();
	SpectatorSystemTray *trayIcon;
	void updateWindowPosition();
	void reinitConnection();
	void setNotificationsEnabled(bool enabled);
	void framesSetEnabled(bool enabled);
	void initCharts();
	void setDockMode(bool active);
	void enableAllTabsAndMenus();
	void recalculateSize();
	void updateTabsAndMenus(const QList<Capability> &capabilities);
	void setTabAndMenusEnabled(int tabIndex, bool enabled);
	void updateNeighbourDevices(const QList<QString> &devices);
	void disconnect();

	QList<QAction *> tabActions;
	QMap<int, QList<Capability> > tabCapabilities;

	InterfaceWidget *interfaceWidget;
	UpdaterUserListsWidget *updaterUserList;
	UpdaterActivitiesWidget *updaterActivities;

	Ui::MainWindow *ui;
	int popupTime;
	QMenu *neighbourInterfaceMenu;
	Connector *connector;
	QTimer reconnectTimer;
	QTimer refreshTimer;
	bool systrayModeActive;
	bool dockModeActive;
	bool enableNeighbourChat;
	bool reconnectStop;

signals:
	void translationNeeded();
};


#endif
