/*
 * Spectator - a desktop app which communicates with router Turris
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_APPLICATION_H
#define SPECTATOR_APPLICATION_H

#include <QApplication>
#include <QSystemSemaphore>
#include <QSharedMemory>
#include <QLocalServer>

#include "mainwindow.h"
#include "settings.h"
#include "custom_widgets.h"

#define appSettings (qobject_cast<SpectatorApplication *>(qApp)->settings)

class SpectatorGuard {

public:
	QLocalServer listener;
	bool locked;
	bool checkAndLock();
	SpectatorGuard(const QString &username);
	~SpectatorGuard();


private:
	QSharedMemory status;
	QSystemSemaphore checkLock;
	QString username;

	const QString getServerName();

	Q_DISABLE_COPY(SpectatorGuard)
};


class SpectatorApplication Q_DECL_FINAL : public QApplication {
	Q_OBJECT

public:
	SpectatorSettings settings;
	bool proceed;
	SpectatorApplication(int& argc, char **argv, const QString &username);
	virtual ~SpectatorApplication();

	int exec();

private:
	SpectatorGuard guard;
	QPointer<SpectatorSystemTray> trayIcon;
	QPointer<MainWindow> mainWindow;
	QPointer<QTimer> trayIconTimer;
	QPointer<QTranslator> qtTranslator;
	QPointer<QTranslator> spectatorTranslator;

	bool forceSystrayMode;
	bool noSystrayMode;

	void createTrayIcon();
	void createMainWindow();
	void loadSettings();

private slots:
	void refreshTrayIcon();
	void updateTranslations();
};


#endif
