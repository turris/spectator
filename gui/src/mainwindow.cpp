/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <spectator/logging.h>
#include <spectator/chat.h>
#include <spectator/communicator.h>

#include "mainwindow.h"
#include "settings.h"
#include "settings_dialog.h"
#include "application.h"

#include "ui_mainwindow.h"


#define RECONNECT_TIMEOUT 10000  // 10s

#define TAB_IDX_NOTIFICATION 0
#define TAB_IDX_NETHIST 1
#define TAB_IDX_STATS 2
#define TAB_IDX_UPDATER 3
#define TAB_IDX_NEIGHBOURS 4

namespace {

void setTextLabel(QLabel *label, QString newText)
{
	if (newText.isEmpty()) {
		label->setText("???");
		label->setToolTip(QString());
	} else {
		label->setText(newText);
		label->setToolTip(newText);
	}
}

}

MainWindow::MainWindow(QWidget *parent, SpectatorSystemTray *trayIcon) :
	QMainWindow(parent),
	trayIcon(trayIcon),
	ui(new Ui::MainWindow),
	connector(NULL),
	enableNeighbourChat(true)
{
	// Set required capabilities
	this->tabCapabilities[TAB_IDX_NOTIFICATION] = QList<Capability>()
			<< Chats::GetNotifications::requiredCapabilities
			<< Chats::DeleteNotification::requiredCapabilities;
	this->tabCapabilities[TAB_IDX_NETHIST] = QList<Capability>()
			<< Chats::GetNethist::requiredCapabilities;
	this->tabCapabilities[TAB_IDX_STATS] = QList<Capability>()
			<< Chats::SetPassword::requiredCapabilities
			<< Chats::GetSerial::requiredCapabilities
			<< Chats::GetStats::requiredCapabilities;
	this->tabCapabilities[TAB_IDX_UPDATER] = QList<Capability>()
			<< Chats::CheckUpdater::requiredCapabilities
			<< Chats::GetUpdaterData::requiredCapabilities
			<< Chats::SetUpdater::requiredCapabilities;
	this->tabCapabilities[TAB_IDX_NEIGHBOURS] = QList<Capability>()
			<< Chats::GetNeighbours::requiredCapabilities;


	// Setup the gui
	this->ui->setupUi(this);
	this->ui->rebootDeviceButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_BrowserReload));

	// Create neighbour interface menu
	this->neighbourInterfaceMenu = new QMenu;
	this->ui->neighbourInterfacesButton->setMenu(this->neighbourInterfaceMenu);

	// Set tab action list (has to be called after createMenus)
	this->tabActions = QList<QAction *>()
			<< this->ui->actionNotifications
			<< this->ui->actionStatistics
			<< this->ui->actionDevice
			<< this->ui->actionUpdater
			<< this->ui->actionNeighbours;

	this->interfaceWidget = new InterfaceWidget(this->ui->statisticsNetworkFrame, this);

	this->updaterUserList = new UpdaterUserListsWidget(this, this);
	this->ui->updaterScrollArea->setWidget(this->updaterUserList);
	this->updaterActivities = new UpdaterActivitiesWidget(this);
	this->ui->updaterLogScrollArea->setWidget(this->updaterActivities);

	// Set refresh timer signal
	QObject::connect(&this->refreshTimer, SIGNAL(timeout()), this, SLOT(callPerformLoopChats()));
	// Set reconnect timer signal
	QObject::connect(&this->reconnectTimer, SIGNAL(timeout()), this, SLOT(handleReconnect()));

	this->initCharts();
}

void MainWindow::contextMenuEvent(QContextMenuEvent *event)
{
	this->ui->menuSpectator->exec(event->globalPos());
}

void MainWindow::changeEvent(QEvent* event)
{
	if (event->type() == QEvent::LanguageChange)
	{
		this->ui->retranslateUi(this);

		this->interfaceWidget->updateTexts();
		this->updaterUserList->updateTexts();
	}

	QMainWindow::changeEvent(event);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
	switch (event->key()) {
		case Qt::Key_Escape:
			if (this->isVisible() && this->isActiveWindow() && this->isInSystrayMode()) {
				this->toggleWindow(QSystemTrayIcon::Unknown);
			}
			// Close neighbour mac edits
			this->callNeighbourMacEditDisable();
			break;
	}
}

MainWindow::~MainWindow()
{
	delete this->connector;
	delete this->ui;
	delete this->neighbourInterfaceMenu;
	delete this->interfaceWidget;
}


void MainWindow::updateSettings()
{
	appSettings.mainTabIndex = this->ui->mainTab->currentIndex();
	appSettings.backupPath = this->ui->backupLineEdit->text();
	appSettings.inactiveLimit = this->ui->neighbourInactiveSpinBox->value();
	appSettings.neighboursAdvancedView = this->ui->neighboursAdvancedViewCheckBox->isChecked();

	if (!this->dockModeActive) {
		if (this->isVisible()) {
			appSettings.geometry = this->saveGeometry();
		}
	}

	// Store preffered inteface
	const QString &interfaceName = this->interfaceWidget->getCurrentInterface();
	if (!interfaceName.isEmpty()) {
		appSettings.prefferedInterface = interfaceName;
	}
}

void MainWindow::useSettings(SpectatorSettings &settings)
{
	this->ui->mainTab->setCurrentIndex(settings.mainTabIndex);
	this->ui->backupLineEdit->setText(settings.backupPath);
	this->ui->backupLineEdit->setToolTip(settings.backupPath);
	this->interfaceWidget->prefferedInterface = settings.prefferedInterface;
	this->ui->neighbourInactiveSpinBox->setValue(settings.inactiveLimit);
	this->ui->neighboursAdvancedViewCheckBox->setChecked(settings.neighboursAdvancedView);

	bool systrayAvailable = this->testSystrayMode();
	if (settings.systrayMode && !systrayAvailable) {
		log_settings.critical() << "System tray is not working properly. Unsing non-systray mode.";
	}
	bool useSystray = settings.systrayMode && systrayAvailable;
	this->setSystrayMode(useSystray);

	bool dockAvailable = this->testDockMode();
	if (settings.dock && !dockAvailable) {
		log_settings.critical() << "Dock mode is not working properly. Using non-docking mode.";
	}
	// Systray in necessary for the dock mode
	bool useDock = settings.dock && dockAvailable && useSystray;
	this->setDockMode(useDock);

	if (!this->isInDockMode()) {
		if (!settings.geometry.isEmpty())
			this->restoreGeometry(settings.geometry);
	}

	if (!isInSystrayMode()) {
		this->show();
	}

	// Run for the first time
	if (!settings.configPresent) {
		this->recalculateSize();
	}

	if (!settings.configPresent) {
		// no config found -> display settings dialog on startup
		this->displaySettings();
	} else {
		this->reinitConnection();
	}
}

bool MainWindow::isInSystrayMode()
{
	return this->systrayModeActive;
}

bool MainWindow::isInDockMode()
{
	return this->dockModeActive;
}

void MainWindow::showRunningInSystrayPopup()
{
	this->trayIcon->showMessage(tr("Running in system tray mode"), tr("Spectator is available in the system tray."), QSystemTrayIcon::Information, 1000 * appSettings.popupTime);
}

void MainWindow::reinitConnection()
{
	log_connection.debug() << "Closing connection.";
	this->disconnect();
	log_connection.debug() << "Connection closed.";

	// Delete old one
	if (this->connector) {
		delete this->connector;
		this->connector = NULL;
	}

	this->connector = new Connector(appSettings.server, appSettings.port, appSettings.tokenPath);
	QObject::connect(this->connector, SIGNAL(report(Communicator::ReportType, const QString &)), this, SLOT(updateStatusBar(Communicator::ReportType, const QString &)));

	// Connect connect and disconnect signals
	QObject::connect(this->connector, SIGNAL(connected()), this, SLOT(handleConnectorConnected()));
	QObject::connect(this->connector, SIGNAL(disconnected()), this, SLOT(handleConnectorDisconnected()));

	// Perform chats chats
	QSharedPointer<Chats::Initial> initialChat(QSharedPointer<Chats::Initial>(new Chats::Initial()));
	QObject::connect(initialChat.data(), SIGNAL(exportCapabilities(const QList<Capability> &)), this, SLOT(handleCapatibilitiesLoaded(const QList<Capability>&)));
	this->connector->performInitialChat(initialChat);

	Chats::GetNotifications *getNotificationsChat = new Chats::GetNotifications(appSettings.language);
	QObject::connect(getNotificationsChat, SIGNAL(exportNotifications(const QList<ParsedNotification> &)), this, SLOT(updateNotificationList(const QList<ParsedNotification> &)));
	QSharedPointer<Chats::Base> spGetNotificationChat = QSharedPointer<Chats::Base>(getNotificationsChat);
	this->connector->performChat(spGetNotificationChat);

	Chats::GetNethist *getNethistChat = new Chats::GetNethist();
	QObject::connect(getNethistChat, SIGNAL(exportNethist(const QList<NethistRecord> &)), this, SLOT(updateNethist(const QList<NethistRecord> &)));
	QSharedPointer<Chats::Base> spGetNethistChat = QSharedPointer<Chats::Base>(getNethistChat);
	this->connector->performChat(spGetNethistChat);

	Chats::GetStats *getStatsChat = new Chats::GetStats();
	QObject::connect(getStatsChat, SIGNAL(exportStats(const StatsRecord &)), this, SLOT(updateStats(const StatsRecord &)));
	QSharedPointer<Chats::Base> spGetStatsChat = QSharedPointer<Chats::Base>(getStatsChat);
	this->connector->performChat(spGetStatsChat);

	Chats::GetUpdaterData *getUpdaterChat = new Chats::GetUpdaterData(appSettings.language);
	QObject::connect(getUpdaterChat, SIGNAL(exportLists(const UpdaterDataRecord &)), this, SLOT(updateUpdaterData(const UpdaterDataRecord &)));
	QSharedPointer<Chats::Base> spGetUpdaterChat = QSharedPointer<Chats::Base>(getUpdaterChat);
	this->connector->performChat(spGetUpdaterChat);

	Chats::GetSerial *getSerialChat = new Chats::GetSerial();
	QObject::connect(getSerialChat, SIGNAL(exportSerial(const QString &)), this, SLOT(updateSerial(const QString &)));
	QSharedPointer<Chats::Base> spGetSerialChat = QSharedPointer<Chats::Base>(getSerialChat);
	this->connector->performChat(spGetSerialChat);

	Chats::GetNeighbours *getNeighbourChat = new Chats::GetNeighbours(appSettings.activeNeighbourDevices);
	QObject::connect(getNeighbourChat, SIGNAL(exportNeighbours(const QList<NeighbourRecord> &)), this, SLOT(updateNeighboursInit(const QList<NeighbourRecord> &)));
	QSharedPointer<Chats::Base> spGetNeighbourChat = QSharedPointer<Chats::Base>(getNeighbourChat);
	this->connector->performChat(spGetNeighbourChat);

	// Trigger notification refresh timer
	this->refreshTimer.stop();
	if (appSettings.refreshInterval > 0)
		this->refreshTimer.start(appSettings.refreshInterval * 1000);


	this->connector->initiateConnection();
}

void MainWindow::show() {
	QMainWindow::show();
	if (this->isInDockMode()) {
		this->recalculateSize();
	}
}

void MainWindow::toggleWindow(QSystemTrayIcon::ActivationReason reason)
{
	if (reason != QSystemTrayIcon::Context) {
		if (this->isVisible()) {
			#ifdef Q_OS_WIN
			this->close();
			#else
			if (!this->isActiveWindow()) {
				this->activateWindow();
				this->raise();
			} else {
				this->close();
			}
			#endif
		} else {
			this->updateWindowPosition();
			this->activateWindow();
			this->raise();
			this->show();
			if (!this->isInDockMode()) {
				if (!appSettings.geometry.isEmpty())
					this->restoreGeometry(appSettings.geometry);
			}
		}
	}
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	this->updateSettings();
	if (!this->isInSystrayMode()) {
		appSettings.store();
	}
	event->accept();
}

void MainWindow::storeSettingsAndQuit()
{
	this->updateSettings();
	appSettings.store();
	qApp->quit();
}

void MainWindow::activateNotificationTab()
{
	this->updateWindowPosition();
	this->show();
	this->ui->mainTab->setCurrentIndex(TAB_IDX_NOTIFICATION);
}

void MainWindow::activateStatisticsTab()
{
	this->updateWindowPosition();
	this->show();
	this->ui->mainTab->setCurrentIndex(TAB_IDX_NETHIST);
}

void MainWindow::activateDeviceTab()
{
	this->updateWindowPosition();
	this->show();
	this->ui->mainTab->setCurrentIndex(TAB_IDX_STATS);
}

void MainWindow::activateUpdaterTab()
{
	this->updateWindowPosition();
	this->show();
	this->ui->mainTab->setCurrentIndex(TAB_IDX_UPDATER);
}

void MainWindow::activateNeighboursTab()
{
	this->updateWindowPosition();
	this->show();
	this->ui->mainTab->setCurrentIndex(TAB_IDX_NEIGHBOURS);
}

void MainWindow::displayAbout()
{
	QMessageBox::about(
		this, tr("About Spectator"), tr(
			"<h1>Spectator</h1>"
			"Version: ") + SPECTATOR_VERSION + tr(
			"<p><center>Application which communicates with the Turris router using <br/> <b>NETCONF protocol</b></center></p>"
			"<b>Uses:</b>") +
			"<ul>"
			"<li>QT " QT_VERSION_STR "</li>"
			"<li>qcustomplot 1.3.1</li>"
			"<li>OpenSSL</li>"
			"</ul>"
			"<center>Copyright © 2016 <a href='https://www.nic.cz'>CZ.NIC, z. s. p. o.</a></center>"
			"<br/>"
	);
}

void MainWindow::displayBackupBrowseDialog()
{
	const QString &filename = QFileInfo(this->ui->backupLineEdit->text()).absoluteFilePath();
	const QString &res = QFileDialog::getSaveFileName(
		this, tr("backup file path"), filename, tr("Backups (*.tar.bz2);;All files (*.*)"),
		0, QFileDialog::DontConfirmOverwrite
	);
	if (!res.isNull()) {
		this->ui->backupLineEdit->setText(res);
		this->ui->backupLineEdit->setToolTip(res);
	}
}

void MainWindow::displaySettings()
{
	SettingsDialog *settingsDialog = new SettingsDialog();
	this->updateSettings();
	settingsDialog->loadSettings(appSettings);

	// Don't try to reconnect when the settings are displayed
	this->reconnectTimer.stop();
	this->reconnectStop = true;

	// Disable neighbour editing
	this->callNeighbourMacEditDisable();

	// Stop refresh when the settings is displayed
	this->refreshTimer.stop();
	if (settingsDialog->exec()) {
		QString origLang = appSettings.language;
		bool systrayModeBefore = this->isInSystrayMode();
		log_settings.debug() << "Applying settings from the settings dialog.";
		settingsDialog->updateSettings(appSettings);
		appSettings.store();
		this->useSettings(appSettings);
		if (this->isInSystrayMode() && !systrayModeBefore) {
			this->showRunningInSystrayPopup();
		}
		if (origLang != appSettings.language) {
			emit this->translationNeeded();
		}
	} else {
		if (appSettings.refreshInterval > 0)
			this->refreshTimer.start(appSettings.refreshInterval * 1000);
	}
	// Enable reconnects
	this->reconnectStop = false;
	// Set the reconnect timer if needed
	if (!this->connector->ready())
		this->reconnectTimer.start(RECONNECT_TIMEOUT);

	delete settingsDialog;
}

void MainWindow::updateStatusBar(Communicator::ReportType type, const QString &msg)
{
	this->ui->statusTextLabel->setText(msg);
	if (Communicator::RT_ERROR == type) {
		this->ui->statusIconLabel->setPixmap(QApplication::style()->standardIcon(QStyle::SP_MessageBoxCritical).pixmap(25,25));
	} else if (Communicator::RT_WARNING == type) {
		this->ui->statusIconLabel->setPixmap(QApplication::style()->standardIcon(QStyle::SP_MessageBoxWarning).pixmap(25,25));
	} else {
		this->ui->statusIconLabel->setPixmap(QApplication::style()->standardIcon(QStyle::SP_MessageBoxInformation).pixmap(25,25));
	}
}

void MainWindow::updateStatusBar(const QString &msg) {
	this->updateStatusBar(Communicator::RT_INFO, msg);
}

void MainWindow::callPerformLoopChats()
{
	if (!this->connector || !this->connector->ready()) {
		return;
	}

	// Always refresh notifications
	Chats::GetNotifications *getNotificationsChat = new Chats::GetNotifications(appSettings.language);
	QObject::connect(getNotificationsChat, SIGNAL(exportNotifications(const QList<ParsedNotification> &)), this, SLOT(updateNotificationList(const QList<ParsedNotification> &)));
	QSharedPointer<Chats::Base> spGetNotificationsChat = QSharedPointer<Chats::Base>(getNotificationsChat);
	this->connector->performChat(spGetNotificationsChat);

	// Always refresh neighbours
	this->callNeighbourChat();

	// Refresh other only it concerns selected tab
	switch (this->ui->mainTab->currentIndex()) {
		case TAB_IDX_NETHIST:
			{
				Chats::GetNethist *getNethistChat = new Chats::GetNethist();
				QObject::connect(getNethistChat, SIGNAL(exportNethist(const QList<NethistRecord> &)), this, SLOT(updateNethist(const QList<NethistRecord> &)));
				QSharedPointer<Chats::Base> spGetNethistChat = QSharedPointer<Chats::Base>(getNethistChat);
				this->connector->performChat(spGetNethistChat);
			}
			break;

		case TAB_IDX_STATS:
			{
				Chats::GetStats *getStatsChat = new Chats::GetStats();
				QObject::connect(getStatsChat, SIGNAL(exportStats(const StatsRecord &)), this, SLOT(updateStats(const StatsRecord &)));
				QSharedPointer<Chats::Base> spGetStatsChat = QSharedPointer<Chats::Base>(getStatsChat);
				this->connector->performChat(spGetStatsChat);
			}
			break;

		case TAB_IDX_UPDATER:
			{
				if (!this->updaterUserList->isInEditMode()) {
					Chats::GetUpdaterData *getUpdaterChat = new Chats::GetUpdaterData(appSettings.language);
					QObject::connect(getUpdaterChat, SIGNAL(exportLists(const UpdaterDataRecord &)), this, SLOT(updateUpdaterData(const UpdaterDataRecord &)));
					QSharedPointer<Chats::Base> spGetUpdaterChat = QSharedPointer<Chats::Base>(getUpdaterChat);
					this->connector->performChat(spGetUpdaterChat);
				}
			}
		break;
	}
}

void MainWindow::callNeighbourChat() {
	if (this->connector && this->connector->ready() && this->enableNeighbourChat) {
		Chats::GetNeighbours *getNeighbourChat = new Chats::GetNeighbours(appSettings.activeNeighbourDevices);
		QObject::connect(getNeighbourChat, SIGNAL(exportNeighbours(const QList<NeighbourRecord> &)), this, SLOT(updateNeighbours(const QList<NeighbourRecord> &)));
		QSharedPointer<Chats::Base> spGetNeighbourChat = QSharedPointer<Chats::Base>(getNeighbourChat);
		this->connector->performChat(spGetNeighbourChat);
	}
}

void MainWindow::handleRebootingRouter()
{
	log_control.debug() << "Router started to reboot.";
	this->setNotificationsEnabled(false);
	this->ui->controlScrollArea->setEnabled(false);
}

void MainWindow::updateWindowPosition()
{
	if (!this->isInDockMode()) {  // update only when in dock mode
		return;
	}

	const QRect &trayGemoetry = this->trayIcon->geometry();
	QScreen *screen = this->trayIcon->getScreen();
	const QRect &screenGeometry = screen->availableGeometry();

	int x1, x2, y1, y2;
	trayGemoetry.getCoords(&x1, &y1, &x2, &y2);
	int window_width = this->width();
	int window_height = this->height();

	// other calculations will be done in relative coordinates to the screen
	x1 -= screenGeometry.x();
	x2 -= screenGeometry.x();
	y1 -= screenGeometry.y();
	y2 -= screenGeometry.y();

	// left vs right
	bool screen_left;
	if (screenGeometry.width() - x1 > x1) {
		screen_left = true;
	} else {
		screen_left = false;
	}

	// top vs bottom
	bool screen_top;
	if (screenGeometry.height() - y1 > y1) {
		screen_top = true;
	} else {
		screen_top = false;
	}

	// horizontal vs vertical
	bool horizontal;
	if (screen_top) {
		if (screen_left) {  // top left
			horizontal = x1 > y1;
		} else { // top right
			horizontal = screenGeometry.width() - x2 > y1;
		}
	} else {
		if (screen_left) { // bottom left
			horizontal = x1 > screenGeometry.height() - y1;
		} else { // bottom right
			horizontal = screenGeometry.width() - x2 > screenGeometry.height() - y2;
		}
	}

	// set the coordinates
	int window_x, window_y;
	if (screen_top) {
		if (screen_left) {  // top left
			window_x = horizontal ? x1 : x2;
			window_y = horizontal ? y2 : y1;
		} else { // top right
			window_x = horizontal ? x2 - window_width : x1 - window_width;
			window_y = horizontal ? y2 : y1;
		}
	} else {
		if (screen_left) { // bottom left
			window_x = horizontal ? x1 : x2;
			window_y = horizontal ? y1 - window_height : y2 - window_height;
		} else { // bottom right
			window_x = horizontal ? x2 - window_width : x1 - window_width;
			window_y = horizontal ? y1 - window_height : y2 - window_height;
		}
	}

	// update final coordinates with the screen offsets
	window_x += screenGeometry.x();
	window_y += screenGeometry.y();

	this->setGeometry(window_x, window_y, this->width(), this->height());
}

void MainWindow::updateNotificationList(const QList<ParsedNotification> &list)
{
	// hide/show noNotification label
	this->setNoNotificationsVisible(list.empty());

	const QList<NotificationWidget *> &widgets = this->ui->notificationScrollAreaWidgetContents->findChildren<NotificationWidget *>();
	log_notifications.debug() << "Updating notifications";
	for (QList<ParsedNotification>::const_iterator i = list.begin(); i != list.end(); ++i) {
		log_notifications.debug() << "Downloaded notification:" << i->id << i->severity;
	}

	// Add new
	int newCount = 0;
	for (QList<ParsedNotification>::const_iterator j = list.begin(); j != list.end(); ++j) {
		bool inList = false;
		for (QList<NotificationWidget *>::const_iterator i = widgets.begin(); i != widgets.end(); ++i) {
			if (j->id == (*i)->id) {
				inList = true;
				(*i)->updateTexts((*j).body);
			}
		}
		if (!inList) {
			new NotificationWidget(this->ui->notificationScrollAreaWidgetContents, this, j->id, j->severity, j->time, j->body);
			newCount += 1;
		}
	}

	// Remove missing
	for (QList<NotificationWidget *>::const_iterator i = widgets.begin(); i != widgets.end(); ++i) {
		bool inList = false;
		for (QList<ParsedNotification>::const_iterator j = list.begin(); j != list.end(); ++j) {
			if (j->id == (*i)->id)
				inList = true;
		}
		if (!inList)
			delete(*i);
	}

	if (newCount && this->isInSystrayMode()) {
		this->trayIcon->showMessage(tr("Notifications lists updated"), tr("Number of new notifications: ") + QString::number(newCount), QSystemTrayIcon::Information, 1000 * appSettings.popupTime);
		this->trayIcon->disconnect(SIGNAL(messageClicked()));
		QObject::connect(this->trayIcon, SIGNAL(messageClicked()), this, SLOT(handleSystrayNotificationMessageClicked()));
	}
	log_notifications.debug() << "Notifications updated.";
}

void MainWindow::updateNethist(const QList<NethistRecord> &list)
{
	log_nethist.debug() << "Updating nethist";

	if (list.empty()) {  // No data returned..
		log_nethist.debug() << "no data returned";
		return;
	}

	const NethistRecord &last = list.last();
	this->ui->loadText->setText(QString("%1").arg(last.cpuLoad));
	this->ui->cpuTemperatureText->setText(QString::number(last.cpuTemperature));
	this->ui->boardTemperatureText->setText(QString::number(last.boardTemperature));
	this->ui->usedDiskText->setText(QString::number(last.usedDiskSpace / 1024) + " MB");  // in MB
	this->ui->freeDiskText->setText(QString::number(last.availableDiskSpace / 1024) + " MB");  // in MB
	this->ui->totalDiskText->setText(QString::number((last.availableDiskSpace + last.usedDiskSpace) / 1024) + " MB");  // in MB
	this->ui->memoryTotalText->setText(QString::number(last.totalMemory / 1024) + " MB");  // in MB
	this->ui->memoryFreeText->setText(QString::number(last.freeMemory / 1024) + " MB");  // in MB
	this->ui->memoryUsedText->setText(QString::number((last.totalMemory - last.freeMemory - last.cachedMemory) / 1024) + " MB");  // in MB
	this->ui->memoryCachedText->setText(QString::number(last.cachedMemory / 1024) + " MB");  // in MB

	QVector<double> xAxis;
	QVector<double> yDiskTotalAxis;
	QVector<double> yDiskUsedAxis;
	QVector<double> yLoadAxis;
	QVector<double> yTemperatureBoardAxis;
	QVector<double> yTemperatureCpuAxis;
	QVector<double> yMemoryTotalAxis;
	QVector<double> yMemoryUsedAxis;
	QVector<double> yMemoryCachedAxis;
	QMap<QString, InterfaceWidgetRecord> interfaceData;

	QMap<QString, qulonglong> lastRx, lastTx, lastTime;
	foreach (const NethistRecord &record, list) {
		xAxis.append(record.time.toTime_t());
		yDiskUsedAxis.append(double(record.usedDiskSpace) / 1024);
		yDiskTotalAxis.append(double(record.usedDiskSpace + record.availableDiskSpace) / 1024);
		yLoadAxis.append(record.cpuLoad);
		yTemperatureCpuAxis.append(record.cpuTemperature);
		yTemperatureBoardAxis.append(record.boardTemperature);
		yMemoryTotalAxis.append(record.totalMemory / 1024);
		yMemoryUsedAxis.append((record.totalMemory - record.freeMemory - record.cachedMemory) / 1024);
		yMemoryCachedAxis.append((record.totalMemory - record.freeMemory) / 1024);

		foreach (const NethistInterfaceRecord &interfaceRecord, record.interfaces) {
			if (!lastTime.contains(interfaceRecord.name)) {  // skip first
				lastRx[interfaceRecord.name] = interfaceRecord.rx;
				lastTx[interfaceRecord.name] = interfaceRecord.tx;
				lastTime[interfaceRecord.name] = record.time.toTime_t();
				continue;
			}
			if (!interfaceData.contains(interfaceRecord.name)) {
				interfaceData[interfaceRecord.name] = InterfaceWidgetRecord();
			}
			int time = record.time.toTime_t();
			InterfaceWidgetRecord *iRecord = &interfaceData[interfaceRecord.name];
			iRecord->xTimeAxis.append(time);
			iRecord->yRxAxis.append(
				(interfaceRecord.rx - lastRx[interfaceRecord.name])
				/ (time - lastTime[interfaceRecord.name])
			);
			iRecord->yTxAxis.append(
				(interfaceRecord.tx - lastTx[interfaceRecord.name])
				/ (time - lastTime[interfaceRecord.name])
			);
			lastRx[interfaceRecord.name] = interfaceRecord.rx;
			lastTx[interfaceRecord.name] = interfaceRecord.tx;
			lastTime[interfaceRecord.name] = time;
		}
	}

	log_nethist.debug() << "Nethist records parsed: " << list.count();

	this->interfaceWidget->setData(interfaceData);

	QCustomPlot *plot;
	plot = this->ui->diskPlot;
	{
		int maxDisk = 0;
		foreach (float load, yDiskTotalAxis) {
			maxDisk = load > maxDisk ? load : maxDisk;
		}
		plot->axisRect()->axis(QCPAxis::atLeft)->setRange(0, maxDisk * 1.1);
		plot->axisRect()->axis(QCPAxis::atBottom)->setRange(xAxis.first(), xAxis.last());
		plot->graph(0)->setData(xAxis, yDiskTotalAxis);
		plot->graph(1)->setData(xAxis, yDiskUsedAxis);
		plot->replot();
	}

	plot = this->ui->loadPlot;
	{
		double maxLoad = 0;
		foreach (float load, yLoadAxis) {
			maxLoad = load > maxLoad ? load : maxLoad;
		}
		plot->axisRect()->axis(QCPAxis::atLeft)->setRange(0, maxLoad * 1.1);
		plot->axisRect()->axis(QCPAxis::atBottom)->setRange(xAxis.first(), xAxis.last());
		plot->graph(0)->setData(xAxis, yLoadAxis);
		plot->replot();
	}

	plot = this->ui->temperaturePlot;
	{
		int maxTemp = 0;
		int minTemp = 150;
		foreach (int temp, yTemperatureCpuAxis) {
			maxTemp = temp > maxTemp ? temp : maxTemp;
			minTemp = temp < minTemp ? temp : minTemp;
		}
		foreach (int temp, yTemperatureBoardAxis) {
			maxTemp = temp > maxTemp ? temp : maxTemp;
			minTemp = temp < minTemp ? temp : minTemp;
		}
		plot->axisRect()->axis(QCPAxis::atLeft)->setRange(minTemp * 0.8, maxTemp * 1.1);
		plot->axisRect()->axis(QCPAxis::atBottom)->setRange(xAxis.first(), xAxis.last());
		plot->graph(0)->setData(xAxis, yTemperatureCpuAxis);
		plot->graph(1)->setData(xAxis, yTemperatureBoardAxis);
		plot->replot();
	}

	plot = this->ui->memoryPlot;
	{
		double maxMem = 0;
		foreach (float mem, yMemoryTotalAxis) {
			maxMem = mem > maxMem ? mem : maxMem;
		}
		plot->axisRect()->axis(QCPAxis::atLeft)->setRange(0, maxMem * 1.1);
		plot->axisRect()->axis(QCPAxis::atBottom)->setRange(xAxis.first(), xAxis.last());
		plot->graph(0)->setData(xAxis, yMemoryTotalAxis);
		plot->graph(1)->setData(xAxis, yMemoryCachedAxis);
		plot->graph(2)->setData(xAxis, yMemoryUsedAxis);
		plot->replot();
	}

	log_nethist.debug() << "Charts updated";
}


void MainWindow::updateStats(const StatsRecord &record)
{
	// TODO move to a better place
	QHash<QString, QString> typeAliases;
	typeAliases["inet6"] = "IPv6";
	typeAliases["inet"] = "IPv4";
	typeAliases["link/ether"] = "MAC";

	log_stats.debug() << "Updating stats.";
	setTextLabel(this->ui->kernelLabel, record.kernelVersion);
	setTextLabel(this->ui->versionLabel, record.osVersion);
	setTextLabel(this->ui->typeLabel, record.model + " - " + record.boardName.toUpper());
	setTextLabel(this->ui->hostnameLabel, record.hostname);
	int days = record.uptime / 24 / 60 / 60;
	int hours = (record.uptime - 24 * 60 * 60 * days) / 60 / 60;
	int minutes = (record.uptime - 24 * 60 * 60  * days - hours * 60 * 60) / 60;
	setTextLabel(this->ui->uptimeLabel, QString("%1d %2h %3m").arg(QString::number(days)).arg(QString::number(hours)).arg(QString::number(minutes)));
	setTextLabel(this->ui->firewallSendingLabel, QString(tr("%1 (%2 second ago)")).arg(record.firewallStatus).arg(record.firewallAge));
	setTextLabel(this->ui->ucollectSendingLabel, QString(tr("%1 (%2 second ago)")).arg(record.ucollectStatus).arg(record.ucollectAge));

	// remove all widgets
	foreach (QWidget *w, this->ui->networkDevicesList->findChildren<QWidget *>()) {
		delete w;
	}

	// remove the spacer
	QSpacerItem *spacer = this->ui->networkDevicesList->layout()->itemAt(0)->spacerItem();
	this->ui->networkDevicesList->layout()->removeItem(spacer);
	delete spacer;

	log_stats.debug() << "Interface count:" << record.interfaces.count();
	QList<QString> interfaceNames;
	int active_count = 0;
	for (QList<StatsInterface>::const_iterator i = record.interfaces.begin(); i != record.interfaces.end(); ++i) {
		interfaceNames.append(i->name);
		// Display only device which are up
		if (!i->up) {
			continue;
		}
		active_count +=1;

		QLabel *l = new QLabel(this->ui->networkDevicesList);
		l->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
		QString text = "<b>" + i->name + "</b>";
		text += "<table>";
		for (QList<StatsAddress>::const_iterator j = i->addresses.begin(); j != i->addresses.end(); ++j) {
			QString typeAlias = typeAliases[j->type];
			text += "<tr>";
			text += "<td>" + (typeAlias.isEmpty() ? j->type : typeAlias) + ":</td>";
			text += "<td><i>" + j->address + "</i></td>";
			text += "</tr>";
		}

		text += "</table>";
		l->setText(text);
		this->ui->networkDevicesList->layout()->addWidget(l);
	}
	this->ui->networkDevicesList->layout()->addItem(new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Expanding));
	log_stats.debug() << "Active Interface count:" << active_count;

	log_stats.debug() << "Updating interface list.";
	this->updateNeighbourDevices(interfaceNames);

	log_stats.debug() << "Stats were updated.";
}

void MainWindow::updateUpdaterData(const UpdaterDataRecord &record)
{
	this->ui->updaterStatusLabel->setText(record.status);
	this->updaterUserList->setUserListData(record.userLists);
	this->updaterActivities->setActivitiesData(record.lastActivities);
}

void MainWindow::updateSerial(const QString &serial)
{
	bool res;
	const QString &serialTmp = (serial.length() % 2 == 0 ? "0x" : "0x0") + serial;
	qlonglong serialDec = serialTmp.toULongLong(&res, 16);
	if (res) {
		this->ui->serialLabel->setText(QString::number(serialDec));
	}
}

void MainWindow::updateNeighbours(const QList<NeighbourRecord> &list, bool popup, bool markUnread)
{
	// Log downloaded neighbours
	log_neighbours.debug() << "Updating neighbours";
	log_neighbours.debug() << "Interface filter used:" << appSettings.activeNeighbourDevices;
	foreach (const NeighbourRecord &record, list) {
		log_neighbours.debug() << "Downloaded neighbour:" << record.macAddress << record.interfaceName;
	}

	// filter list based on time limit
	QList<NeighbourRecord> filteredList;
	foreach (const NeighbourRecord &record, list) {
		NeighbourRecord newRecord(record);
		newRecord.ips.clear();
		foreach (const NeighbourIpRecord &ipRecord, record.ips) {
			// At leas one of the Since times is smaller then limit
			// Since times are -1 when statistics tag is not present
			int limit = this->ui->neighbourInactiveSpinBox->value();
			if (ipRecord.updatedSince < limit || ipRecord.usedSince < limit || ipRecord.confirmedSince < limit) {
				newRecord.ips.append(ipRecord);
			} else {
				log_neighbours.debug() << "Record is outdated and will be skipped" << record.macAddress << record.interfaceName << ipRecord.ipAddress;
			}
		}

		if (!newRecord.ips.isEmpty())
			filteredList.append(newRecord);
	}

	// hide/show noNeighbours label
	this->setNoNeighboursVisible(filteredList.empty());

	const QList<NeighbourWidget *> &widgets = this->ui->neighboursScrollWidget->findChildren<NeighbourWidget *>();

	// Add new and update existing
	QStringList addedDevices;
	foreach (const NeighbourRecord &record, filteredList) {
		bool inList = false;
		foreach (NeighbourWidget *widget, widgets) {
			if (widget->mac == record.macAddress && widget->dev == record.interfaceName) {
				// Update name mapping
				widget->setMacMapping(&appSettings.neighboursMacToName);
				// Updating record
				widget->setContent(&record);
				widget->setBodyText(appSettings.neighboursAdvancedView);
				QObject::connect(widget, SIGNAL(editStatusChanged(bool)), this, SLOT(handleNeighbourMacEdit(bool)));
				inList = true;
			}
		}
		if (!inList) {
			NeighbourWidget *widget = new NeighbourWidget(this->ui->neighboursScrollWidget,
					this, &record, &appSettings.neighboursMacToName);
			widget->setBodyText(appSettings.neighboursAdvancedView);
			((QVBoxLayout *) this->ui->neighboursScrollWidget->layout())->insertWidget(0, widget);
			if (markUnread) {
				widget->markUnread();
			} else {
				widget->markRead();
			}
			const QString &alias = widget->getAlias(widget->mac);
			addedDevices.append(alias.isEmpty() ? widget->mac : alias);
		}
	}

	// Remove missing
	QStringList removedDevices;
	foreach (NeighbourWidget *widget, widgets) {
		bool inList = false;
		foreach (const NeighbourRecord &record, filteredList) {
			if (widget->mac == record.macAddress && widget->dev == record.interfaceName) {
				inList = true;
			}
		}
		if (!inList) {
			const QString &alias = widget->getAlias(widget->mac);
			removedDevices.append(alias.isEmpty() ? widget->mac : alias);
			delete widget;
		}
	}

	if (this->isInSystrayMode() && popup && (removedDevices.count()|| addedDevices.count())) {
		QString msg;
		msg += tr("discovered: ") + QString::number(addedDevices.count()) + "\n";
		foreach (const QString &device, addedDevices) {
			msg += " • " + device + "\n";
		}
		msg += tr("disappeared: ") + QString::number(removedDevices.count())  + "\n";
		foreach (const QString &device, removedDevices) {
			msg += " • " + device + "\n";
		}

		this->trayIcon->showMessage(
			tr("Neighbour list has changed."), msg, QSystemTrayIcon::Information, 1000 * appSettings.popupTime);
		this->trayIcon->disconnect(SIGNAL(messageClicked()));
		QObject::connect(this->trayIcon, SIGNAL(messageClicked()), this, SLOT(handleSystrayNeighbourClicked()));
	}

	log_neighbours.debug() << "Neighbours updated.";
}

void MainWindow::updateNeighboursInit(const QList<NeighbourRecord> &list)
{
	this->updateNeighbours(list, false, false);
}

void MainWindow::setNoNotificationsVisible(bool visible)
{
	this->ui->notificationScrollAreaWidgetContents->setSizePolicy(QSizePolicy::Expanding, visible ? QSizePolicy::Expanding : QSizePolicy::Fixed);
	this->ui->noNotificationsLabel->setVisible(visible);
}

void MainWindow::setNoNeighboursVisible(bool visible)
{
	this->ui->neighboursScrollWidget->setSizePolicy(QSizePolicy::Expanding, visible ? QSizePolicy::Expanding : QSizePolicy::Fixed);
	this->ui->noNeighboursLabel->setVisible(visible);
}

bool MainWindow::testDockMode()
{
	const QRect &rect = this->trayIcon->geometry();
	return rect.width() > 0 && rect.height() > 0;
}

bool MainWindow::testSystrayMode()
{
	if (appSettings.noSystrayMode) {
		return false;
	} else if (appSettings.forceSystrayMode) {
		return true;
	}
	return QSystemTrayIcon::isSystemTrayAvailable();
}

void MainWindow::setSystrayMode(bool active)
{
	if (active) {

		this->trayIcon->show();

		// Don't close on exit
		qApp->setQuitOnLastWindowClosed(false);


		this->systrayModeActive = true;
	} else {

		this->trayIcon->hide();

		// Close on exit
		qApp->setQuitOnLastWindowClosed(true);
		this->show();

		this->systrayModeActive = false;
	}
}

void MainWindow::setDockMode(bool active)
{
	unsigned int framelessFlags = Qt::FramelessWindowHint | Qt::MSWindowsFixedSizeDialogHint | Qt::CustomizeWindowHint;

	if (active) {
		// Don't switch window params when it's not necessary
		if (!((this->windowFlags() & framelessFlags) == framelessFlags)) {

			// Remove window decorations
			this->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);
			this->setWindowFlags(Qt::CustomizeWindowHint);
			this->setWindowFlags(Qt::FramelessWindowHint);

			// Set status bar icon margins
			this->ui->statusIconLayout->setContentsMargins(5, 3, 0, 5);

			this->ui->menuBar->hide();
		}

		this->dockModeActive = true;
		// update the main window position
		this->updateWindowPosition();
	} else {
		// Don't switch window params when it's not necessary
		if (this->windowFlags() & framelessFlags) {
			// Use window decorations
			this->setWindowFlags(this->windowFlags() & ~Qt::MSWindowsFixedSizeDialogHint);
			this->setWindowFlags(this->windowFlags() & ~Qt::CustomizeWindowHint);
			this->setWindowFlags(this->windowFlags() & ~Qt::FramelessWindowHint);

			// Remove status bar icon margins
			this->ui->statusIconLayout->setContentsMargins(0, 0, 0, 0);

			this->ui->menuBar->show();
		}
		this->dockModeActive = false;
	}
}

void MainWindow::initCharts()
{

	QCustomPlot *plot;


	// Disk plot
	plot = this->ui->diskPlot;
	{
		plot->plotLayout()->clear();
		plot->plotLayout()->setMargins(QMargins(0,0,0,0));
		QCPAxisRect *wideAxisRect = new QCPAxisRect(plot, false);
		wideAxisRect->setMargins(QMargins(0,0,0,0));
		QCPAxis *leftAxis = wideAxisRect->addAxis(QCPAxis::atLeft);
		leftAxis->setPadding(0);
		leftAxis->setRange(0.0, 249.0);
		QCPAxis *bottomAxis = wideAxisRect->addAxis(QCPAxis::atBottom);
		bottomAxis->setVisible(false);
		plot->plotLayout()->addElement(0, 0, wideAxisRect);
		QCPGraph *graph;
		// Add total
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::green));
		graph->setBrush(QBrush(QColor(0x80, 0XFF, 0x80, 0x80)));
		// Add used
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::red));
		graph->setBrush(QBrush(QColor(0XFF, 0x80, 0x80, 0x80)));

		plot->graph(0)->setChannelFillGraph(plot->graph(1));
	}

	// Load plot
	plot = this->ui->loadPlot;
	{
		plot->plotLayout()->clear();
		plot->plotLayout()->setMargins(QMargins(0,0,0,0));
		QCPAxisRect *wideAxisRect = new QCPAxisRect(plot, false);
		wideAxisRect->setMargins(QMargins(0,0,0,0));
		QCPAxis *leftAxis = wideAxisRect->addAxis(QCPAxis::atLeft);
		leftAxis->setPadding(0);
		leftAxis->setRange(0.0, 1.0);
		leftAxis->setAutoTickCount(3);
		QCPAxis *bottomAxis = wideAxisRect->addAxis(QCPAxis::atBottom);
		bottomAxis->setVisible(false);
		plot->plotLayout()->addElement(0, 0, wideAxisRect);
		QCPGraph *graph;
		// Add load
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::black));
		graph->setBrush(QBrush(QColor(0xD0, 0xD0, 0xD4, 0x80)));
	}

	// Temperature plot
	plot = this->ui->temperaturePlot;
	{
		plot->plotLayout()->clear();
		plot->plotLayout()->setMargins(QMargins(0,0,0,0));
		QCPAxisRect *wideAxisRect = new QCPAxisRect(plot, false);
		wideAxisRect->setMargins(QMargins(0,0,0,0));
		QCPAxis *leftAxis = wideAxisRect->addAxis(QCPAxis::atLeft);
		leftAxis->setPadding(0);
		leftAxis->setRange(0.0, 150);
		leftAxis->setAutoTickCount(3);
		QCPAxis *bottomAxis = wideAxisRect->addAxis(QCPAxis::atBottom);
		bottomAxis->setVisible(false);
		plot->plotLayout()->addElement(0, 0, wideAxisRect);
		QCPGraph *graph;
		// Add board temperature
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::red));
		// Add board temperature
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::blue));
	}

	// Memory plot
	plot = this->ui->memoryPlot;
	{
		plot->plotLayout()->clear();
		plot->plotLayout()->setMargins(QMargins(0,0,0,0));
		QCPAxisRect *wideAxisRect = new QCPAxisRect(plot, false);
		wideAxisRect->setMargins(QMargins(0,0,0,0));
		QCPAxis *leftAxis = wideAxisRect->addAxis(QCPAxis::atLeft);
		leftAxis->setPadding(0);
		leftAxis->setRange(0.0, 2048);
		QCPAxis *bottomAxis = wideAxisRect->addAxis(QCPAxis::atBottom);
		bottomAxis->setVisible(false);
		leftAxis->setAutoTickCount(5);
		plot->plotLayout()->addElement(0, 0, wideAxisRect);
		QCPGraph *graph;
		// Add Free memory
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::green));
		graph->setBrush(QBrush(QColor(0x80, 0XFF, 0x80, 0x80)));
		// Add cached memory
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::gray));
		graph->setBrush(QBrush(QColor(0xD0, 0xD0, 0xD4, 0x80)));
		// Add used memory
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::red));
		graph->setBrush(QBrush(QColor(0XFF, 0x80, 0x80, 0x80)));

		plot->graph(0)->setChannelFillGraph(plot->graph(1));
		plot->graph(1)->setChannelFillGraph(plot->graph(2));
	}
}

void MainWindow::discardNotification(const QString &messageId)
{
	const QList<NotificationWidget *> &widgets = this->ui->notificationScrollAreaWidgetContents->findChildren<NotificationWidget *>();
	for (QList<NotificationWidget *>::const_iterator i = widgets.begin(); i != widgets.end(); ++i) {
		if ((*i)->id == messageId) {
			delete *i;
			if (widgets.length() == 1)  // last notification removed
				this->setNoNotificationsVisible(true);
			break;
		}
	}
}

void MainWindow::setNotificationsEnabled(bool enabled)
{
	const QList<NotificationWidget *> &widgets = this->ui->notificationScrollAreaWidgetContents->findChildren<NotificationWidget *>();
	for (QList<NotificationWidget *>::const_iterator i = widgets.begin(); i != widgets.end(); ++i) {
		((NotificationWidget *)(*i))->setEnabledChildren(enabled);
	}
	this->ui->notificationsScrollArea->setEnabled(enabled);
}

void MainWindow::callCloseNotification(const QString &messageId)
{
	Chats::DeleteNotification *chat = new Chats::DeleteNotification(messageId);
	QObject::connect(chat, SIGNAL(notificationDeleted(const QString &)), this, SLOT(discardNotification(const QString &)));
	QSharedPointer<Chats::Base> spChat = QSharedPointer<Chats::Base>(chat);
	this->connector->performChat(spChat);
}

void MainWindow::callRebootRouter()
{
	if (QMessageBox::Yes == QMessageBox::question(
			this, tr("Reboot the router?"),
			tr("Do you really want to reboot the router? Note that all current notifications will be discarded afterwards."),
			QMessageBox::Yes|QMessageBox::No)) {
		Chats::RebootRouter *chat = new Chats::RebootRouter();
		QObject::connect(chat, SIGNAL(rebootingRouter()), this, SLOT(handleRebootingRouter()));
		log_control.debug() << "Reboot command issued.";
		QSharedPointer<Chats::Base> spChat = QSharedPointer<Chats::Base>(chat);
		this->connector->performChat(spChat);
	}
}

void MainWindow::callCheckUpdater() {
	Chats::CheckUpdater *checkChat = new Chats::CheckUpdater();
	QObject::connect(checkChat, SIGNAL(finished(bool)), this, SLOT(updaterCheckFinished(bool)));
	QSharedPointer<Chats::Base> spCheckChat = QSharedPointer<Chats::Base>(checkChat);
	Chats::GetUpdaterData *getChat = new Chats::GetUpdaterData(appSettings.language);
	QObject::connect(getChat, SIGNAL(exportLists(const UpdaterDataRecord &)), this, SLOT(updateUpdaterData(const UpdaterDataRecord &)));
	QSharedPointer<Chats::Base> spGetChat = QSharedPointer<Chats::Base>(getChat);
	this->connector->performChat(spCheckChat);
	this->connector->performChat(spGetChat);
}

void MainWindow::callSetPassword()
{
	if (this->ui->password2LineEdit->text() != this->ui->passwordLineEdit->text()) {
		QMessageBox::critical(this, tr("password mismatch"), tr("Password mismatch. Please write the same password into both fields."));
		return;
	}

	if (this->ui->password2LineEdit->text().size() < 6) {
		QMessageBox::critical(this, tr("short password"), tr("The password is too short. Please use at least <b>6 characters</b>."));
		return;
	}

	Chats::SetPassword *chat = new Chats::SetPassword(this->ui->passwordLineEdit->text());
	log_control.debug() << "Set password command started.";
	QObject::connect(chat, SIGNAL(finished(bool)), this, SLOT(setPasswordFinished(bool)));
	QSharedPointer<Chats::Base> spChat = QSharedPointer<Chats::Base>(chat);
	this->connector->performChat(spChat);
}

void MainWindow::callStoreBackup()
{
	Chats::StoreBackup *chat = new Chats::StoreBackup();
	QObject::connect(chat, SIGNAL(storeData(const QByteArray &)), this, SLOT(storeBackupFinished(const QByteArray &)));
	log_control.debug() << "Store backup started.";
	QSharedPointer<Chats::Base> spChat = QSharedPointer<Chats::Base>(chat);
	this->connector->performChat(spChat);
}

void MainWindow::callRestoreBackup()
{
	QFile file(this->ui->backupLineEdit->text());
	if (file.open(QFile::ReadOnly)) {
		const QByteArray &data = file.readAll();
		Chats::RestoreBackup *chat = new Chats::RestoreBackup(data);
		QObject::connect(chat, SIGNAL(dataRestored(const QString &)), this, SLOT(restoreBackupFinished(const QString &)));
		log_control.debug() << "Restore backup started.";
		QSharedPointer<Chats::Base> spChat = QSharedPointer<Chats::Base>(chat);
		this->connector->performChat(spChat);
	} else {
		// Failed to open
		QMessageBox::critical(this, tr("failed to open"), tr("Failed to open the backup file."));
		log_control.critical() << QString("Failed to open backup file '%1'.").arg(this->ui->backupLineEdit->text());
	}
}

void MainWindow::callSetUpdater()
{
	QList<QString> mergeList, removeList;
	for (int i = 0; i < this->updaterUserList->getLenght(); ++i) {
		if (this->updaterUserList->getCheckbox(i)->isChecked()) {
			mergeList.append(*this->updaterUserList->getName(i));
		} else {
			removeList.append(*this->updaterUserList->getName(i));
		}
	}

	Chats::SetUpdater *chat = new Chats::SetUpdater(mergeList, removeList);
	QObject::connect(chat, SIGNAL(finished(bool)), this, SLOT(setUpdaterFinished(bool)));
	QSharedPointer<Chats::Base> spChat = QSharedPointer<Chats::Base>(chat);
	this->connector->performChat(spChat);
}

void MainWindow::framesSetEnabled(bool enabled)
{
	this->updaterUserList->setEnabled(enabled);
	this->ui->controlScrollArea->setEnabled(enabled);
	this->ui->neighbourInterfacesButton->setEnabled(enabled);
	this->ui->neighbourInactiveSpinBox->setEnabled(enabled);
	this->ui->neighboursAdvancedViewCheckBox->setEnabled(enabled);
	this->ui->updaterStatusScrollArea->setEnabled(enabled);
	this->ui->neighboursScrollWidget->setEnabled(enabled);
	this->ui->deviceDataCollectingScrollArea->setEnabled(enabled);
	this->ui->deviceInterfacesScrollArea->setEnabled(enabled);
	this->ui->deviceSystemScrollArea->setEnabled(enabled);
	this->ui->statisticsMemoryFrame->setEnabled(enabled);
	this->ui->statisticsNandFrame->setEnabled(enabled);
	this->ui->statisticsNetworkFrame->setEnabled(enabled);
	this->ui->statisticsTemperatureFrame->setEnabled(enabled);
}

void MainWindow::handleConnectorConnected()
{
	this->reconnectTimer.stop();
	this->setNotificationsEnabled(true);

	this->framesSetEnabled(true);

	// update tray icon
	QIcon icon(":/icons/img/trayicon.png");
	this->trayIcon->setIcon(icon);
}

void MainWindow::disconnect() {
	// Set the timer
	this->reconnectTimer.stop();
	if (!this->reconnectStop) {
		this->reconnectTimer.start(RECONNECT_TIMEOUT);
	}

	// update tray icon
	const QIcon icon(":/icons/img/trayicon-gray.png");
	this->trayIcon->setIcon(icon);

	this->updateStatusBar(Communicator::RT_ERROR, tr("Disconnected."));
	this->setNotificationsEnabled(false);

	this->framesSetEnabled(false);

	// enable all tabs (capabilities of server are not known - disconnected)
	this->enableAllTabsAndMenus();
}

void MainWindow::handleConnectorDisconnected()
{
	log_connection.debug() << "Connector disconnected.";
	this->disconnect();


}

void MainWindow::recalculateSize()
{
	// Calculate optimal window size
	QSize sizeHint = this->sizeHint();
	sizeHint.setHeight(sizeHint.height() + 10);
	sizeHint.setWidth(sizeHint.width() + 10);
	QSize size;
	size.setHeight(this->height() > sizeHint.height() ? this->height() : sizeHint.height());
	size.setWidth(this->width() > sizeHint.width() ? this->width() : sizeHint.width());
	this->resize(size);
}

void MainWindow::handleReconnect()
{
	this->updateStatusBar(tr("Trying to reconnect."));
	this->reinitConnection();
}

void MainWindow::handleCapatibilitiesLoaded(const QList<Capability> &capabilities)
{
	this->updateTabsAndMenus(capabilities);
}

void MainWindow::handleSystrayNotificationMessageClicked()
{
	this->activateNotificationTab();
	this->ui->notificationsScrollArea->verticalScrollBar()->setValue(this->ui->notificationsScrollArea->verticalScrollBar()->maximum());
}

void MainWindow::handleSystrayNeighbourClicked()
{
	this->activateNeighboursTab();
	this->ui->neighboursScrollArea->verticalScrollBar()->setValue(0);
}

void MainWindow::handleNeighbourMenuItemToggled(bool checked)
{
	// get action
	QAction *action = qobject_cast<QAction *>(QObject::sender());
	const QString &name = action->property("interface_name").toString();
	if (appSettings.activeNeighbourDevices.contains(name)) {
		// removing
		if (!checked) {
			appSettings.activeNeighbourDevices.removeAll(name);
			log_neighbours.debug() << "Removing interface" << name << "from the filter";
		}
	} else {
		// adding
		if (checked) {
			appSettings.activeNeighbourDevices.append(name);
			log_neighbours.debug() << "Adding interface" << name << "to the filter";
		}
	}

	// remove from edit mode
	this->callNeighbourMacEditDisable();
	this->callNeighbourChat();
}

void MainWindow::handleNeighbourMacEdit(bool toEditMode)
{
	const QList<NeighbourWidget *> &widgets = this->ui->neighboursScrollWidget->findChildren<NeighbourWidget *>();
	if (toEditMode) {
		// disable neighbour queries
		this->enableNeighbourChat = false;
		NeighbourWidget *senderWidget = qobject_cast<NeighbourWidget *>(QObject::sender());
		foreach (NeighbourWidget *widget, widgets) {
			if (widget != senderWidget) {
				widget->setMacMapping(&appSettings.neighboursMacToName);
				widget->resetEditState();
			}
		}
	} else {
		// enable neighbour queries
		this->enableNeighbourChat = true;
		foreach (NeighbourWidget *widget, widgets) {
			widget->setMacMapping(&appSettings.neighboursMacToName);
			widget->resetEditState();
		}
	}
}

void MainWindow::handleNeighbourAdvancedViewToggled(bool advanced)
{
	appSettings.neighboursAdvancedView = advanced;
	const QList<NeighbourWidget *> &widgets = this->ui->neighboursScrollWidget->findChildren<NeighbourWidget *>();
	foreach (NeighbourWidget *widget, widgets) {
		widget->setBodyText(advanced);
	}
}

void MainWindow::handleGuardListenerConnected()
{
	this->trayIcon->showMessage(
		tr("Already running."), tr("to run more instances of spectator use '-m' parameter"),
		QSystemTrayIcon::Information, 1000 * appSettings.popupTime
	);
	this->trayIcon->disconnect(SIGNAL(messageClicked()));
}

void MainWindow::callNeighbourMacEditDisable()
{
	this->handleNeighbourMacEdit(false);
}

void MainWindow::setPasswordFinished(bool success)
{
	if (success) {
		this->updateStatusBar(Communicator::RT_SUCCESS, tr("The password has been set successfully."));
		log_control.debug() << "Password was set.";
		this->ui->password2LineEdit->clear();
		this->ui->passwordLineEdit->clear();
	} else {
		this->updateStatusBar(Communicator::RT_ERROR, tr("Setting password failed."));
		log_control.critical() << "Failed to set password.";
	}
}

void MainWindow::updaterCheckFinished(bool success)
{
	if (success) {
		this->updateStatusBar(Communicator::RT_SUCCESS, tr("The updater check has been triggered."));
		log_updater.debug() << "Updater check was triggered.";
	} else {
		this->updateStatusBar(Communicator::RT_ERROR, tr("Failed to trigger the updater check."));
		log_updater.debug() << "Updater check was triggered.";
	}
}

void MainWindow::setUpdaterFinished(bool success)
{
	if (success) {
		this->updateStatusBar(Communicator::RT_SUCCESS, tr("The user lists of updater were successfully set."));
		log_updater.debug() << "The user lists of updater were successfully set.";
	} else {
		this->updateStatusBar(Communicator::RT_ERROR, tr("Failed to set the user lists of updater."));
		log_updater.debug() << "Failed to set the user lists of updater.";
	}
}

void MainWindow::storeBackupFinished(const QByteArray &data)
{
	const QString &path = this->ui->backupLineEdit->text();
	QFileInfo fileInfo(path);
	if (fileInfo.exists()) {
		if (fileInfo.isFile()) {
			// override dialog else return
			QMessageBox::StandardButton res = QMessageBox::warning(
				this, tr("Overwrite file"), ("File already exists. Do you want to overwrite it?"),
				QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel
			);
			if (QMessageBox::Ok != res) {
				return;
			}
		} else {
			QMessageBox::critical(this, tr("file type error"), tr("'%1' is not a valid path to a file.").arg(path));
			log_control.critical() << QString("'%1' is not a file.").arg(path);
			return;
		}
	}
	QFile file(path);
	if(file.open(QFile::WriteOnly | QFile::Truncate)) {
		file.write(data);
		file.close();
		this->updateStatusBar(Communicator::RT_SUCCESS, tr("Backup stored."));
		log_control.debug() << QString("Backup was stored into '%1'.").arg(path).toStdString().c_str();
	} else {
		this->updateStatusBar(Communicator::RT_ERROR, tr("Failed to save backup file."));
		log_control.critical() << QString("Failed to save a file '%1'.").arg(path);
	}
}

void MainWindow::restoreBackupFinished(const QString &newIp)
{
	// update ip if present and different
	if (!newIp.isNull() && newIp != appSettings.server) {
		appSettings.server = newIp;
		// after backup restore the router will be restarted
		// program will lose connection
		// it reconnect to the new ip
		// so calling reconnect function is not needed
	}
	this->updateStatusBar(Communicator::RT_SUCCESS, tr("Backup restored."));
	log_control.debug() << QString("Backup restored from a file '%1'.").arg(this->ui->backupLineEdit->text()).toStdString().c_str();
}

void MainWindow::updateTabsAndMenus(const QList<Capability> &capabilities)
{
	for(QMap<int, QList<Capability> >::const_iterator i = this->tabCapabilities.begin(); i != this->tabCapabilities.end(); ++i) {
		foreach (const Capability &requiredCapability, i.value()) {
			bool present = false;
			foreach (const Capability &capability, capabilities) {
				if (capability.covers(requiredCapability)) {
					present = true;
					break;
				}
			}
			if (!present) {
				this->setTabAndMenusEnabled(i.key(), false);
			}
		}
	}
}

void MainWindow::enableAllTabsAndMenus()
{
	this->setTabAndMenusEnabled(TAB_IDX_NOTIFICATION, true);
	this->setTabAndMenusEnabled(TAB_IDX_NETHIST, true);
	this->setTabAndMenusEnabled(TAB_IDX_STATS, true);
	this->setTabAndMenusEnabled(TAB_IDX_UPDATER, true);
	this->setTabAndMenusEnabled(TAB_IDX_NEIGHBOURS, true);
}

void MainWindow::setTabAndMenusEnabled(int tabIndex, bool enabled)
{
	this->ui->mainTab->setTabEnabled(tabIndex, enabled);
	this->tabActions[tabIndex]->setEnabled(enabled);
}

void MainWindow::updateNeighbourDevices(const QList<QString> &devices)
{
	// Remove all devices and delete
	const QList<QAction *> &items = this->neighbourInterfaceMenu->findChildren<QAction *>();
	foreach (QAction *item, items) {
		delete item;
	}

	// Append new list
	foreach (const QString &device, devices) {
		QAction *action = new QAction(device, this->neighbourInterfaceMenu);
		action->setCheckable(true);
		action->setProperty("interface_name", device);
		action->setChecked(appSettings.activeNeighbourDevices.contains(device));
		QObject::connect(action, SIGNAL(toggled(bool)), this, SLOT(handleNeighbourMenuItemToggled(bool)));
		this->neighbourInterfaceMenu->addAction(action);
	}

	// Append interface without name
	QAction *action = new QAction(tr("Unknown"), this->neighbourInterfaceMenu);
	action->setCheckable(true);
	action->setProperty("interface_name", NEIGHBOURS_INTERFACE_WITHOUT_NAME);
	action->setChecked(appSettings.activeNeighbourDevices.contains(NEIGHBOURS_INTERFACE_WITHOUT_NAME));
	QObject::connect(action, SIGNAL(toggled(bool)), this, SLOT(handleNeighbourMenuItemToggled(bool)));
	this->neighbourInterfaceMenu->addAction(action);
}

QMenu *MainWindow::getMenu()
{
	return this->ui->menuSpectator;
}
