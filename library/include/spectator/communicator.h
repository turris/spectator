/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_COMMUNICATOR_H
#define SPECTATOR_COMMUNICATOR_H

#include "conn_consts.h"
#include "chat.h"
#include "spectator_lib.h"

#include <QTimer>
#include <QSslSocket>
#include <QSslConfiguration>

#define CHAT_TIMEOUT 5000  // 5 seconds

struct SPECTATOR_LIB_MODE QueuedChat {
	QSharedPointer<Chats::Base> chat;
	Communicator::ConnectorStates state;
	bool capabilityRequired;

	QueuedChat(QSharedPointer<Chats::Base> &chat, Communicator::ConnectorStates state, bool capabilityRequired = true)
		: chat(chat), state(state), capabilityRequired(capabilityRequired) {}
};

class SPECTATOR_LIB_MODE Connector : public QObject {
	Q_OBJECT

public:
	bool ready();
	void performInitialChat(QSharedPointer<Chats::Initial> &chat);
	void initiateConnection();
	void performChat(
		QSharedPointer<Chats::Base> &chat,
		Communicator::ConnectorStates requiredState = Communicator::CONNECTOR_READY,
		bool requireCapabilities = true
	);
	Connector(const QString &server, int port, const QString &tokenPath);
	~Connector();

private:
	QList<Capability> capabilities;
	QSslSocket socket;
	QSslConfiguration configuration;
	QString server;
	int port;
	QString tokenPath;
	QByteArray readBuffer;
	QList<QueuedChat> chatQueue;
	Communicator::ConnectorStates state;
	QTimer chatTimer;

	bool setConfiguration(const QString &tokenPath);
	void connectChat(QSharedPointer<Chats::Base> &chat);
	void popChatQueue();
	void resetChatTimer();
	bool capabilityCheck(QSharedPointer<Chats::Base> &chat);
	bool startChat(QSharedPointer<Chats::Base> &chat, bool requireCapabilities);
	void startNextChat();

signals:
	void report(Communicator::ReportType type, const QString &msg);
	void readMessage(const QString &msg);
	void connected();
	void disconnected();

private slots:
	void handleWriteMessage(const QString &msg);
	void handleChatFinished(bool);
	void handleSocketError(QAbstractSocket::SocketError error);
	void handleSslErrors(const QList<QSslError> &errors);
	void handleSocketStateChange(QAbstractSocket::SocketState socketState);
	void handleStateChange(Communicator::ConnectorStates newState);
	void handleSocketConnected();
	void handleSocketDisconnected();
	void handleReadReady();
	void handleEncrypted();
	void handleReport(Communicator::ReportType type, const QString &msg);
	void handleChatTimeOut();
	void handleStoreCapabilities(const QList<Capability> &capabilities);
	void handleNetconfErrors(const QList<NetconfError> &errors);
};

#endif
