QT += core gui network widgets xml printsupport

win32 {
	isEmpty(WIN_CONSOLE) {
	} else {
		CONFIG += console
	}
	RC_ICONS = 'img/spectator.ico'

	CONFIG += skip_target_version_ext
}

TARGET = turris-spectator

VERSION="$$cat(../VERSION)"
DEFINES += "SPECTATOR_VERSION=\\\"$${VERSION}\\\""
message("Spectator GUI version $${VERSION}")
TEMPLATE = app
ICON = 'img/spectator.ico'

APP_DIR = "."
SRC_DIR = "$${APP_DIR}/src"
HDR_DIR = "$${APP_DIR}/src"

UI_DIR = "$${APP_DIR}/ui"
QRC_DIR = "$${APP_DIR}/qrc"
TRANSLATION_DIR = "$${APP_DIR}/qrc"

MOC_DIR = build
RCC_DIR = build
OBJECTS_DIR = build

DESTDIR = ../bin

INCLUDEPATH += "./$${HDR_DIR}" "../library/include" "../qcustomplot/"
DEPENDPATH += . "./$${HDR_DIR}" "../library/include" "../qcustomplot/"
LIBS += -L../lib -lturris-spectator

SOURCES += \
	"$${SRC_DIR}"/main.cpp \
	"$${SRC_DIR}"/mainwindow.cpp \
	"$${SRC_DIR}"/settings.cpp \
	"$${SRC_DIR}"/settings_dialog.cpp \
	"$${SRC_DIR}"/settings_gui.cpp \
	../qcustomplot/qcustomplot.cpp \
	"$${SRC_DIR}"/custom_widgets.cpp \
	"$${SRC_DIR}"/application.cpp


HEADERS += \
	"$${HDR_DIR}"/mainwindow.h \
	"$${HDR_DIR}"/settings.h \
	"$${HDR_DIR}"/settings_dialog.h \
	"$${HDR_DIR}"/settings_gui.h \
	../qcustomplot/qcustomplot.h \
	"$${HDR_DIR}"/custom_widgets.h \
	"$${HDR_DIR}"/application.h


FORMS += \
	"$${UI_DIR}"/mainwindow.ui \
	"$${UI_DIR}"/settings.ui

RESOURCES += \
	"$${QRC_DIR}"/icons.qrc \
	"$${QRC_DIR}"/translations.qrc

TRANSLATIONS += \
	"$${TRANSLATION_DIR}"/spectator_cs.ts

win32 {
	message(Copying Qt translation from $$[QT_INSTALL_DATA].)
	if(!system(copy "$$[QT_INSTALL_DATA]\\translations\\qtbase_cs.qm" "lang\\qtbase_cs.qm")) {
		error("Failed to copy system translation. Aborting.")
	}
}

unix {
	message(Copying Qt translation from $$[QT_INSTALL_DATA].)
	if(!system(cp "$$[QT_INSTALL_DATA]/translations/qtbase_cs.qm" "lang/qtbase_cs.qm")) {
		error("Failed to copy system translation. Aborting.")
	}
}

isEmpty(PREFIX) {
	unix {
		PREFIX = "/usr/local"
	}
	win32 {
		PREFIX = "C:\\spectator"
	}
}

target.path = "$${PREFIX}/bin"

INSTALLS += target

linux {
	desktop.files += spectator.desktop
	desktop.path = "$${PREFIX}/share/applications"
	icon.files = "$${APP_DIR}/img/spectator.png"
	icon.path = "$${PREFIX}/share/pixmaps"
	INSTALLS += icon desktop
}
