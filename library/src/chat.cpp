/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spectator/chat.h"

namespace ChatStatus {

enum statuses {
	started,
	quitting
};

}

namespace {

QDomElement buildXmlHeaderAndRpc(QDomDocument &xml, int rpcMessageId)
{
	// xml append header
	xml.appendChild(xml.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"UTF-8\""));
	// append root element
	QDomElement root(xml.createElementNS(NS_NETCONF_BASE, "rpc"));
	root.setAttribute("message-id", rpcMessageId);
	xml.appendChild(root);
	return root;
}

}


namespace Chats {

Base::Base()
{
	qsrand(QDateTime::currentMSecsSinceEpoch());
	this->rpcMessageId = qrand();
	this->status = ChatStatus::started;
}

bool Base::parseXml(QDomDocument &xml, const QString &msg)
{
	if (!xml.setContent(msg, true)) {
		emit this->report(Communicator::RT_ERROR, tr("Failed to parse obtained data."));
		emit this->finished(false);
		return false;
	}
	return true;
}

bool Base::checkRpcMessageId(QDomDocument &xml)
{
	const QDomNodeList &rpcReplies = xml.elementsByTagNameNS(NS_NETCONF_BASE, "rpc-reply");
	if (rpcReplies.isEmpty()) {
		emit this->report(Communicator::RT_ERROR, tr("Failed to parse RPC reply message."));
		emit this->finished(false);
		return false;
	}
	const QDomAttr &attr = rpcReplies.at(0).attributes().namedItem("message-id").toAttr();
	if (attr.isNull() || attr.value() != QString::number(this->rpcMessageId)) {
		emit this->report(Communicator::RT_ERROR, tr("RPC message-id mismatch."));
		emit this->finished(false);
		return false;
	}
	return true;
}

bool Base::checkRpcMessageOk(QDomDocument &xml, bool reportError)
{
	const QDomNode &rpcReply = xml.elementsByTagNameNS(NS_NETCONF_BASE, "rpc-reply").at(0);
	if (rpcReply.firstChild().localName() != "ok") {
		if (reportError) {
			// Report netconf error if present
			this->reportNetconfErrors(xml);

			emit this->report(Communicator::RT_ERROR, tr("Incorrect RPC response."));
			emit this->finished(false);
		}
		return false;
	}
	return true;
}

bool Base::parseNetconfErrors(QDomDocument &xml, QList<NetconfError> &outErrors)
{
	const QDomNodeList &errorElements = xml.elementsByTagNameNS(NS_NETCONF_BASE, "rpc-error");
	if (errorElements.isEmpty()) {
		return false;
	}

	for (int i = 0; i < errorElements.count(); ++i) {
		const QDomNode &node = errorElements.at(i);
		NetconfError record;

		const QDomElement &type = node.firstChildElement("error-type");
		record.type = type.text();

		const QDomElement &tag = node.firstChildElement("error-tag");
		record.tag = tag.text();

		const QDomElement &severity = node.firstChildElement("error-severity");
		record.serverity = severity.text();

		const QDomElement &appTag = node.firstChildElement("error-app-tag");
		record.appTag = appTag.text();

		const QDomElement &errorPath = node.firstChildElement("error-path");
		record.path = errorPath.text();

		const QDomElement &errorMessage = node.firstChildElement("error-message");
		record.message = errorMessage.text();

		const QDomElement &errorInfo = node.firstChildElement("error-info");
		const QDomNodeList &errorInfoList = errorInfo.childNodes();
		for (int j = 0; j < errorInfoList.count(); ++j) {
			const QDomNode &subnode = errorInfoList.at(j);
			NetconfErrorInfoElement errorInfo;
			errorInfo.name = subnode.localName();
			errorInfo.content = subnode.toElement().text();
			record.info.append(errorInfo);
		}
		outErrors.append(record);
	}

	return true;
}

bool Base::reportNetconfErrors(QDomDocument &xml)
{
	QList<NetconfError> errors;
	if (this->parseNetconfErrors(xml, errors)) {
		emit this->netconfErrorsOccured(errors);
		return true;
	}

	// No errors occured
	return false;
}

void Base::finish(bool res)
{
	this->status = ChatStatus::quitting;
	emit this->finished(res);
}

QList<Capability> Initial::requiredCapabilities = QList<Capability>();

void Initial::talk()
{
	// nothing to be done here (waiting for the response here)
}

QString Initial::capibilityMessage()
{
	QDomDocument xml;

	// xml append header
	xml.appendChild(xml.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"UTF-8\""));
	// append root element
	QDomElement root(xml.createElementNS(NS_NETCONF_BASE, "hello"));
	xml.appendChild(root);

	// append capabilities list
	QDomElement capabilities(xml.createElement("capabilities"));
	root.appendChild(capabilities);

	// append capability
	QDomElement capability(xml.createElement("capability"));
	capability.appendChild(xml.createTextNode(CAPABILITY_NETCONF_BASE));
	capabilities.appendChild(capability);

	return xml.toString();
}

void Initial::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Parse capabilities
	const QDomNodeList &capabilities = xml.elementsByTagNameNS(NS_NETCONF_BASE, "capability");
	if (!capabilities.isEmpty()) {
		QList<Capability> list;
		for (int i = 0; i < capabilities.count(); ++i) {
			list.append(Capability(capabilities.at(i).toElement().text()));
		}
		emit this->exportCapabilities(list);
	} else {
		emit this->exportCapabilities(QList<Capability>());
	}

	switch (status) {
		case ChatStatus::started:
			emit this->report(Communicator::RT_SUCCESS, tr("An initial message received."));
			emit this->writeMessage(Initial::capibilityMessage());
			emit this->communicationStatusUpdate(Communicator::CONNECTOR_READY);
			this->finish(true);
			break;

		case ChatStatus::quitting:
			this->finish(true);
			break;
	}
}


QList<Capability> GetNotifications::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_NOTIFICATION_BASE, QString());

QString GetNotifications::getNotificationsMessage(int rpcMessageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append get
	QDomElement get(xml.createElement("get"));
	root.appendChild(get);

	// append filter
	QDomElement filter(xml.createElement("filter"));
	filter.setAttribute("type", "subtree");
	get.appendChild(filter);

	QDomElement messages(xml.createElementNS(CAPABILITY_NOTIFICATION_BASE, "messages"));
	filter.appendChild(messages);

	return xml.toString();
}

void GetNotifications::talk()
{
	emit this->writeMessage(GetNotifications::getNotificationsMessage(this->rpcMessageId));
}

bool compareNotifications(const ParsedNotification &n1, const ParsedNotification &n2)
{
	return n1.time < n2.time;
}

void GetNotifications::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	const QDomNodeList &messages = xml.elementsByTagNameNS(NS_NOTIFICATION, "message");
	if (!messages.isEmpty()) {
		QList<ParsedNotification> list;
		for (int i = 0; i < messages.count(); ++i) {
			const QDomNode &messageNode = messages.at(i);
			if (messageNode.firstChildElement("displayed").isNull()) {  // filters removed
				ParsedNotification notification;
				notification.id = messageNode.firstChildElement("id").text();

				QString noLangText;
				const QDomNodeList &bodyElements = messageNode.toElement().elementsByTagNameNS(NS_NOTIFICATION, "body");
				for (int j = 0; j < bodyElements.size(); ++j) {
					const QDomElement &bodyElement = bodyElements.at(j).toElement();
					if (bodyElement.attribute("lang") == this->lang) {
						notification.body = bodyElement.text();
						break;
					} else {
						noLangText = bodyElement.text();
					}
				}


				if (notification.body.isNull())  // if the required lang is not present display at least some language
					notification.body = noLangText;

				notification.severity = messageNode.firstChildElement("severity").text();
				notification.time = QDateTime::fromTime_t(messageNode.firstChildElement("timestamp").text().toInt());
				list.append(notification);
			}
		}
		// sort notifications by id
		qSort(list.begin(), list.end(), compareNotifications);
		emit this->exportNotifications(list);
	} else {
		emit this->exportNotifications(QList<ParsedNotification>());
	}

	emit this->report(Communicator::RT_SUCCESS, tr("Notifications were downloaded."));
	this->finish(true);
}

QList<Capability> DeleteNotification::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_NOTIFICATION_BASE, QString());

QString DeleteNotification::deleteNotificationMessage(int rpcMessageId, const QString &messageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append display
	QDomElement display(xml.createElementNS(CAPABILITY_NOTIFICATION_BASE, "display"));
	root.appendChild(display);

	// append filter
	QDomElement message_id(xml.createElement("message-id"));
	message_id.appendChild(xml.createTextNode(messageId));
	display.appendChild(message_id);

	return xml.toString();
}

void DeleteNotification::talk()
{
	emit this->writeMessage(DeleteNotification::deleteNotificationMessage(this->rpcMessageId, this->messageId));
}

void DeleteNotification::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// Check Ok
	if (this->checkRpcMessageOk(xml)) {
		emit this->notificationDeleted(this->messageId);
		this->finish(true);
	}
}


QList<Capability> RebootRouter::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_MAINTAIN_BASE, QString());

QString RebootRouter::rebootRouterMessage(int rpcMessageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append display
	QDomElement display(xml.createElementNS(CAPABILITY_MAINTAIN_BASE, "reboot"));
	root.appendChild(display);

	return xml.toString();
}

void RebootRouter::talk()
{
	emit this->writeMessage(RebootRouter::rebootRouterMessage(this->rpcMessageId));
}

void RebootRouter::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// Check Ok
	if (this->checkRpcMessageOk(xml)) {
		emit this->rebootingRouter();
		this->finish(true);
	}
}


QList<Capability> GetNethist::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_NETHIST_BASE, QString());

QString GetNethist::getNethistMessage(int rpcMessageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append get
	QDomElement get(xml.createElement("get"));
	root.appendChild(get);

	// append filter
	QDomElement filter(xml.createElement("filter"));
	filter.setAttribute("type", "subtree");
	get.appendChild(filter);

	QDomElement messages(xml.createElementNS(CAPABILITY_NETHIST_BASE, "nethist"));
	filter.appendChild(messages);

	return xml.toString();
}

void GetNethist::talk()
{
	emit this->writeMessage(GetNethist::getNethistMessage(this->rpcMessageId));
}

bool compareNethist(const NethistRecord &n1, const NethistRecord &n2)
{
	return n1.time < n2.time;
}

void GetNethist::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// Parse the nethist
	const QDomNodeList &snapshots = xml.elementsByTagNameNS(NS_NETHIST, "snapshot");
	if (!snapshots.isEmpty()) {
		QList<NethistRecord> list;
		for (int i = 0; i < snapshots.count(); ++i) {
			NethistRecord record;
			if (!this->parseRecord(snapshots.at(i), record)) {
				emit this->report(Communicator::RT_ERROR, tr("Incorrect Nethist XML format."));
				this->finish(false);
				return;
			}
			list.append(record);
		}
		// sort records by time
		qSort(list.begin(), list.end(), compareNethist);

		emit this->exportNethist(list);
	} else {
		emit this->exportNethist(QList<NethistRecord>());
	}

	this->finish(true);
}

bool GetNethist::parseRecord(const QDomNode &node, NethistRecord &record)
{
	// parse time
	record.time = QDateTime::fromTime_t(node.firstChildElement("time").text().toInt());

	// parse network interfaces
	QList<NethistInterfaceRecord> interfaceRecords;
	const QDomNodeList &interfaces = node.firstChildElement("network").toElement().elementsByTagNameNS(NS_NETHIST, "interface");
	for (int i = 0; i < interfaces.count(); ++i) {
		NethistInterfaceRecord interfaceRecord;
		const QDomNode &interfaceNode = interfaces.at(i);
		interfaceRecord.name = interfaceNode.firstChildElement("name").text();
		interfaceRecord.rx = interfaceNode.firstChildElement("rx").text().toULongLong();
		interfaceRecord.tx = interfaceNode.firstChildElement("tx").text().toULongLong();
		interfaceRecords.append(interfaceRecord);
	}
	record.interfaces = interfaceRecords;

	// cpu
	record.cpuLoad = node.firstChildElement("cpu").firstChildElement("load").text().toDouble();

	// temperatore
	const QDomNode &temperatureNode = node.firstChildElement("temperature");
	record.cpuTemperature = temperatureNode.firstChildElement("cpu").text().toInt();
	record.boardTemperature = temperatureNode.firstChildElement("board").text().toInt();

	// memory
	const QDomNode &memoryNode = node.firstChildElement("memory");
	record.totalMemory = memoryNode.firstChildElement("memtotal").text().toInt();
	record.freeMemory = memoryNode.firstChildElement("memfree").text().toInt();
	record.buffersMemory = memoryNode.firstChildElement("buffers").text().toInt();
	record.cachedMemory = memoryNode.firstChildElement("cached").text().toInt();

	// disk
	const QDomNode &diskNode = node.firstChildElement("rootfs");
	record.usedDiskSpace = diskNode.firstChildElement("used").text().toInt();
	record.availableDiskSpace = diskNode.firstChildElement("available").text().toInt();

	return true;
}


QList<Capability> SetPassword::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_PASSWORD_BASE, QString());

void SetPassword::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// Check Ok
	if (this->checkRpcMessageOk(xml)) {
		this->finish(true);
	}

}

QString SetPassword::setPasswordMessage(int rpcMessageId, const QString &newPassword)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append set
	QDomElement set(xml.createElementNS(CAPABILITY_PASSWORD_BASE, "set"));
	root.appendChild(set);

	// append user
	QDomElement user(xml.createElement("user"));
	user.appendChild(xml.createTextNode("root"));
	set.appendChild(user);

	// append password
	QDomElement password(xml.createElement("password"));
	password.appendChild(xml.createTextNode(newPassword));
	set.appendChild(password);

	return xml.toString();
}

void SetPassword::talk()
{
	emit this->writeMessage(SetPassword::setPasswordMessage(this->rpcMessageId, this->password.toHtmlEscaped()));
}


QList<Capability> StoreBackup::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_MAINTAIN_BASE, QString());

void StoreBackup::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// Look for rpc-error
	if (this->reportNetconfErrors(xml)) {
		this->finish(false);
		return;
	}

	bool res;
	const QDomNodeList &data = xml.elementsByTagNameNS(NS_MAINTAIN, "data");
	if (data.length() == 1) {
		QByteArray base64_data(data.at(0).toElement().text().toStdString().c_str());
		emit this->storeData(QByteArray::fromBase64(base64_data));
		res = true;
	} else {
		res = false;
	}

	this->finish(res);
}

QString StoreBackup::storeBackupMessage(int rpcMessageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append config-backup
	QDomElement config_backup(xml.createElementNS(CAPABILITY_MAINTAIN_BASE, "config-backup"));
	root.appendChild(config_backup);

	return xml.toString();
}

void StoreBackup::talk()
{
	emit this->writeMessage(StoreBackup::storeBackupMessage(this->rpcMessageId));
}


QList<Capability> RestoreBackup::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_MAINTAIN_BASE, QString());

void RestoreBackup::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	bool res = true;
	// Look for rpc-error
	if (this->reportNetconfErrors(xml)) {
		emit this->report(Communicator::RT_ERROR, tr("Failed to restore the backup."));
		res = false;
	} else {
		const QDomNodeList &ips = xml.elementsByTagNameNS(NS_MAINTAIN, "new-ip");
		if (ips.length() == 1) {
			emit this->dataRestored(ips.at(0).toElement().text());
		} else {
			// new-ip tag is not mandatory
			emit this->dataRestored(QString());
		}
	}

	this->finish(res);
}

QString RestoreBackup::restoreBackupMessage(int rpcMessageId, const QByteArray &data)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append config-restore
	QDomElement config_restore(xml.createElementNS(CAPABILITY_MAINTAIN_BASE, "config-restore"));
	root.appendChild(config_restore);

	// append data
	QDomElement data_element(xml.createElement("data"));
	data_element.appendChild(xml.createTextNode(QString(data.toBase64())));
	config_restore.appendChild(data_element);

	return xml.toString();
}

void RestoreBackup::talk()
{
	emit this->writeMessage(RestoreBackup::restoreBackupMessage(this->rpcMessageId, this->data));
}

QList<Capability> GetStats::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_STATS_BASE, QString());

void GetStats::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;


	const QDomNodeList &stats = xml.elementsByTagNameNS(NS_STATS, "stats");
	if (stats.count() != 1) {
		emit this->report(Communicator::RT_ERROR, tr("Incorrect Stat XML format."));
		this->finish(false);
		return;

	}

	StatsRecord record;
	const QDomNode &statsNode = stats.at(0);
	record.model = statsNode.firstChildElement("model").text();
	record.boardName = statsNode.firstChildElement("board-name").text();
	record.kernelVersion= statsNode.firstChildElement("kernel-version").text();
	record.osVersion = statsNode.firstChildElement("turris-os-version").text();
	record.uptime = statsNode.firstChildElement("uptime").text().toFloat();
	record.hostname = statsNode.firstChildElement("hostname").text();
	const QDomElement &firewallSending = statsNode.firstChildElement("firewall-sending");
	record.firewallStatus = firewallSending.firstChildElement("status").text();
	record.firewallAge = firewallSending.firstChildElement("age").text().toInt();
	const QDomElement &ucollectSending = statsNode.firstChildElement("ucollect-sending");
	record.ucollectStatus = ucollectSending.firstChildElement("status").text();
	record.ucollectAge = ucollectSending.firstChildElement("age").text().toInt();

	const QDomNodeList &nodes = statsNode.firstChildElement("interfaces").toElement().elementsByTagNameNS(NS_STATS, "interface");
	for (int i = 0; i < nodes.size(); ++i) {
		const QDomElement &interface = nodes.at(i).toElement();
		StatsInterface interfaceRecord;
		interfaceRecord.down = interface.firstChildElement("down").isNull() ? false : true;
		interfaceRecord.up = interface.firstChildElement("up").isNull() ? false : true;
		interfaceRecord.use = interface.firstChildElement("use").text();
		interfaceRecord.name = interface.firstChildElement("name").text();


		const QDomNodeList &addressNodes = interface.elementsByTagNameNS(NS_STATS, "address");
		for (int j = 0; j < addressNodes.size(); ++j) {
			const QDomElement &address = addressNodes.at(j).toElement();
			StatsAddress statsAddress;
			statsAddress.address = address.firstChildElement("address").text();
			statsAddress.type = address.firstChildElement("type").text();

			// <address>...<address> </address>...</address> :-(
			// skip the inner address
			if (statsAddress.address.isEmpty() || statsAddress.type.isEmpty()) {
				continue;
			}
			interfaceRecord.addresses.append(statsAddress);
		}

		record.interfaces.append(interfaceRecord);

	}

	emit this->exportStats(record);

	this->finish(true);
}

QString GetStats::getStatsMessage(int rpcMessageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append get
	QDomElement get(xml.createElement("get"));
	root.appendChild(get);

	// append filter
	QDomElement filter(xml.createElement("filter"));
	filter.setAttribute("type", "subtree");
	get.appendChild(filter);

	// append stats
	QDomElement stats(xml.createElementNS(CAPABILITY_STATS_BASE, "stats"));
	filter.appendChild(stats);

	return xml.toString();
}

void GetStats::talk()
{
	emit this->writeMessage(GetStats::getStatsMessage(this->rpcMessageId));
}


QList<Capability> GetSerial::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_REGISTRATION_BASE, QString());

void GetSerial::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// Export serial
	bool res;
	const QDomNodeList &serials = xml.elementsByTagNameNS(NS_REGISTRATION, "serial");
	if (serials.length() == 1) {
		emit this->exportSerial(serials.at(0).toElement().text());
		res = true;
	} else {
		res = false;
	}

	this->finish(res);
}

QString GetSerial::getSerialMessage(int rpcMessageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append serial
	QDomElement serial(xml.createElementNS(CAPABILITY_REGISTRATION_BASE, "serial"));
	root.appendChild(serial);

	return xml.toString();
}

void GetSerial::talk()
{
	emit this->writeMessage(GetSerial::getSerialMessage(this->rpcMessageId));
}


QList<Capability> GetUpdaterData::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_UPDATER_BASE, QString());

void GetUpdaterData::readMessage(const QString &msg)
{
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	const QDomNodeList &updater = xml.elementsByTagNameNS(NS_UPDATER, "updater");
	if (updater.count() != 1) {
		emit this->report(Communicator::RT_ERROR, tr("Incorrect updater XML format."));
		this->finish(false);
		return;
	}

	UpdaterDataRecord record;
	const QDomElement &running = updater.at(0).firstChildElement("running");
	record.status = running.isNull() ? "inactive" : running.text();

	QList<PkgListRecord> userListRecords;
	const QDomNodeList &userLists = xml.elementsByTagNameNS(NS_UPDATER, "pkg-list");
	for (int i = 0; i < userLists.count(); ++i) {
		PkgListRecord pkgRecord;
		const QDomNode &userListNode = userLists.at(i);
		pkgRecord.name = userListNode.firstChildElement("name").text();

		QString noLangText;
		// Load multilang title
		const QDomNodeList &titleNodes = userListNode.toElement().elementsByTagNameNS(NS_UPDATER, "title");
		for (int i = 0; i < titleNodes.size(); ++i) {
			const QDomElement &titleTextElement = titleNodes.at(i).toElement();
			if (titleTextElement.attribute("lang") == this->lang) {
				pkgRecord.title = titleTextElement.text();
				break;
			} else {
				noLangText = titleTextElement.text();
			}
		}

		if (pkgRecord.title.isNull())
			pkgRecord.title = noLangText;

		// Load multilang description
		const QDomNodeList &descriptionNodes = userListNode.toElement().elementsByTagNameNS(NS_UPDATER, "description");
		for (int i = 0; i < descriptionNodes.size(); ++i) {
			const QDomElement &descriptionTextElement = descriptionNodes.at(i).toElement();
			if (descriptionTextElement.attribute("lang") == this->lang) {
				pkgRecord.description= descriptionTextElement.text();
				break;
			} else {
				noLangText = descriptionTextElement.text();
			}
		}

		if (pkgRecord.description.isNull())
			pkgRecord.description = noLangText;

		// Get activated flag
		const QDomElement &activated = userListNode.firstChildElement("activated");
		pkgRecord.activated = !activated.isNull();

		userListRecords.append(pkgRecord);
	}

	const QDomNodeList &lastActivity = xml.elementsByTagNameNS(NS_UPDATER, "last_activity");
	if (lastActivity.count() > 1) {
		emit this->report(Communicator::RT_ERROR, tr("Incorrect Stat XML format."));
		this->finish(false);
		return;
	}

	if (lastActivity.count() == 1) {
		const QDomNodeList &activities = lastActivity.at(0).childNodes();
		for (int i = 0; i < activities.count(); ++i) {
			const QDomElement &activity = activities.at(i).toElement();
			QString packageName, version;
			foreach (const QString &str, activity.text().split(" ")) {
				if (packageName.isEmpty()) {
					packageName = str;
				} else {
					version += str;
				}
			}
			UpdaterActivityRecord activityRecord;
			activityRecord.action = activity.tagName();
			activityRecord.packageName = packageName;
			activityRecord.packageVersion = version;
			record.lastActivities.append(activityRecord);
		}
	}

	record.userLists = userListRecords;
	emit this->exportLists(record);

	this->finish(true);
}

QString GetUpdaterData::getUpdaterDataMessage(int rpcMessageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append get
	QDomElement get(xml.createElement("get"));
	root.appendChild(get);

	// append filter
	QDomElement filter(xml.createElement("filter"));
	filter.setAttribute("type", "subtree");
	get.appendChild(filter);

	// append updater
	QDomElement updater(xml.createElementNS(CAPABILITY_UPDATER_BASE, "updater"));
	filter.appendChild(updater);

	return xml.toString();
}

void GetUpdaterData::talk()
{
	emit this->writeMessage(GetUpdaterData::getUpdaterDataMessage(this->rpcMessageId));
}


QList<Capability> CheckUpdater::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_UPDATER_BASE, QString());

void CheckUpdater::readMessage(const QString &msg) {
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// Check Ok
	// Don't report any extra error info
	this->finish(this->checkRpcMessageOk(xml, false));
}

QString CheckUpdater::checkUpdaterMessage(int rpcMessageId)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append display
	QDomElement display(xml.createElementNS(CAPABILITY_UPDATER_BASE, "check"));
	root.appendChild(display);

	return xml.toString();
}

void CheckUpdater::talk()
{
	emit this->writeMessage(CheckUpdater::checkUpdaterMessage(this->rpcMessageId));
}


QList<Capability> SetUpdater::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_UPDATER_BASE, QString());

void SetUpdater::readMessage(const QString &msg) {
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// Check Ok
	if (this->checkRpcMessageOk(xml)) {
		this->finish(true);
	}
}

QString SetUpdater::setUpdaterMessage(int rpcMessageId, const QList<QString> &mergeList, const QList<QString> &removeList)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append edit-config
	QDomElement edit_config(xml.createElement("edit-config"));
	root.appendChild(edit_config);

	// append target
	QDomElement target(xml.createElement("target"));
	target.appendChild(xml.createElement("running"));
	edit_config.appendChild(target);

	// append config
	QDomElement config(xml.createElement("config"));
	edit_config.appendChild(config);

	// append updater-config
	QDomElement updater_config(xml.createElementNS(CAPABILITY_UPDATER_BASE, "updater-config"));
	config.appendChild(updater_config);

	// append active_lists
	QDomElement active_lists(xml.createElement("active-lists"));
	//active_lists.setPrefix("nc");
	updater_config.appendChild(active_lists);

	foreach (const QString &listName, mergeList) {
		// append user-list
		QDomElement user_list(xml.createElement("user-list"));
		QDomAttr attr_nc(xml.createAttribute("xmlns:nc"));
		attr_nc.setValue(NS_NETCONF_BASE);
		QDomAttr attr_merge(xml.createAttribute("nc:operation"));
		attr_merge.setValue("merge");
		user_list.setAttributeNode(attr_nc);
		user_list.setAttributeNode(attr_merge);
		active_lists.appendChild(user_list);

		// append name
		QDomElement name_element(xml.createElement("name"));
		name_element.appendChild(xml.createTextNode(listName));
		user_list.appendChild(name_element);
	}

	foreach (const QString &listName, removeList) {
		// append user-list
		QDomElement user_list(xml.createElement("user-list"));
		QDomAttr attr_nc(xml.createAttribute("xmlns:nc"));
		attr_nc.setValue(NS_NETCONF_BASE);
		QDomAttr attr_remove(xml.createAttribute("nc:operation"));
		attr_remove.setValue("remove");
		user_list.setAttributeNode(attr_nc);
		user_list.setAttributeNode(attr_remove);
		active_lists.appendChild(user_list);

		// append name
		QDomElement name_element(xml.createElement("name"));
		name_element.appendChild(xml.createTextNode(listName));
		user_list.appendChild(name_element);
	}

	return xml.toString();
}

void SetUpdater::talk()
{
	emit this->writeMessage(SetUpdater::setUpdaterMessage(this->rpcMessageId, this->mergeList, this->removeList));
}


QList<Capability> GetNeighbours::requiredCapabilities = QList<Capability>() << Capability(CAPABILITY_NEIGHBOURS_BASE, QString());

void GetNeighbours::readMessage(const QString &msg) {
	// Parse xml
	QDomDocument xml;
	if (!this->parseXml(xml, msg))
		return;

	// Check message id
	if (!this->checkRpcMessageId(xml))
		return;

	// read neighbours
	const QDomNodeList &neighbours = xml.elementsByTagNameNS(NS_NEIGHBOURS, "neighbour");
	if (!neighbours.isEmpty()) {
		QList<NeighbourRecord> result;
		for (int i = 0; i < neighbours.size(); ++i) {
			NeighbourRecord neighbourRecord;
			// read mac address
			const QDomNode &neighbourNode = neighbours.at(i);
			const QDomElement &macAddress = neighbourNode.firstChildElement("mac-address");
			neighbourRecord.macAddress = macAddress.text();

			// read interface
			const QDomElement &interface = neighbourNode.firstChildElement("interface");
			neighbourRecord.interfaceName = interface.text();

			// read ips
			const QDomNodeList &nodes = neighbourNode.toElement().elementsByTagNameNS(NS_NEIGHBOURS, "ip-address");
			for (int i = 0; i < nodes.size(); ++i) {
				const QDomElement &ipAddress = nodes.at(i).toElement();
				NeighbourIpRecord ipRecord;

				ipRecord.ipAddress = ipAddress.firstChildElement("ip").text();
				const QDomElement &count = ipAddress.firstChildElement("connection-count");
				ipRecord.connectionCount = count.isNull() ? 0 : count.text().toInt();
				ipRecord.nud = ipAddress.firstChildElement("nud").text();
				const QDomElement &lease = ipAddress.firstChildElement("dhcp-lease");
				ipRecord.dhcpLease = lease.isNull() ? QDateTime() : QDateTime::fromTime_t(lease.text().toInt());
				ipRecord.hostname = ipAddress.firstChildElement("hostname").text();

				const QDomElement &statistics = ipAddress.firstChildElement("statistics");
				if (!statistics.isNull()) {
					const QDomElement &used = statistics.firstChildElement("used");
					ipRecord.usedSince = used.isNull() ? -1 : used.text().toInt();
					const QDomElement &confirmed = statistics.firstChildElement("confirmed");
					ipRecord.confirmedSince = confirmed.isNull() ? -1 : confirmed.text().toInt();
					const QDomElement &updated = statistics.firstChildElement("updated");
					ipRecord.updatedSince = updated.isNull() ? -1 : updated.text().toInt();
				}

				neighbourRecord.ips.append(ipRecord);
			}
			result.append(neighbourRecord);
		}
		emit this->exportNeighbours(result);
	} else {
		emit this->exportNeighbours(QList<NeighbourRecord>());
	}

	this->finish(true);
}

QString GetNeighbours::getNeighbourMessage(int rpcMessageId, const QList<QString> &interfaces)
{
	QDomDocument xml;

	QDomElement root(buildXmlHeaderAndRpc(xml, rpcMessageId));

	// append get
	QDomElement get(xml.createElement("get"));
	root.appendChild(get);

	// append filter
	QDomElement filter(xml.createElement("filter"));
	filter.setAttribute("type", "subtree");
	get.appendChild(filter);

	QDomElement messages(xml.createElementNS(CAPABILITY_NEIGHBOURS_BASE, "neighbours"));
	filter.appendChild(messages);

	foreach (const QString &interface, interfaces) {
		QDomElement neighbour(xml.createElement("neighbour"));
		messages.appendChild(neighbour);
		QDomElement interfaceDom(xml.createElement("interface"));
		neighbour.appendChild(interfaceDom);
		interfaceDom.appendChild(xml.createTextNode(interface));
	}

	return xml.toString();
}

void GetNeighbours::talk()
{
	emit this->writeMessage(GetNeighbours::getNeighbourMessage(this->rpcMessageId, this->interfaces));
}

}
