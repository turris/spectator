/*
 * Spectator - a desktop app which communicates with router Turris
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"

int main(int argc, char *argv[])
{
	// Needed for QSettings
	QCoreApplication::setApplicationName("Spectator");
	QCoreApplication::setOrganizationDomain("cz.nic");
	QCoreApplication::setOrganizationName("cznic");

	QCoreApplication::setApplicationVersion(SPECTATOR_VERSION);

#ifdef Q_OS_WIN
	const QString &username = qgetenv("USERNAME");
#else
	const QString &username = qgetenv("USER");
#endif

	SpectatorApplication a(argc, argv, username);


	return a.proceed ? a.exec() : 1;
}
