/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_LOGGING_H
#define SPECTATOR_LOGGING_H

#include "spectator_lib.h"

#include <QMessageLogger>
#include <QLoggingCategory>
#include <QFile>

#define LEVEL_DEBUG "DEBUG"
#define LEVEL_INFO "INFO"
#define LEVEL_FATAL "FATAL"
#define LEVEL_WARN "WARNING"
#define LEVEL_CRIT "CRITICAL"

void spectatorMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

namespace SpectatorLogging {

// Current log level
extern SPECTATOR_LIB_MODE QtMsgType logLevel;
// Output file
extern SPECTATOR_LIB_MODE QFile spectatorLogFile;
// Write to stdout
extern SPECTATOR_LIB_MODE bool stdout;

}

SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorSettings)
SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorConnection)
SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorNotifications)
SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorNethist)
SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorControl)
SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorStats)
SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorUpdater)
SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorNeighbours)
SPECTATOR_LIB_MODE Q_DECLARE_LOGGING_CATEGORY(spectatorApplication)

#define log_settings QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorSettings().categoryName())

#define log_connection QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorConnection().categoryName())

#define log_notifications QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorNotifications().categoryName())

#define log_nethist QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorNethist().categoryName())

#define log_control QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorControl().categoryName())

#define log_stats QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorStats().categoryName())

#define log_updater QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorUpdater().categoryName())

#define log_neighbours QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorNeighbours().categoryName())

#define log_application QMessageLogger(__FILE__, __LINE__, __PRETTY_FUNCTION__, \
	spectatorApplication().categoryName())

#endif
