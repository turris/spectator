/*
 * Spectator - a desktop app which communicates with router Turris
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <spectator/consts.h>
#include <spectator/logging.h>

#include "settings_gui.h"


SpectatorGuiSettings::SpectatorGuiSettings() : QSettings() { }

void SpectatorGuiSettings::load()
{
	log_settings.debug("Loading settings.");

	this->beginGroup("Connection");
	this->refreshInterval = this->value("refresh_interval", DefaultSettings::refreshInterval).toInt();
	log_settings.debug() << "Loading refresh_interval:" << this->refreshInterval;
	this->endGroup();

	this->beginGroup("GUI");
	this->popupTime = this->value("popup_time", DefaultSettings::popupTime).toInt();
	log_settings.debug() << "Loading popup_time:" << this->popupTime;
	this->mainTabIndex = this->value("main_tab_index", DefaultSettings::mainTabIndex).toInt();
	log_settings.debug() << "Loading main_tab_index:" << this->mainTabIndex;
	this->language = this->value("language", DefaultSettings::language).toString();
	log_settings.debug() << "Loading language:" << this->language;
	this->prefferedInterface = this->value("preffered_interface", DefaultSettings::preferedInterface).toString();
	log_settings.debug() << "Loading preffered_interface:" << this->prefferedInterface;
	this->systrayMode = this->value("systray_mode", DefaultSettings::systrayMode).toBool();
	log_settings.debug() << "Loading systray_mode:" << this->systrayMode;
	this->dock = this->value("dock", DefaultSettings::dock).toBool();
	log_settings.debug() << "Loading dock:" << this->dock;
	this->backupPath = this->value("backup_path", DefaultSettings::backupPath).toString();
	log_settings.debug() << "Loading backup_path:" << this->backupPath;
	this->geometry = this->value("geometry", QByteArray()).toByteArray();
	log_settings.debug() << "Loading geometry:" << this->geometry.toHex();
	this->endGroup();

	this->beginGroup("neighbours");
	this->inactiveLimit = this->value("neighbours_inactive_limit", DefaultSettings::inactiveLimit).toInt();
	log_settings.debug() << "Loading neighbours_inactive_limit:" << this->inactiveLimit;
	this->activeNeighbourDevices = this->value("neighbours_interfaces_filter", DefaultSettings::activeNeighbourDevices).toStringList();
	log_settings.debug() << "Loading neighbours_interfaces_filter:" << this->activeNeighbourDevices;
	const QHash<QString, QVariant> &macToName= this->value("neighbours_mac_to_name").toHash();
	this->neighboursMacToName.clear();
	foreach(const QString &macAddress, macToName.keys()) {
		this->neighboursMacToName.insert(macAddress, macToName.value(macAddress).toString());
	}
	log_settings.debug() << "Loading neighbours_mac_to_name:" << this->neighboursMacToName;
	this->neighboursAdvancedView = this->value("neighbours_advanced_view", DefaultSettings::neighboursAdvancedView).toBool();
	log_settings.debug() << "Loading neighbours_advanced_view:" << this->neighboursAdvancedView;
	this->endGroup();
}

void SpectatorGuiSettings::store()
{
	log_settings.debug("Storing settings.");

	this->beginGroup("Connection");
	this->setValue("refresh_interval", this->refreshInterval);
	log_settings.debug() << "Storing refresh_interval:" << this->refreshInterval;
	this->endGroup();

	this->beginGroup("GUI");
	this->setValue("popup_time", this->popupTime);
	log_settings.debug() << "Storing popup_time:" << this->popupTime;
	this->setValue("main_tab_index", this->mainTabIndex);
	log_settings.debug() << "Storing main_tab_index:" << this->mainTabIndex;
	this->setValue("language", this->language);
	log_settings.debug() << "Storing language:" << this->language;
	this->setValue("preffered_interface", this->prefferedInterface);
	log_settings.debug() << "Storing preffered_interface:" << this->prefferedInterface;
	this->setValue("systray_mode", this->systrayMode);
	log_settings.debug() << "Storing systray_mode:" << this->systrayMode;
	this->setValue("dock", this->dock);
	log_settings.debug() << "Storing dock:" << this->dock;
	this->setValue("backup_path", this->backupPath);
	log_settings.debug() << "Storing backup_path:" << this->backupPath;
	this->setValue("geometry", this->geometry);
	log_settings.debug() << "Storing geometry:" << this->geometry.toHex();
	this->endGroup();

	this->beginGroup("neighbours");
	this->setValue("neighbours_inactive_limit", this->inactiveLimit);
	log_settings.debug() << "Storing neighbours_inactive_limit" << this->inactiveLimit;
	this->setValue("neighbours_interfaces_filter", this->activeNeighbourDevices);
	log_settings.debug() << "Storing neighbours_interfaces_filter" << this->activeNeighbourDevices;

	QHash<QString, QVariant> macToName;
	foreach (const QString &macAddress, this->neighboursMacToName.keys()) {
		macToName.insert(macAddress, this->neighboursMacToName.value(macAddress));
	}
	this->setValue("neighbours_mac_to_name", macToName);
	log_settings.debug() << "Storing neighbours_mac_to_name" << this->neighboursMacToName;
	this->setValue("neighbours_advanced_view", this->neighboursAdvancedView);
	log_settings.debug() << "Storing neighbours_advanced_view" << this->neighboursAdvancedView;

	this->endGroup();
}
