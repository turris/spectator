/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <spectator/logging.h>

#include "custom_widgets.h"
#include "application.h"

static QString convertBytes(double number, int prec = 0) {
	QString suffix;
	if (number < 1024.0) {
		suffix = " B";
	} else if (number < 1024.0 * 1024) {
		suffix = " KB";
		number = number / 1024;
	} else if (number < 1024.0 * 1024 * 1024) {
		suffix = " MB";
		number = number / 1024 / 1024;
	} else if (number < 1024.0 * 1024 * 1024 * 1024) {
		suffix = " GB";
		number = number / 1024 / 1024 / 1024;
	} else {
		suffix = " TB";
		number = number / 1024 / 1024 / 1024 / 1024;
	}

	if (number > 0 && number < 10)
		prec++;

	return QString::number(number, 'f', prec) + suffix;
}

static void relabelTaxis(QCPAxis *axis, int prec = 0)
{
	axis->setAutoTickLabels(false);
	const QVector<double> &vector = axis->tickVector();
	QVector<QString> labels;
	foreach(double val, vector) {
		labels.append(convertBytes(val, prec));
	}
	axis->setTickVectorLabels(labels);
}

NotificationWidget::NotificationWidget(QWidget *parent, QWidget *mainWindow, const QString &id, const QString &severity, const QDateTime &time, const QString &text) :
	QWidget(parent),
	id(id),
	mainWindow(mainWindow),
	time(time)
{
	if (severity == SPECTATOR_SEVERITY_UPDATE) {
		this->severity = NotificationWidget::S_UPDATE;
	} else if (severity == SPECTATOR_SEVERITY_ERROR) {
		this->severity = NotificationWidget::S_ERROR;
	} else if (severity == SPECTATOR_SEVERITY_NEWS) {
		this->severity = NotificationWidget::S_NEWS;
	} else if (severity == SPECTATOR_SEVERITY_RESTART) {
		this->severity = NotificationWidget::S_RESTART;
	} else {
		this->severity = NotificationWidget::S_UNKNOWN;
	}

	log_notifications.debug() << "Creating notification:" << this->id << this->severity;
	this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	this->outerLayout = new QHBoxLayout(this);
	this->outerLayout->setSpacing(6);
	this->outerLayout->setContentsMargins(11, 6, 11, 11);
	this->setLayout(this->outerLayout);

	this->groupBox = new QGroupBox(this);
	this->groupBox->setTitle(this->getTitle());
	this->groupBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	this->outerLayout->addWidget(this->groupBox);

	this->innerLayout = new QHBoxLayout(this->groupBox);
	this->innerLayout->setSpacing(6);
	this->innerLayout->setContentsMargins(11, 11, 11, 11);
	this->groupBox->setLayout(this->innerLayout);

	this->textWidget = new QLabel(this->groupBox);
	this->textWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	this->textWidget->setWordWrap(true);
	this->textWidget->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->innerLayout->addWidget(this->textWidget);

	this->rebootButton = new QPushButton(this->groupBox);
	if (this->severity == NotificationWidget::S_RESTART) {
		this->rebootButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_BrowserReload));
		QObject::connect(this->rebootButton, SIGNAL(clicked()), this->mainWindow, SLOT(callRebootRouter()));
		this->innerLayout->addWidget(this->rebootButton);
	} else {
		this->rebootButton->hide();
	}

	this->closeButton = new QPushButton(this->groupBox);
	this->closeButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton));
	QObject::connect(this->closeButton, SIGNAL(clicked()), this, SLOT(callClose()));
	QObject::connect(this, SIGNAL(closedCalled(const QString &)), this->mainWindow, SLOT(callCloseNotification(const QString &)));
	this->innerLayout->addWidget(this->closeButton);

	// Set background color
	QPalette palette(this->palette());
	switch (this->severity) {
		case NotificationWidget::S_RESTART:
			palette.setColor(this->backgroundRole(), QColor(0xCC, 0x33, 0x33, 0x40));
			break;
		case NotificationWidget::S_NEWS:
			palette.setColor(this->backgroundRole(), QColor(0x00, 0xAA, 0xCC, 0x40));
			break;
		case NotificationWidget::S_UPDATE:
			palette.setColor(this->backgroundRole(), QColor(0x66, 0xDD, 0x88, 0x40));
			break;
		case NotificationWidget::S_ERROR:
			palette.setColor(this->backgroundRole(), QColor(0xFF, 0xCC, 0x00, 0x40));
			break;
		default:
			break;
	}
	this->setAutoFillBackground(true);
	this->setPalette(palette);

	this->parentWidget()->layout()->addWidget(this);
	this->updateTexts(text);
}


NotificationWidget::~NotificationWidget()
{
	log_notifications.debug() << "Discarding notification:" << this->id << this->severity;
	QLayout *parent_layout = this->parentWidget()->layout();
	if (parent_layout) {
		parent_layout->removeWidget(this);
	}
	delete this->closeButton;
	delete this->rebootButton;
	delete this->textWidget;
	delete this->innerLayout;
	delete this->groupBox;
	delete this->outerLayout;
}

void NotificationWidget::callClose()
{
	emit this->closedCalled(this->id);
}

QString NotificationWidget::getTitle()
{
	// Keep SEVERITY_MAP here so it updates when the translations are present
	static QMap<NotificationWidget::SEVERITIES, QString> SEVERITY_MAP;
	SEVERITY_MAP[NotificationWidget::S_NEWS] = tr("News");
	SEVERITY_MAP[NotificationWidget::S_ERROR] = tr("Error");
	SEVERITY_MAP[NotificationWidget::S_RESTART] = tr("Restart");
	SEVERITY_MAP[NotificationWidget::S_UPDATE] = tr("Update");
	SEVERITY_MAP[NotificationWidget::S_UNKNOWN] = tr("Unknown");
	const QString &title = SEVERITY_MAP.contains(this->severity) ? SEVERITY_MAP[this->severity] : "";
	return title + " - " + QLocale().toString(this->time, "yyyy-MM-dd HH:mm:ss");
}

void NotificationWidget::setEnabledChildren(bool enabled)
{
	this->textWidget->setEnabled(enabled);
	this->closeButton->setEnabled(enabled);
	this->rebootButton->setEnabled(enabled);
}

void NotificationWidget::updateTexts(const QString &body)
{
	this->groupBox->setTitle(this->getTitle());
	this->textWidget->setText(body);
}

InterfaceWidget::InterfaceWidget(QWidget *parent, QWidget *mainWindow):
	QWidget(parent),
	currentIndex(0),
	mainWindow(mainWindow)
{
	QFont boldFont;
	boldFont.setBold(true);

	this->innerLayout = new QGridLayout(this);
	this->innerLayout->setSpacing(6);
	this->innerLayout->setContentsMargins(0, 0, 0, 0);
	this->setLayout(this->innerLayout);
	this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);


	this->buttonWidget = new QWidget(this);
	this->innerLayout->addWidget(this->buttonWidget, 0, 0, 1, 4);

	this->buttonLayout = new QHBoxLayout(this->buttonWidget);
	this->buttonWidget->setLayout(buttonLayout);

	this->backwardButton = new QPushButton(this->buttonWidget);
	this->backwardButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_ArrowBack));
	this->backwardButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	QObject::connect(this->backwardButton, SIGNAL(clicked()), this, SLOT(backward()));
	this->buttonLayout->addWidget(this->backwardButton);

	this->deviceLabel = new QLabel(this);
	this->deviceLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
	this->deviceLabel->setFont(boldFont);
	this->deviceLabel->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->buttonLayout->addWidget(this->deviceLabel);

	this->deviceText = new QLabel(this);
	this->deviceText->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->buttonLayout->addWidget(this->deviceText);

	this->forwardButton = new QPushButton(this->buttonWidget);
	this->forwardButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_ArrowForward));
	this->forwardButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	QObject::connect(this->forwardButton, SIGNAL(clicked()), this, SLOT(forward()));
	this->buttonLayout->addWidget(this->forwardButton);

	this->txLabel = new QLabel(this);
	this->txLabel->setFont(boldFont);
	this->txLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
	this->txLabel->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->txLabel->setStyleSheet("color: #0000FF");
	this->innerLayout->addWidget(this->txLabel, 1, 0);

	this->txText = new QLabel(this);
	this->txText->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->innerLayout->addWidget(this->txText, 1, 1);

	this->rxLabel = new QLabel(this);
	this->rxLabel->setFont(boldFont);
	this->rxLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
	this->rxLabel->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->rxLabel->setStyleSheet("color: #00FF00");
	this->innerLayout->addWidget(this->rxLabel, 1, 2);

	this->rxText = new QLabel(this);
	this->rxText->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->innerLayout->addWidget(this->rxText, 1, 3);

	this->interfacePlot = new QCustomPlot(this);
	this->interfacePlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	this->innerLayout->addWidget(this->interfacePlot, 2, 0, 1, 4);
	// Set plot
	{
		QCustomPlot *plot = this->interfacePlot;
		plot->plotLayout()->clear();
		plot->plotLayout()->setMargins(QMargins(0,0,0,0));
		QCPAxisRect *wideAxisRect = new QCPAxisRect(plot, false);
		wideAxisRect->setMargins(QMargins(0,0,0,0));
		QCPAxis *leftAxis = wideAxisRect->addAxis(QCPAxis::atLeft);
		leftAxis->setPadding(0);
		leftAxis->setRange(0.0, 500.0);
		leftAxis->setAutoTickCount(4);
		QCPAxis *bottomAxis = wideAxisRect->addAxis(QCPAxis::atBottom);
		bottomAxis->setVisible(false);
		plot->plotLayout()->addElement(0, 0, wideAxisRect);
		QCPGraph *graph;
		// Add rx
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::green));
		// Add tx
		graph = plot->addGraph(plot->axisRect()->axis(QCPAxis::atBottom), plot->axisRect()->axis(QCPAxis::atLeft));
		graph->setPen(QPen(Qt::blue));
	}

	this->parentWidget()->layout()->addWidget(this);
	this->updateTexts();
	this->setStyleSheet("color: black");
}

InterfaceWidget::~InterfaceWidget()
{
	delete this->deviceText;
	delete this->txText;
	delete this->rxText;
	delete this->deviceLabel;
	delete this->txLabel;
	delete this->rxLabel;
	delete this->backwardButton;
	delete this->forwardButton;
	delete this->interfacePlot;
	delete this->buttonLayout;
	delete this->buttonWidget;
	delete this->innerLayout;
}

void InterfaceWidget::setData(const QMap<QString, InterfaceWidgetRecord> &data)
{
	this->interfaces = data.keys();
	if (this->data.empty()) {  // first run try to set preffered interface
		for (int i = 0; i < this->interfaces.count(); ++i) {
			if (this->interfaces[i] == this->prefferedInterface) {
				this->currentIndex = i;
				break;
			}
		}
	}
	this->data = data;
	qSort(this->interfaces.begin(), this->interfaces.end());

	if (this->currentIndex < this->interfaces.count()) {
		this->setIndex(currentIndex);
	} else {
		this->setIndex(0);
	}
}

void InterfaceWidget::setIndex(int index)
{
	if (index < this->interfaces.count()) {
		const QString &interfaceName = this->interfaces[index];
		this->deviceText->setText(interfaceName);
		this->rxText->setText(convertBytes(this->data[interfaceName].yRxAxis.last()));
		this->txText->setText(convertBytes(this->data[interfaceName].yTxAxis.last()));
		// Set plot data
		QCustomPlot *plot = this->interfacePlot;
		{
			int max = 0;
			int min = INT_MAX;
			foreach (int value, this->data[interfaceName].yRxAxis) {
				max = value > max ? value : max;
				min = value < min ? value : min;
			}
			foreach (int value, this->data[interfaceName].yTxAxis) {
				max = value > max ? value : max;
				min = value < min ? value : min;
			}
			// ensure that max > 0
			max = max > 0 ? max : 1;
			// recalculate max charts high and low values (dark magick goes here)
			float highGraph = 1.1 * max;
			float lowGraph = -(highGraph / 100);
			plot->axisRect()->axis(QCPAxis::atLeft)->setRange(lowGraph, highGraph);
			plot->axisRect()->axis(QCPAxis::atBottom)->setRange(
				this->data[interfaceName].xTimeAxis.first(), this->data[interfaceName].xTimeAxis.last() + 1);
			plot->graph(0)->setData(this->data[interfaceName].xTimeAxis, this->data[interfaceName].yRxAxis);
			plot->graph(1)->setData(this->data[interfaceName].xTimeAxis, this->data[interfaceName].yTxAxis);
			plot->replot();
			// convert axis labels
			relabelTaxis(plot->axisRect()->axis(QCPAxis::atLeft));
		}
		this->currentIndex = index;
	}
}

void InterfaceWidget::forward()
{
	if (!this->interfaces.empty()) {
		this->setIndex(this->currentIndex + 1 == this->interfaces.count() ? 0 : this->currentIndex + 1);
	}
}

void InterfaceWidget::backward()
{
	if (!this->interfaces.empty()) {
		this->setIndex(0 == this->currentIndex ? this->interfaces.count() - 1 : this->currentIndex - 1);
	}
}

QString InterfaceWidget::getCurrentInterface()
{
	if (0 <= this->currentIndex && this->currentIndex < this->interfaces.count()) {
		return this->interfaces[this->currentIndex];
	}
	return "";
}

void InterfaceWidget::updateTexts()
{
	this->deviceLabel->setText(tr("Device:"));
	this->rxLabel->setText(tr("RX:"));
	this->txLabel->setText(tr("TX:"));
}


UpdaterUserListsWidget::UpdaterUserListsWidget(QWidget *parent, QWidget *mainWindow) :
	QWidget(parent),
	editMode(false),
	mainWindow(mainWindow)
{
	this->mainLayout = new QGridLayout(this);
	this->mainLayout->setContentsMargins(11, 11, 11, 11);
	this->mainLayout->setSpacing(15);
	this->vSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
	this->hSpacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
	this->mainLayout->addItem(this->vSpacer, 0, 0);
	this->mainLayout->addItem(this->hSpacer, 0, 1);

	this->setListsButton = new QPushButton(this);
	this->setListsButton->setVisible(false);
	QObject::connect(this->setListsButton, SIGNAL(clicked(bool)), this->mainWindow, SLOT(callSetUpdater()));
	QObject::connect(this->setListsButton, SIGNAL(clicked(bool)), this, SLOT(exitEditMode()));
	this->updateTexts();

	this->setLayout(this->mainLayout);
}

void UpdaterUserListsWidget::updateTexts()
{
	this->setListsButton->setText(tr("Set"));
}

void UpdaterUserListsWidget::disposeUserListsWidgets()
{
	foreach (QString *name, this->names) {
		delete name;
	}
	this->names.clear();

	foreach (QLabel *title, this->titles) {
		delete title;
	}
	this->titles.clear();

	foreach (QLabel *description, this->descriptions) {
		delete description;
	}
	this->descriptions.clear();

	foreach (QCheckBox *checkbox, this->checkboxes) {
		delete checkbox;
	}
	this->checkboxes.clear();
}

UpdaterUserListsWidget::~UpdaterUserListsWidget()
{
	this->disposeUserListsWidgets();

	// Delete all class widgets manually
	this->mainLayout->removeItem(this->hSpacer);
	this->mainLayout->removeItem(this->vSpacer);

	delete this->vSpacer;
	delete this->hSpacer;
	delete this->setListsButton;
	delete this->mainLayout;
}

int UpdaterUserListsWidget::getLenght()
{
	return this->names.length();
}


QString * UpdaterUserListsWidget::getName(int index)
{
	return this->names.at(index);
}

QCheckBox * UpdaterUserListsWidget::getCheckbox(int index)
{
	return this->checkboxes.at(index);
}

void UpdaterUserListsWidget::setUserListData(const QList<PkgListRecord> &data)
{
	// Cleanup
	this->mainLayout->removeItem(this->hSpacer);
	this->mainLayout->removeItem(this->vSpacer);
	this->mainLayout->removeWidget(this->setListsButton);

	this->disposeUserListsWidgets();

	// Reinit the lists
	int i = 0;
	foreach (const PkgListRecord &list, data) {
		log_updater.debug() << "Adding updater user list -" << list.name;
		this->names.append(new QString(list.name));

		QLabel *title = new QLabel(this);
		title->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
		title->setText(list.title);
		title->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
		QFont font(title->font());
		font.setBold(true);
		title->setFont(font);
		this->mainLayout->addWidget(title, i, 0);
		this->titles.append(title);

		QLabel *description = new QLabel(this);
		description->setAlignment(Qt::AlignCenter|Qt::AlignVCenter);
		description->setText(list.description);
		description->setWordWrap(true);
		description->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
		this->mainLayout->addWidget(description, i, 1);
		this->descriptions.append(description);

		QCheckBox *checkBox = new QCheckBox(this);
		checkBox->setObjectName(list.name);
		this->mainLayout->addWidget(checkBox, i, 2);
		checkBox->setChecked(list.activated);
		QObject::connect(checkBox, SIGNAL(toggled(bool)), this, SLOT(enterEditMode()));
		this->checkboxes.append(checkBox);

		++i;
	}

	this->mainLayout->addWidget(this->setListsButton, i, 0, 1, 3, Qt::AlignRight);
	this->setListsButton->setVisible(true);

	this->mainLayout->addItem(this->hSpacer, i + 1, 1);
	this->mainLayout->addItem(this->vSpacer, i + 1, 0);
}

void UpdaterUserListsWidget::exitEditMode()
{
	// Disable
	this->editMode = false;
	QPalette palette(this->palette());
	palette.setColor(this->backgroundRole(), QColor(0x00, 0xAA, 0xCC, 0x00));
	this->setAutoFillBackground(true);
	this->setPalette(palette);
}

void UpdaterUserListsWidget::enterEditMode()
{
	// Highlight
	this->editMode = true;
	QPalette palette(this->palette());
	palette.setColor(this->backgroundRole(), QColor(0x00, 0xAA, 0xCC, 0x40));
	this->setAutoFillBackground(true);
	this->setPalette(palette);
}

UpdaterActivitiesWidget::UpdaterActivitiesWidget(QWidget *parent)
	: QWidget(parent)
{
	this->mainLayout = new QGridLayout(this);
	this->spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding);
}

UpdaterActivitiesWidget::~UpdaterActivitiesWidget()
{
	foreach(QLabel *label, this->labels) {
		this->mainLayout->removeWidget(label);
		delete label;
	}
	this->mainLayout->removeItem(this->spacer);
	delete this->spacer;
	delete this->mainLayout;
}

void UpdaterActivitiesWidget::disposeWidgets()
{
	// remove the spacer
	this->mainLayout->removeItem(this->spacer);

	// remove widgets
	foreach(QLabel *label, this->labels) {
		this->mainLayout->removeWidget(label);
		delete label;
	}
	this->labels.clear();

	// add spacer in the beginning
	this->mainLayout->addItem(this->spacer, 0, 0);
}

void UpdaterActivitiesWidget::setActivitiesData(const QList<UpdaterActivityRecord> &data)
{
	this->disposeWidgets();

	this->mainLayout->removeItem(this->spacer);
	int line = 0;
	foreach (const UpdaterActivityRecord &activityRecord, data) {
		log_updater.debug() << "Adding activity " << activityRecord.action << activityRecord.packageName << activityRecord.packageVersion;

		// icon
		QLabel *iconLabel = new QLabel(this);
		this->labels.append(iconLabel);
		if (activityRecord.action == "remove") {
			iconLabel->setPixmap(QApplication::style()->standardIcon(QStyle::SP_TrashIcon).pixmap(15, 15));
			iconLabel->setToolTip(tr("deleted"));
		} else if (activityRecord.action == "download") {
			iconLabel->setPixmap(QApplication::style()->standardIcon(QStyle::SP_ArrowDown).pixmap(15, 15));
			iconLabel->setToolTip(tr("downloaded"));
		} else if (activityRecord.action == "install") {
			iconLabel->setPixmap(QApplication::style()->standardIcon(QStyle::SP_FileDialogNewFolder).pixmap(15, 15));
			iconLabel->setToolTip(tr("installed"));
		} else {
			log_updater.critical() << "Unknown activity action type -" << activityRecord.action;
			continue;
		}
		iconLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		this->mainLayout->addWidget(iconLabel, line, 0);

		// package
		QLabel *packageLabel = new QLabel(activityRecord.packageName, this);
		this->labels.append(packageLabel);
		packageLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		this->mainLayout->addWidget(packageLabel, line, 1);

		// version
		QLabel *versionLabel = new QLabel(activityRecord.packageVersion, this);
		this->labels.append(versionLabel);
		versionLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		this->mainLayout->addWidget(versionLabel, line, 2);

		++line;
	}

	// Add spacer
	this->mainLayout->addItem(this->spacer, line, 3);
}

QString NeighbourWidget::getAlias(const QString &macAddress)
{
	const QHash<QString, QString>::const_iterator &it = this->macToNameMapping->find(macAddress);
	if (it != this->macToNameMapping->end()) {
		// Alias was found
		return it.value();
	} else {
		// Alias was not found, return empty string
		return QString();
	}
}

void NeighbourWidget::setHeaderLabel(const QString &interfaceName, const QString &macAddress)
{
	// basic part
	this->basicHeaderText = tr("MAC address") + " <b>" + macAddress + "</b>";
	this->basicHeaderText += (NEIGHBOURS_INTERFACE_WITHOUT_NAME == interfaceName ? " " : " " + tr("on") + " <b>" + this->dev + "</b> ");

	// header part
	const QString &alias = this->getAlias(macAddress);
	this->extraHeaderText = alias.isEmpty() ? "" : " " + tr("with alias") + " <b>" + alias + "</b>";

	// update the header
	this->aliasLineEdit->setText(alias);
	this->headerLabel->setText(this->basicHeaderText + this->extraHeaderText);
}

void NeighbourWidget::setBodyText(bool extra)
{
	this->textWidget->setText(extra ? this->extraBodyText : this->basicBodyText);
}

void NeighbourWidget::setContent(const NeighbourRecord *neighbour)
{
	this->setHeaderLabel(neighbour->interfaceName, neighbour->macAddress);

	QString text;
	QString extraText;
	text += "<ul>";
	extraText += "<ul>";
	int count = 0;
	foreach (NeighbourIpRecord ipRecord, neighbour->ips) {
		count += 1;
		text += "<li>" + ipRecord.ipAddress;
		extraText += "<li>" + ipRecord.ipAddress;
		if (!ipRecord.hostname.trimmed().isEmpty()) {
			text += " (" + ipRecord.hostname + ")";
			extraText += " (" + ipRecord.hostname + ")";
		}
		extraText += !ipRecord.dhcpLease.isValid() ? "" : "<br /> DHCP lease: " + QLocale().toString(ipRecord.dhcpLease, "yyyy-MM-dd HH:mm:ss");
		extraText += "<br /> connections: " + QString::number(ipRecord.connectionCount);
		extraText += ipRecord.nud.isEmpty() ? "" : "<br /> nud: " + ipRecord.nud;
		extraText += ipRecord.usedSince == -1 || ipRecord.confirmedSince == -1 || ipRecord.updatedSince == -1 ?
			"" : "<br /> used: " + QString::number(ipRecord.usedSince) + "/" + QString::number(ipRecord.confirmedSince) + "/" + QString::number(ipRecord.updatedSince);
		if (count != neighbour->ips.count()) {
			extraText += "<br />";
		}
		text += "</li>";
		extraText += "</li>";
	}

	text += "</ul>";
	extraText += "</ul>";

	// Set the texts
	this->basicBodyText = text;
	this->extraBodyText = extraText;
}


NeighbourWidget::NeighbourWidget(QWidget *parent, QWidget *mainWindow,
		const NeighbourRecord *neighbour, QHash<QString, QString> *macToNameMapping) :
	QWidget(parent),
	mac(neighbour->macAddress),
	dev(neighbour->interfaceName),
	mainWindow(mainWindow),
	macToNameMapping(macToNameMapping)
{
	log_neighbours.debug() << "Creating neighbour:" << this->mac << this->dev;
	this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	// Setting gui
	this->outerLayout = new QHBoxLayout(this);
	this->outerLayout->setContentsMargins(5, 5, 5, 5);
	this->setLayout(this->outerLayout);

	this->innerWidget = new QWidget(this);
	this->outerLayout->addWidget(this->innerWidget);

	this->innerLayout = new QVBoxLayout(this->innerWidget);
	this->innerLayout->setSpacing(0);
	this->innerLayout->setContentsMargins(0, 0, 0, 0);
	this->innerWidget->setLayout(this->innerLayout);

	this->headerWidget = new QWidget(this->innerWidget);
	this->headerWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	this->innerLayout->addWidget(headerWidget);

	this->headerLayout = new QHBoxLayout(this->headerWidget);
	this->headerLayout->setSpacing(6);
	this->headerLayout->setContentsMargins(0, 0, 0, 0);
	this->headerWidget->setLayout(this->headerLayout);

	this->headerLabel = new QLabel(this->headerWidget);
	this->headerLabel->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->headerLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	this->headerLayout->addWidget(this->headerLabel);

	this->aliasLineEdit = new QLineEdit(this->headerWidget);
	this->aliasLineEdit->setHidden(true);
	this->aliasLineEdit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	QObject::connect(this->aliasLineEdit, SIGNAL(returnPressed()), this, SLOT(switchMacEdit()));
	this->headerLayout->addWidget(this->aliasLineEdit);

	this->aliasEditButton = new QPushButton(this->headerWidget);
	this->aliasEditButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_FileDialogDetailedView));
	this->aliasEditButton->setToolTip(tr("update alias"));
	this->aliasEditButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	QObject::connect(this->aliasEditButton, SIGNAL(clicked(bool)), this, SLOT(switchMacEdit()));
	this->headerLayout->addWidget(this->aliasEditButton);

	this->textWidget = new QLabel(this->innerWidget);
	this->textWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	this->textWidget->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	this->innerLayout->addWidget(this->textWidget);

	this->appearedLabel = new QLabel(QTime::currentTime().toString("HH:mm:ss"), this);
	this->appearedLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
	this->outerLayout->addWidget(this->appearedLabel);
	this->markReadButton = new QPushButton(this);
	this->markReadButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCloseButton));
	QObject::connect(this->markReadButton, SIGNAL(clicked(bool)), this, SLOT(markRead()));
	this->outerLayout->addWidget(this->markReadButton);

	// Fill the backgroud for the highlights
	this->setAutoFillBackground(true);

	// update mapping
	this->setMacMapping(this->macToNameMapping);

	// update data
	this->setContent(neighbour);
}

void NeighbourWidget::markRead()
{
	QPalette palette(this->palette());
	palette.setColor(this->backgroundRole(), QColor(0xFF, 0xCC, 0x00, 0x00));
	this->setPalette(palette);
	this->markReadButton->setHidden(true);
	this->appearedLabel->setHidden(true);
}

void NeighbourWidget::markUnread()
{
	QPalette palette(this->palette());
	palette.setColor(this->backgroundRole(), QColor(0xFF, 0xCC, 0x00, 0x40));
	this->setPalette(palette);
	this->markReadButton->setHidden(false);
}

void NeighbourWidget::switchMacEdit()
{
	if (this->aliasLineEdit->isVisible()) {
		QString newName(this->aliasLineEdit->text().trimmed());

		// Store data
		if (newName.isEmpty()) {
			this->macToNameMapping->remove(this->mac);
			newName = this->mac;
		} else {
			this->macToNameMapping->insert(this->mac, newName);
		}

		// Update gui
		this->aliasLineEdit->hide();
		this->setHeaderLabel(this->dev, this->mac);
		this->aliasEditButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_FileDialogDetailedView));
		this->aliasEditButton->setToolTip(tr("update alias"));
		emit this->editStatusChanged(false);
	} else {
		// Update gui
		this->headerLabel->setText(this->basicHeaderText);
		this->aliasLineEdit->show();
		this->aliasLineEdit->setFocus();
		this->aliasLineEdit->selectAll();
		this->aliasEditButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogApplyButton));
		this->aliasEditButton->setToolTip(tr("apply alias"));
		emit this->editStatusChanged(true);
	}
}

void NeighbourWidget::setMacMapping(QHash<QString, QString> *macToNameMapping)
{
	this->macToNameMapping = macToNameMapping;
}

void NeighbourWidget::resetEditState()
{
	// Hide edit and reset text
	this->setHeaderLabel(this->dev, this->mac);
	this->headerLabel->setText(this->basicHeaderText + this->extraHeaderText);
	this->aliasLineEdit->hide();
	this->aliasEditButton->setIcon(QApplication::style()->standardIcon(QStyle::SP_FileDialogDetailedView));
	this->aliasEditButton->setToolTip(tr("update alias"));
}

NeighbourWidget::~NeighbourWidget()
{
	log_neighbours.debug() << "Discarding neighbour:" << this->mac << this->dev;
	QLayout *parent_layout = this->parentWidget()->layout();
	if (parent_layout) {
		parent_layout->removeWidget(this);
	}
	// Dispose all components (to be sure that it is properly delete)
	// it should be deleted from the lists
	delete this->markReadButton;
	delete this->textWidget;
	delete this->headerLabel;
	delete this->aliasLineEdit;
	delete this->aliasEditButton;
	delete this->headerLayout;
	delete this->headerWidget;
	delete this->innerLayout;
	delete this->innerWidget;
	delete this->outerLayout;
}

QScreen *SpectatorSystemTray::getScreen()
{
	int screenIdx = SpectatorApplication::desktop()->screenNumber(this->geometry().center());
	return SpectatorApplication::screens()[screenIdx];
}
