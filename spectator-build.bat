REM To use this script you need to have QT installed to a standard location C:\Qt\ and qt was to contain mingw32 compiler
REM
REM To run this script from wine use 'wine cmd < spectator-build.bat'
REM

SET Path=C:\Qt\Qt5.5.0\5.5\mingw492_32\bin;C:\Qt\Qt5.5.0\Tools\mingw492_32\bin;C:\Qt\Qt5.5.0\Tools\QtCreator\bin;C:32;C:

qmake spectator.pro
mingw32-make
