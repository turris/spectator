/*
 * Spectator - a desktop app which communicates with router Turris
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_LIB_H
#define SPECTATOR_LIB_H

#if defined(SPECTATOR_LIBRARY)
#  define SPECTATOR_LIB_MODE Q_DECL_EXPORT
#else
#  define SPECTATOR_LIB_MODE Q_DECL_IMPORT
#endif

#endif
