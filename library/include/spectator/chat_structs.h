/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_CHAT_STRUCTS_H
#define SPECTATOR_CHAT_STRUCTS_H

#include <QString>
#include <QList>
#include <QDateTime>
#include "spectator_lib.h"

struct SPECTATOR_LIB_MODE NetconfErrorInfoElement {
	QString name;
	QString content;
};

struct SPECTATOR_LIB_MODE NetconfError {
	QString type;
	QString tag;
	QString serverity;
	QString appTag;
	QString path;
	QString message;
	QList<NetconfErrorInfoElement> info;
};

struct SPECTATOR_LIB_MODE Capability {
	QString base;
	QString revision;

	Capability(const QString &url);
	Capability(const QString &base, const QString &revision) : base(base), revision(revision) {}

	bool covers(const Capability &capability) const;
};

struct SPECTATOR_LIB_MODE ParsedNotification {
	QString id;
	QString severity;
	QString body;
	QDateTime time;
};

struct SPECTATOR_LIB_MODE NethistInterfaceRecord {
	QString name;
	qulonglong rx;
	qulonglong tx;
};

struct SPECTATOR_LIB_MODE NethistRecord {
	QDateTime time;
	QList<NethistInterfaceRecord> interfaces;
	double cpuLoad;
	long boardTemperature;
	long cpuTemperature;
	long usedDiskSpace;
	long availableDiskSpace;
	long totalMemory;
	long freeMemory;
	long buffersMemory;
	long cachedMemory;
};

struct SPECTATOR_LIB_MODE StatsAddress {
	QString type;
	QString address;
};

struct SPECTATOR_LIB_MODE StatsInterface {
	QString name;
	QString use;
	bool up;
	bool down;
	QList<StatsAddress> addresses;
};

struct SPECTATOR_LIB_MODE StatsRecord {
	QString model;
	QString boardName;
	QString kernelVersion;
	QString osVersion;
	QString firewallStatus;
	int firewallAge;
	QString ucollectStatus;
	int ucollectAge;
	float uptime;
	QString hostname;
	QList<StatsInterface> interfaces;
};

struct SPECTATOR_LIB_MODE PkgListRecord {
	QString name;
	QString title;
	QString description;
	bool activated;
};

struct SPECTATOR_LIB_MODE UpdaterActivityRecord {
	QString action;
	QString packageName;
	QString packageVersion;
};

struct SPECTATOR_LIB_MODE UpdaterDataRecord {
	QString status;
	QList<PkgListRecord> userLists;
	QList<UpdaterActivityRecord> lastActivities;
};

struct SPECTATOR_LIB_MODE NeighbourIpRecord {
	QString ipAddress;
	QString nud;
	int connectionCount;
	QDateTime dhcpLease;
	QString hostname;
	int updatedSince;
	int confirmedSince;
	int usedSince;

	NeighbourIpRecord() : connectionCount(0), updatedSince(-1), confirmedSince(-1), usedSince(-1) {}
};

struct SPECTATOR_LIB_MODE NeighbourRecord {
	QString interfaceName;
	QString macAddress;
	QList<NeighbourIpRecord> ips;
};

#endif
