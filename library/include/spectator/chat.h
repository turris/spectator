/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_CHAT_H
#define SPECTATOR_CHAT_H

#include "chat_structs.h"
#include "conn_consts.h"
#include "spectator_lib.h"

#include <QObject>
#include <QDomDocument>

#define SPECTATOR_SEVERITY_RESTART "restart"
#define SPECTATOR_SEVERITY_ERROR "error"
#define SPECTATOR_SEVERITY_NEWS "news"
#define SPECTATOR_SEVERITY_UPDATE "update"

#define CAPABILITY_NETCONF_BASE "urn:ietf:params:netconf:base:1.0"
#define CAPABILITY_NOTIFICATION_BASE "http://www.nic.cz/ns/router/user-notify"
#define CAPABILITY_NETHIST_BASE "http://www.nic.cz/ns/router/nethist"
#define CAPABILITY_MAINTAIN_BASE "http://www.nic.cz/ns/router/maintain"
#define CAPABILITY_PASSWORD_BASE "http://www.nic.cz/ns/router/password"
#define CAPABILITY_STATS_BASE "http://www.nic.cz/ns/router/stats"
#define CAPABILITY_REGISTRATION_BASE "http://www.nic.cz/ns/router/registration"
#define CAPABILITY_UPDATER_BASE "http://www.nic.cz/ns/router/updater"
#define CAPABILITY_NEIGHBOURS_BASE "http://www.nic.cz/ns/router/neighbours"

#define NS_NETCONF_BASE "urn:ietf:params:xml:ns:netconf:base:1.0"
#define NS_NOTIFICATION CAPABILITY_NOTIFICATION_BASE
#define NS_NETHIST CAPABILITY_NETHIST_BASE
#define NS_MAINTAIN CAPABILITY_MAINTAIN_BASE
#define NS_PASSWORD CAPABILITY_PASSWORD_BASE
#define NS_STATS CAPABILITY_STATS_BASE
#define NS_REGISTRATION CAPABILITY_REGISTRATION_BASE
#define NS_UPDATER CAPABILITY_UPDATER_BASE
#define NS_NEIGHBOURS CAPABILITY_NEIGHBOURS_BASE

#define NEIGHBOURS_INTERFACE_WITHOUT_NAME "unknown"


namespace Chats {

class SPECTATOR_LIB_MODE Base : public QObject {
	Q_OBJECT

public:
	Base();
	virtual ~Base() {}
	virtual void talk() = 0;
	virtual const char *getName() = 0;
	virtual const QList<Capability> &getRequiredCapabilities() = 0;

signals:
	void communicationStatusUpdate(Communicator::ConnectorStates newState);
	void writeMessage(const QString &msg);
	void finished(bool success);
	void netconfErrorsOccured(const QList<NetconfError> &errors);
	void report(Communicator::ReportType type, const QString &msg);

public slots:
	virtual void readMessage(const QString &msg) = 0;

protected:
	int status;
	int rpcMessageId;

	bool checkRpcMessageId(QDomDocument &xml);
	bool checkRpcMessageOk(QDomDocument &xml, bool reportError=true);
	bool parseNetconfErrors(QDomDocument &xml, QList<NetconfError> &outErrors);
	bool reportNetconfErrors(QDomDocument &xml);
	bool parseXml(QDomDocument &xml, const QString &msg);
	void finish(bool res);
};

class SPECTATOR_LIB_MODE Initial : public Base {
	Q_OBJECT

signals:
	void exportCapabilities(const QList<Capability> &capabilities);

public:
	virtual void talk();
	static const char *name() { return "initial"; }
	virtual const char *getName() { return Initial::name(); }
	static QList<Capability> requiredCapabilities;
	virtual const QList<Capability> &getRequiredCapabilities() { return Initial::requiredCapabilities; }
	~Initial() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString capibilityMessage();
};

class SPECTATOR_LIB_MODE GetNotifications : public Base {
	Q_OBJECT

signals:
	void exportNotifications(const QList<ParsedNotification> &notifications);

public:
	virtual void talk();
	static const char *name() { return "get_notifications"; }
	virtual const char *getName() { return GetNotifications::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return GetNotifications::requiredCapabilities;}
	GetNotifications(const QString &lang = "en") : Base(), lang(lang) {}
	~GetNotifications() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	QString lang;
	static QString getNotificationsMessage(int rpcMessageId);
};

class SPECTATOR_LIB_MODE DeleteNotification : public Base {
	Q_OBJECT

signals:
	void notificationDeleted(const QString &id);

public:
	QString messageId;

	virtual void talk();
	static const char *name() { return "delete_notification"; }
	virtual const char *getName() { return DeleteNotification::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return DeleteNotification::requiredCapabilities;}
	DeleteNotification(const QString &messageId) : Base(), messageId(messageId) {}
	~DeleteNotification() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString deleteNotificationMessage(int rpcMessageId, const QString &messageId);

};

class SPECTATOR_LIB_MODE RebootRouter : public Base {
	Q_OBJECT

signals:
	void rebootingRouter();

public:

	virtual void talk();
	static const char *name() { return "reboot_router"; }
	virtual const char *getName() { return RebootRouter::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return RebootRouter::requiredCapabilities;}
	RebootRouter() : Base() {}
	~RebootRouter() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString rebootRouterMessage(int rpcMessageId);

};

class SPECTATOR_LIB_MODE GetNethist : public Base {
	Q_OBJECT

signals:
	void exportNethist(const QList<NethistRecord> &records);

public:
	virtual void talk();
	static const char *name() { return "get_nethist"; }
	virtual const char *getName() { return GetNethist::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return GetNethist::requiredCapabilities;}
	GetNethist() : Base() {}
	~GetNethist() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	bool parseRecord(const QDomNode &node, NethistRecord &record);
	static QString getNethistMessage(int rpcMessageId);
};

class SPECTATOR_LIB_MODE SetPassword : public Base {
	Q_OBJECT

signals:
	void passwordSetResult(bool success);

public:
	virtual void talk();
	static const char *name() { return "set_password"; }
	virtual const char *getName() { return SetPassword::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return SetPassword::requiredCapabilities;}
	SetPassword(const QString &password) : Base(), password(password) {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	QString password;
	static QString setPasswordMessage(int rpcMessageId, const QString &newPassword);
};

class SPECTATOR_LIB_MODE StoreBackup : public Base {
	Q_OBJECT

signals:
	void storeData(const QByteArray &data);

public:
	virtual void talk();
	static const char *name() { return "store_backup"; }
	virtual const char *getName() { return StoreBackup::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return StoreBackup::requiredCapabilities;}
	StoreBackup() : Base() {}
	~StoreBackup() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString storeBackupMessage(int rpcMessageId);
};

class SPECTATOR_LIB_MODE RestoreBackup : public Base {
	Q_OBJECT

private:
	QByteArray data;

signals:
	void dataRestored(const QString &newIp);

public:
	virtual void talk();
	static const char *name() { return "restore_backup"; }
	virtual const char *getName() { return RestoreBackup::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return RestoreBackup::requiredCapabilities;}
	RestoreBackup(const QByteArray &data) : Base(), data(data) {}
	~RestoreBackup() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString restoreBackupMessage(int rpcMessageId, const QByteArray &data);

};

class SPECTATOR_LIB_MODE GetStats : public Base {
	Q_OBJECT

signals:
	void exportStats(const StatsRecord &record);

public:
	virtual void talk();
	static const char *name() { return "get_stats"; }
	virtual const char *getName() { return GetStats::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return GetStats::requiredCapabilities;}
	GetStats() : Base() {}
	~GetStats() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString getStatsMessage(int rpcMessageId);
};

class SPECTATOR_LIB_MODE GetSerial : public Base {
	Q_OBJECT

signals:
	void exportSerial(const QString &serial);

public:
	virtual void talk();
	static const char *name() { return "get_serial"; }
	virtual const char *getName() { return GetSerial::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return GetSerial::requiredCapabilities;}
	GetSerial() : Base() {}
	~GetSerial() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString getSerialMessage(int rpcMessageId);
};

class SPECTATOR_LIB_MODE GetUpdaterData : public Base {
	Q_OBJECT

signals:
	void exportLists(const UpdaterDataRecord &record);

public:
	virtual void talk();
	static const char *name() { return "get_updater_data"; }
	virtual const char *getName() { return GetUpdaterData::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return GetUpdaterData::requiredCapabilities;}
	GetUpdaterData(const QString &lang = "en") : Base(), lang(lang) {}
	~GetUpdaterData() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	QString lang;
	static QString getUpdaterDataMessage(int rpcMessageId);
};

class SPECTATOR_LIB_MODE CheckUpdater : public Base {
	Q_OBJECT

public:
	virtual void talk();
	static const char *name() { return "check_updater"; }
	virtual const char *getName() { return CheckUpdater::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return CheckUpdater::requiredCapabilities;}
	CheckUpdater() : Base() {}
	~CheckUpdater() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString checkUpdaterMessage(int rpcMessageId);
};

class SPECTATOR_LIB_MODE SetUpdater : public Base {
	Q_OBJECT

public:
	virtual void talk();
	static const char *name() { return "set_updater"; }
	virtual const char *getName() { return SetUpdater::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return SetUpdater::requiredCapabilities;}
	SetUpdater(const QList<QString> &mergeList, const QList<QString> &removeList) : Base(), mergeList(mergeList), removeList(removeList) {}
	~SetUpdater() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	QList<QString> mergeList;
	QList<QString> removeList;
	static QString setUpdaterMessage(int rpcMessageId, const QList<QString> &mergeList, const QList<QString> &removeList);
};

class SPECTATOR_LIB_MODE GetNeighbours : public Base {
	Q_OBJECT

public:
	virtual void talk();
	static const char *name() { return "get_neighbours"; }
	virtual const char *getName() { return GetNeighbours::name(); }
	static QList<Capability> requiredCapabilities;
	virtual QList<Capability> &getRequiredCapabilities() { return GetNeighbours::requiredCapabilities;}
	GetNeighbours(const QList<QString> &interfaces = QList<QString>()) : Base(), interfaces(interfaces) {}
	~GetNeighbours() {}

public slots:
	virtual void readMessage(const QString &msg);

private:
	static QString getNeighbourMessage(int rpcMessageId, const QList<QString> &interfaces);

	QList<QString> interfaces;

signals:
	void exportNeighbours(const QList<NeighbourRecord> &neighbours);
};

}

#endif
