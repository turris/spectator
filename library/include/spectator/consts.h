/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_CONSTS_H
#define SPECTATOR_CONSTS_H

#include <QString>

#include "spectator_lib.h"

namespace Languages {

extern SPECTATOR_LIB_MODE QString english;
extern SPECTATOR_LIB_MODE QString czech;

}

namespace DefaultSettings {

extern SPECTATOR_LIB_MODE QString server;
extern SPECTATOR_LIB_MODE int port;
extern SPECTATOR_LIB_MODE QString language;
extern SPECTATOR_LIB_MODE int refreshInterval;
extern SPECTATOR_LIB_MODE int popupTime;
extern SPECTATOR_LIB_MODE int mainTabIndex;
extern SPECTATOR_LIB_MODE QString tokenPath;
extern SPECTATOR_LIB_MODE QString backupPath;
extern SPECTATOR_LIB_MODE QString preferedInterface;
extern SPECTATOR_LIB_MODE bool systrayMode;
extern SPECTATOR_LIB_MODE bool dock;
extern SPECTATOR_LIB_MODE int inactiveLimit;
extern SPECTATOR_LIB_MODE QStringList activeNeighbourDevices;
extern SPECTATOR_LIB_MODE bool neighboursAdvancedView;

}


#endif
