/*
 * Spectator - a desktop app which communicates with router Turris
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spectator/consts.h"
#include "spectator/logging.h"
#include "spectator/settings_connection.h"


SpectatorConnectionSettings::SpectatorConnectionSettings() : QSettings() { }

void SpectatorConnectionSettings::load()
{
	log_settings.debug("Loading settings.");

	this->beginGroup("Connection");
	this->server = this->value("server", DefaultSettings::server).toString();
	log_settings.debug() << "Loading server:" << this->server;
	this->port = this->value("port", DefaultSettings::port).toInt();
	log_settings.debug() << "Loading port:" << this->port;
	this->tokenPath = this->value("token_path", DefaultSettings::tokenPath).toString();
	log_settings.debug() << "Loading token_path:" << this->tokenPath;
	this->endGroup();

}

void SpectatorConnectionSettings::store()
{
	log_settings.debug("Storing settings.");

	this->beginGroup("Connection");
	this->setValue("server", this->server);
	log_settings.debug() << "Storing server:" << this->server;
	this->setValue("port", this->port);
	log_settings.debug() << "Storing port:" << this->port;
	this->setValue("token_path", this->tokenPath);
	log_settings.debug() << "Storing token_path:" << this->tokenPath;
	this->endGroup();
}
