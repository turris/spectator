/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDir>

#include "spectator/consts.h"

namespace Languages {

QString english = "en";
QString czech = "cs";

}

namespace DefaultSettings {

QString server = "192.168.1.1";
int port = 6513;
QString language = Languages::english;
int refreshInterval = 5;  //in seconds
int popupTime = 5;  //in seconds
int mainTabIndex = 0;
QString tokenPath = QDir::homePath() + QDir::separator() + "spectator" + QDir::separator() + "client.pem";
QString backupPath = QDir::homePath() + QDir::separator() + "spectator" + QDir::separator() + "backup.tar.bz2";
QString preferedInterface = "eth2";
bool systrayMode = true;
bool dock = true;
int inactiveLimit = 300;
QStringList activeNeighbourDevices = QStringList() << "br-lan";
bool neighboursAdvancedView = false;

}
