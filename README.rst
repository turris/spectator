Spectator
=========

a system tray which downloads and displays various information from router Turris.

App is written in QT5.


HOWTO compile
=============
Note that QT 5.2.1 and above are required.

The compilation itself consists of two parts: library and gui

To compile both parts you can perform following commands::

    QT_SELECT=5 qmake "CONFIG+=staticlib"
    make


Required packages on Ubuntu 15.04
---------------------------------

* qt5-qmake
* qttranslations5-l10n
* qtbase5-dev-tools
* qtdeclarative5-dev


Extra debug output
------------------
To gain an extra debugging output (netconf communication) use::

    QT_SELECT=5 qmake spectator.pro "CONFIG+=staticlib" "QMAKE_CXXFLAGS+=-DMORE_DEBUG"

Windows console mode
--------------------
To compile sources and see program output, you need to compile app in console mode::

    QT_SELECT=5 qmake spectator.pro "CONFIG+=staticlib" "WIN_CONSOLE=1"


HOWTO run
=========
To run it you can need simply use::

    ./bin/turris-spectator


Windows packaging on Linux
==========================

Requirements:
-------------

* wine
* nsis (and all related packages)
* Qt 5.5.0 binaries (http://download.qt.io/official_releases/qt/5.5/5.5.0/qt-opensource-windows-x86-mingw492-5.5.0.exe)
* OpenSSL binaries (http://slproweb.com/download/Win32OpenSSL-1_0_2d.exe)

Steps:
------

1. Download exefiles of QT and OpenSSL
2. Install them into wine (**use default locations** and  **ignore errors during the installation**)::

    wine qt-opensource-windows-x86-mingw492-5.5.0.exe
    wine Win32OpenSSL-1_0_2d.exe

3. Compile sources::

    wine cmd < spectator-build.bat

4. Prepare required dll files::

    wine cmd < spectator-prepare-dlls.bat

5. Pack it all together using nsis::

    makensis nsis/spectator.nsi

6. Final install executable should be located in::

    nsis/spectator-{version}-install.exe


Systray mode issues
===================
Currently the systray mode doesn't work properly on some versions of ubuntu.
To run spectator with systray disabled::

    turris-spectator --no-systray

You can also try to force it to run in systray mode (might work for some versions)::

    turris-spectator --force-systray
