REM To use this script you need to have QT installed to a standard location C:\Qt\ and qt was to contain mingw32 compiler
REM
REM To run this script from wine use 'wine cmd < spectator-distclean.bat'
REM

SET Path=C:\Qt\Qt5.5.0\5.5\mingw492_32\bin;C:\Qt\Qt5.5.0\Tools\mingw492_32\bin;C:\Qt\Qt5.5.0\Tools\QtCreator\bin;C:32;C:
mingw32-make distclean
del gui\lang\qtbase_cs.qm
del gui\object_script.*
del library\object_script.*
del gui\turris-spectator_resource.rc
del library\turris-spectator_resource.rc
del nsis\spectator-*-install.exe
