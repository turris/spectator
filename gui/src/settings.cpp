/*
 * Spectator - a desktop app which communicates with router Turris
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <spectator/logging.h>
#include "settings.h"


SpectatorSettings::SpectatorSettings() : SpectatorGuiSettings(), SpectatorConnectionSettings()
{
	SpectatorConnectionSettings::beginGroup("Connection");
	// server is mandatory
	this->configPresent = !SpectatorConnectionSettings::value("server").isNull();
	SpectatorConnectionSettings::endGroup();
}

void SpectatorSettings::load()
{
	log_settings.debug("Loading settings.");

	if (this->configPresent)
		log_settings.debug("Previous configuration found.");
	else
		log_settings.debug("No previous configuration found. Using default settings.");

	SpectatorConnectionSettings::load();
	SpectatorGuiSettings::load();
}

void SpectatorSettings::store()
{
	log_settings.debug("Storing settings.");

	SpectatorConnectionSettings::store();
	SpectatorGuiSettings::store();
}
