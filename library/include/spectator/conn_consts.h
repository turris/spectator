/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_CONN_CONSTS_H
#define SPECTATOR_CONN_CONSTS_H

namespace Communicator {

enum ReportType {
	RT_INFO,
	RT_SUCCESS,
	RT_WARNING,
	RT_ERROR,
};

enum ConnectorStates {
	CONNECTOR_STARTED,
	CONNECTOR_CONFIGURED,
	CONNECTOR_READY,
	CONNECTOR_ERROR,
	CONNECTOR_TIMEOUT,
	CONNECTOR_ANY,
};

}

#endif
