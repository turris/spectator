/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECTATOR_SETTINGS_GUI_H
#define SPECTATOR_SETTINGS_GUI_H

#include <QSettings>
#include <QString>
#include <QHash>

struct SpectatorGuiSettings : public QSettings
{
	SpectatorGuiSettings();
	void store();
	void load();

	QString language;
	int refreshInterval;
	int popupTime;
	int mainTabIndex;
	QString backupPath;
	QString prefferedInterface;
	bool systrayMode;
	bool dock;
	QByteArray geometry;
	QStringList activeNeighbourDevices;
	int inactiveLimit;
	bool neighboursAdvancedView;
	QHash<QString, QString> neighboursMacToName;

	// options read from the command line
	bool forceSystrayMode;
	bool noSystrayMode;
};


#endif
