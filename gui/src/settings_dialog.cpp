/*
 * Spectator - a desktop app which communicates with router Turris
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <spectator/consts.h>

#include "settings_dialog.h"

#include "ui_settings.h"

#include <QFileDialog>

SettingsDialog::SettingsDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::SettingsDialog)
{
	this->ui->setupUi(this);
	this->setFixedSize(this->size());
	this->ui->languageComboBox->addItem(tr("Czech"), Languages::czech);
	this->ui->languageComboBox->addItem(tr("English"), Languages::english);
}

SettingsDialog::~SettingsDialog()
{
	delete this->ui;
}

void SettingsDialog::displayTokenPathBrowseDialog()
{
	const QString &dirname = QFileInfo(this->ui->tokenPathEdit->text()).absoluteDir().absolutePath();
	const QString &res = QFileDialog::getOpenFileName(this, tr("token file path"), dirname, tr("Tokens (*.pem);;All files (*.*)"));
	if (!res.isNull()) {
		this->ui->tokenPathEdit->setText(res);
		this->ui->tokenPathEdit->setToolTip(res);
	}
}

void SettingsDialog::loadSettings(const SpectatorSettings &settings)
{
	this->ui->popupTimeSpinBox->setValue(settings.popupTime);
	this->ui->serverEdit->setText(settings.server);
	this->ui->refreshIntervalSpinBox->setValue(settings.refreshInterval);
	this->ui->portSpinBox->setValue(settings.port);
	int language_idx = this->ui->languageComboBox->findData(settings.language);
	if (language_idx < 0) {  // laguage not found -> use default
		language_idx = this->ui->languageComboBox->findData(DefaultSettings::language);
	}
	this->ui->languageComboBox->setCurrentIndex(language_idx);
	this->ui->tokenPathEdit->setText(settings.tokenPath);
	this->ui->tokenPathEdit->setToolTip(settings.tokenPath);
	this->ui->systrayModeCheckBox->setChecked(settings.systrayMode);
	this->ui->dockCheckBox->setChecked(settings.dock);
}

void SettingsDialog::updateSettings(SpectatorSettings &settings)
{
	settings.popupTime = this->ui->popupTimeSpinBox->value();
	settings.server = this->ui->serverEdit->text();
	settings.refreshInterval = this->ui->refreshIntervalSpinBox->value();
	settings.language = this->ui->languageComboBox->currentData().toString();
	settings.port = this->ui->portSpinBox->value();
	settings.tokenPath = this->ui->tokenPathEdit->text();
	settings.systrayMode = this->ui->systrayModeCheckBox->isChecked();
	settings.dock = this->ui->dockCheckBox->isChecked();
}
