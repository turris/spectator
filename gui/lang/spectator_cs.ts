<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ" sourcelanguage="en_US">
<context>
    <name>Chats::Base</name>
    <message>
        <location filename="../src/chat.cpp" line="58"/>
        <source>Failed to parse obtained data.</source>
        <translation>Nezdařilo se získat naparsovaná data.</translation>
    </message>
    <message>
        <location filename="../src/chat.cpp" line="69"/>
        <source>Failed to parse RPC reply message.</source>
        <translation>Nepodařilo se naparsovat zprávu odpovědi.</translation>
    </message>
    <message>
        <location filename="../src/chat.cpp" line="75"/>
        <source>RPC message-id mismatch.</source>
        <translation>ID RPC zprávy nesouhlasí.</translation>
    </message>
    <message>
        <location filename="../src/chat.cpp" line="90"/>
        <source>Incorrect RPC response.</source>
        <translation>Špatná odpověď RPC.</translation>
    </message>
</context>
<context>
    <name>Chats::GetNethist</name>
    <message>
        <location filename="../src/chat.cpp" line="442"/>
        <source>Incorrect Nethist XML format.</source>
        <translation>Špatný formát XML s daty pro graf.</translation>
    </message>
</context>
<context>
    <name>Chats::GetNotifications</name>
    <message>
        <location filename="../src/chat.cpp" line="304"/>
        <source>Notifications were downloaded.</source>
        <translation>Notifikace byly staženy.</translation>
    </message>
</context>
<context>
    <name>Chats::GetStats</name>
    <message>
        <location filename="../src/chat.cpp" line="671"/>
        <source>Incorrect Stat XML format.</source>
        <translation>Nesprávný formát XML pro Stats.</translation>
    </message>
</context>
<context>
    <name>Chats::GetUpdaterData</name>
    <message>
        <location filename="../src/chat.cpp" line="814"/>
        <source>Incorrect updater XML format.</source>
        <translation>Nesprávný formát XML pro updater.</translation>
    </message>
    <message>
        <location filename="../src/chat.cpp" line="870"/>
        <source>Incorrect Stat XML format.</source>
        <translation>Nesprávný formát Stat XML.</translation>
    </message>
</context>
<context>
    <name>Chats::Initial</name>
    <message>
        <location filename="../src/chat.cpp" line="210"/>
        <source>An initial message received.</source>
        <translation>Úvodní zpráva přijata.</translation>
    </message>
</context>
<context>
    <name>Chats::RestoreBackup</name>
    <message>
        <location filename="../src/chat.cpp" line="617"/>
        <source>Failed to restore the backup.</source>
        <translation>Nepodařilo se obnovit systém ze zálohy.</translation>
    </message>
</context>
<context>
    <name>Connector</name>
    <message>
        <location filename="../src/communicator.cpp" line="73"/>
        <source>Failed to open the token file.</source>
        <translation>Nepodařilo se otevřít soubor s tokenem.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="79"/>
        <source>Failed to read the token file.</source>
        <translation>Nepodařilo se přečíst soubor s tokenem.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="90"/>
        <source>Failed to load client certificate.</source>
        <translation>Nepodařilo se nahrát klientský certifikát.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="99"/>
        <source>Failed to load client key.</source>
        <translation>Nepodařilo se nahrát klientský klíč.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="106"/>
        <source>Failed to load CA certificate.</source>
        <translation>Nepodařilo se nahrát CA certifikát.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="168"/>
        <location filename="../src/communicator.cpp" line="219"/>
        <source>Incorrect communication state.</source>
        <translation>Špatný stav komunikace.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="243"/>
        <source>Socket error occured.</source>
        <translation>Nastala chyba připojení.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="251"/>
        <source>SSL error occured.</source>
        <translation>Nastala chyba SSL.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="285"/>
        <source>Disconnected from the router.</source>
        <translation>Odpojeno od routeru.</translation>
    </message>
    <message>
        <location filename="../src/communicator.cpp" line="307"/>
        <source>Connected to the router.</source>
        <translation>Připojeno k routeru.</translation>
    </message>
</context>
<context>
    <name>InterfaceWidget</name>
    <message>
        <location filename="../src/custom_widgets.cpp" line="383"/>
        <source>Device:</source>
        <translation>Zařízení:</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="385"/>
        <source>TX:</source>
        <translation>TX:</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="384"/>
        <source>RX:</source>
        <translation>RX:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.ui" line="20"/>
        <location filename="../ui/mainwindow.ui" line="1733"/>
        <source>Spectator</source>
        <translation>Spectator</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="65"/>
        <location filename="../ui/mainwindow.ui" line="1750"/>
        <source>Notifications</source>
        <translation>Notifikace</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="113"/>
        <source>No notifications to display.</source>
        <translation>Žádné notifikace k zobrazení.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="128"/>
        <location filename="../ui/mainwindow.ui" line="1755"/>
        <source>Statistics</source>
        <translation>Statistiky</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="204"/>
        <source>Network</source>
        <translation>Síť</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="707"/>
        <source>Memory</source>
        <translation>Místo v paměti</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="490"/>
        <location filename="../ui/mainwindow.ui" line="653"/>
        <source>Total:</source>
        <translation>Celkem:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="368"/>
        <source>Temperature</source>
        <translation>Teplota</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="732"/>
        <source>NAND</source>
        <translation>Místo v NAND</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="137"/>
        <source>Load</source>
        <translation>Zatížení</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="743"/>
        <location filename="../ui/mainwindow.ui" line="1760"/>
        <source>Device</source>
        <translation>Zařízení</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="757"/>
        <location filename="../ui/mainwindow.ui" line="1395"/>
        <source>Control</source>
        <translation>Ovládání</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="993"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1006"/>
        <location filename="../ui/mainwindow.ui" line="1038"/>
        <location filename="../ui/mainwindow.ui" line="1070"/>
        <location filename="../ui/mainwindow.ui" line="1099"/>
        <location filename="../ui/mainwindow.ui" line="1128"/>
        <location filename="../ui/mainwindow.ui" line="1157"/>
        <location filename="../ui/mainwindow.ui" line="1234"/>
        <location filename="../ui/mainwindow.ui" line="1263"/>
        <location filename="../ui/mainwindow.ui" line="1489"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1025"/>
        <source>Serial:</source>
        <translation>Serial:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1057"/>
        <source>Kernel:</source>
        <translation>Jádro:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1089"/>
        <source>Version:</source>
        <translation>Verze:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1147"/>
        <source>Uptime:</source>
        <translation>Uptime:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1179"/>
        <source>Data collecting</source>
        <translation>Sběr dat</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1224"/>
        <source>Ucollect:</source>
        <translation>Ucollect:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1253"/>
        <source>Firewall:</source>
        <translation>Firewall:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1470"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1628"/>
        <source>Inactivity limit</source>
        <translation>Limit neaktivity</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1775"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1780"/>
        <source>About</source>
        <translation>O programu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1785"/>
        <source>Quit</source>
        <translation>Konec</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1118"/>
        <source>Hostname:</source>
        <translation>Hostname:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="264"/>
        <source>Board:</source>
        <translation>Deska:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="286"/>
        <source>CPU:</source>
        <translation>Procesor:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="408"/>
        <location filename="../ui/mainwindow.ui" line="548"/>
        <source>Free:</source>
        <translation>Volné:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="443"/>
        <location filename="../ui/mainwindow.ui" line="583"/>
        <source>Used:</source>
        <translation>Využité:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="618"/>
        <source>Cached:</source>
        <translation>Cache:</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1293"/>
        <source>Addresses of Interfaces</source>
        <translation>Adresy rozhraní</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1505"/>
        <source>Run updater</source>
        <translation>Spusť updater</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1554"/>
        <source>User lists</source>
        <translation>Uživatelské seznamy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1571"/>
        <location filename="../ui/mainwindow.ui" line="1770"/>
        <source>Neighbours</source>
        <translation>Sousedé</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1585"/>
        <source>Interfaces</source>
        <translation>Rozhraní</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1605"/>
        <source>Advanced view</source>
        <translation>Zobraz detaily</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1625"/>
        <location filename="../ui/mainwindow.ui" line="1635"/>
        <source>In seconds</source>
        <translation>V sekundách</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1671"/>
        <source>No neighbours to display.</source>
        <translation>Žádní sousedi k zobrazení.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="800"/>
        <source>Reboot</source>
        <translation>Restartuj</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="824"/>
        <source>Root password</source>
        <translation>Rootovské heslo</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="863"/>
        <source>Set Password</source>
        <translation>Nastav Heslo</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="883"/>
        <source>Browse</source>
        <translation>Procházet</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="890"/>
        <source>Backup</source>
        <translation>Záloha</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="902"/>
        <source>Restore</source>
        <translation>Obnov</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="909"/>
        <source>Store</source>
        <translation>Ulož</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="944"/>
        <source>System</source>
        <translation>Systém</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1381"/>
        <location filename="../ui/mainwindow.ui" line="1765"/>
        <source>Updater</source>
        <translation>Updater</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="168"/>
        <source>Load:</source>
        <translation>Zatížení:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="226"/>
        <source>Running in system tray mode</source>
        <translation>Běží v notifikační oblasti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="226"/>
        <source>Spectator is available in the system tray.</source>
        <translation>Spectator je přístupný z notifikační oblasti.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="380"/>
        <source>About Spectator</source>
        <translation>O programu Spectator</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="380"/>
        <source>&lt;h1&gt;Spectator&lt;/h1&gt;Version: </source>
        <translation>&lt;h1&gt;Spectator&lt;/h1&gt;Verze: </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="399"/>
        <source>backup file path</source>
        <translation>cesta k záloze</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="399"/>
        <source>Backups (*.tar.bz2);;All files (*.*)</source>
        <translation>Zálohy (*.tar.bz2);;Všechny soubory (*.*)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="810"/>
        <location filename="../src/mainwindow.cpp" line="811"/>
        <source>%1 (%2 second ago)</source>
        <translation>%1 (před %2 sekundami)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="968"/>
        <source>Neighbour list has changed.</source>
        <translation>Seznam sousedů se změnil.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="382"/>
        <source>&lt;p&gt;&lt;center&gt;Application which communicates with the Turris router using &lt;br/&gt; &lt;b&gt;NETCONF protocol&lt;/b&gt;&lt;/center&gt;&lt;/p&gt;&lt;b&gt;Uses:&lt;/b&gt;</source>
        <translation>&lt;p&gt;&lt;center&gt;Aplikace, která komunikuje s routerem Turris pomocí &lt;br/&gt; &lt;b&gt;protokolu NETCONF&lt;/b&gt;&lt;/center&gt;&lt;/p&gt;&lt;b&gt;Používá:&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="648"/>
        <source>Notifications lists updated</source>
        <translation>Seznam notifikací aktualizován</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="648"/>
        <source>Number of new notifications: </source>
        <translation>Počet nových notifikací: </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="958"/>
        <source>discovered: </source>
        <translation>objeveni: </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="962"/>
        <source>disappeared: </source>
        <translation>zmizeli: </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1213"/>
        <source>Reboot the router?</source>
        <translation>Restartovat router?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1238"/>
        <source>password mismatch</source>
        <translation>heslo není stejné</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1214"/>
        <source>Do you really want to reboot the router? Note that all current notifications will be discarded afterwards.</source>
        <translation>Opravdu chcete restartovat router? Všechny současné notifikace budou zahozeny.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1238"/>
        <source>Password mismatch. Please write the same password into both fields.</source>
        <translation>Heslo není stejné. Napište prosím stejné heslo do obou políček.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1243"/>
        <source>The password is too short. Please use at least &lt;b&gt;6 characters&lt;/b&gt;.</source>
        <translation>Heslo je příliš krátké. Použijte prosím aspoň &lt;b&gt;6 znaků&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1243"/>
        <source>short password</source>
        <translation>krátké heslo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1275"/>
        <source>failed to open</source>
        <translation>nepodařilo se otevřít</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1275"/>
        <source>Failed to open the backup file.</source>
        <translation>Nepodařilo se otevřít soubor se zálohou.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1338"/>
        <source>Disconnected.</source>
        <translation>Odpojeno.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1369"/>
        <source>Trying to reconnect.</source>
        <translation>Snažím se znovu připojit.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1449"/>
        <source>Already running.</source>
        <translation>Již běží.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1449"/>
        <source>to run more instances of spectator use &apos;-m&apos; parameter</source>
        <translation>pro běh více instancí spectatora použijte parametr &apos;-m&apos;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1463"/>
        <source>The password has been set successfully.</source>
        <translation>Heslo bylo úspěšně nastaveno.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1468"/>
        <source>Setting password failed.</source>
        <translation>Nastavení hesla selhalo.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1476"/>
        <source>The updater check has been triggered.</source>
        <translation>Kontrola updateru byla spuštěna.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1479"/>
        <source>Failed to trigger the updater check.</source>
        <translation>Nepodařilo se pustit kontrolu updateru.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1487"/>
        <source>The user lists of updater were successfully set.</source>
        <translation>Uživatelské seznamy updateru byly úspěšně nastaveny.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1490"/>
        <source>Failed to set the user lists of updater.</source>
        <translation>Nepodařilo se nastavit uživatelské seznamy updateru.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1503"/>
        <source>Overwrite file</source>
        <translation>Přepsat soubor</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1510"/>
        <source>file type error</source>
        <translation>špatný typ souboru</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1510"/>
        <source>&apos;%1&apos; is not a valid path to a file.</source>
        <translation>&apos;%1&apos; není validní cesta k souboru.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1519"/>
        <source>Backup stored.</source>
        <translation>Záloha uložena.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1522"/>
        <source>Failed to save backup file.</source>
        <translation>Nepodařilo se uložit soubor se zálohou.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1593"/>
        <source>Unknown</source>
        <translation>Neznámé</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1537"/>
        <source>Backup restored.</source>
        <translation>Záloha obnovena.</translation>
    </message>
</context>
<context>
    <name>NeighbourWidget</name>
    <message>
        <location filename="../src/custom_widgets.cpp" line="640"/>
        <source>with alias</source>
        <translation>s aliasem</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="736"/>
        <location filename="../src/custom_widgets.cpp" line="794"/>
        <location filename="../src/custom_widgets.cpp" line="820"/>
        <source>update alias</source>
        <translation>změn alias</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="803"/>
        <source>apply alias</source>
        <translation>použij alias</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="636"/>
        <source>on</source>
        <translation>na</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="635"/>
        <source>MAC address</source>
        <translation>MAC adresa</translation>
    </message>
</context>
<context>
    <name>NotificationWidget</name>
    <message>
        <location filename="../src/custom_widgets.cpp" line="166"/>
        <source>News</source>
        <translation>Novinky</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="167"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="168"/>
        <source>Restart</source>
        <translation>Restart</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="169"/>
        <source>Update</source>
        <translation>Aktualizace</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="170"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../ui/settings.ui" line="23"/>
        <source>Spectator - Settings</source>
        <translation>Spectator - Nastavení</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="33"/>
        <source>Connection</source>
        <translation>Spojení</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="41"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="51"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="78"/>
        <source>Refresh interval (s)</source>
        <translation>Interval obnovení (s)</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="102"/>
        <source>Token path</source>
        <translation>Cesta k tokenu</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="166"/>
        <source>Popup duration</source>
        <translation>Délka upozornění</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="198"/>
        <source>Systray mode</source>
        <translatorcomment>Použij notifikační oblast</translatorcomment>
        <translation>Systray mód</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="219"/>
        <source>Dock</source>
        <translation>Dock</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="116"/>
        <source>Browse</source>
        <translation>Procházet</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="128"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="148"/>
        <source>Apply</source>
        <translation>Použít</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="158"/>
        <source>GUI</source>
        <translation>Grafické rozhraní</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="184"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../src/settings_dialog.cpp" line="31"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../src/settings_dialog.cpp" line="32"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location filename="../src/settings_dialog.cpp" line="43"/>
        <source>token file path</source>
        <translation>cesta k tokenu</translation>
    </message>
    <message>
        <location filename="../src/settings_dialog.cpp" line="43"/>
        <source>Tokens (*.pem);;All files (*.*)</source>
        <translation>Tokeny (*.pem);;Všechny soubory (*.*)</translation>
    </message>
</context>
<context>
    <name>UpdaterActivitiesWidget</name>
    <message>
        <location filename="../src/custom_widgets.cpp" line="587"/>
        <source>deleted</source>
        <translation>odstraněn</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="590"/>
        <source>downloaded</source>
        <translation>stažen</translation>
    </message>
    <message>
        <location filename="../src/custom_widgets.cpp" line="593"/>
        <source>installed</source>
        <translation>nainstalován</translation>
    </message>
</context>
<context>
    <name>UpdaterUserListsWidget</name>
    <message>
        <location filename="../src/custom_widgets.cpp" line="413"/>
        <source>Set</source>
        <translation>Nastav</translation>
    </message>
</context>
</TS>
