QT += core network xml
win32 {
	CONFIG += skip_target_version_ext
}

TARGET = turris-spectator

VERSION="$$cat(../VERSION)"
DEFINES += "SPECTATOR_VERSION=\\\"$${VERSION}\\\""
message("Spectator lib version $${VERSION}")
TEMPLATE = lib

DEFINES += SPECTATOR_LIBRARY

APP_DIR = "."
SRC_DIR = "$${APP_DIR}/src"
HDR_DIR = "$${APP_DIR}/include/spectator"

MOC_DIR = build
RCC_DIR = build
OBJECTS_DIR = build

DESTDIR = ../lib

INCLUDEPATH += "./$${APP_DIR}/include"
DEPENDPATH += . "./$${APP_DIR}/include"

SOURCES += \
	"$${SRC_DIR}"/logging.cpp \
	"$${SRC_DIR}"/settings_connection.cpp \
	"$${SRC_DIR}"/communicator.cpp \
	"$${SRC_DIR}"/consts.cpp \
	"$${SRC_DIR}"/chat.cpp \
	"$${SRC_DIR}"/chat_structs.cpp


HEADERS += \
	"$${HDR_DIR}"/logging.h \
	"$${HDR_DIR}"/consts.h \
	"$${HDR_DIR}"/settings_connection.h \
	"$${HDR_DIR}"/communicator.h \
	"$${HDR_DIR}"/chat.h \
	"$${HDR_DIR}"/conn_consts.h \
	"$${HDR_DIR}"/chat_structs.h


isEmpty(PREFIX) {
	unix {
		PREFIX = "/usr/local"
	}
	win32 {
		PREFIX = "C:\\spectator"
	}
}

target.path = "$${PREFIX}/lib"

INSTALLS += target

unix {
	includes.path = "$${PREFIX}/include/spectator"
	includes.files += "$${HDR_DIR}/*.h"
	INSTALLS += includes
}
