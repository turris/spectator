/*
 * Spectator - a desktop app which communicates with router Turris
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <spectator/logging.h>

#include "application.h"

#include <QLocalSocket>

#define LISTEN_SERVER_NAME "spectator_guard_listener_"
#define GUARD_MEMORY_KEY "spectator_status_mem_"
#define GUARD_LOCK_KEY "spectator_check_lock_"


SpectatorApplication::SpectatorApplication(int &argc, char **argv, const QString &username) :
	QApplication(argc, argv),
	proceed(true),
	guard(username),
	forceSystrayMode(false),
	noSystrayMode(false)
{
	// Parse command line
	QCommandLineParser parser;
	parser.setApplicationDescription("Spectator");
	parser.addHelpOption();
	parser.addVersionOption();

	// -s --force-systray
	const QCommandLineOption forceSystray(
		QStringList() << "s" << "force-systray",
		"force to use systay mode"
	);
	parser.addOption(forceSystray);

	// -n --no-systray
	const QCommandLineOption noSystray(
		QStringList() << "n" << "no-systray",
		"force to use systay mode"
	);
	parser.addOption(noSystray);

	// -m --multiple-instances
	const QCommandLineOption multipleInstances(
		QStringList() << "m" << "multiple-instances",
		"allow multiple instances of program to run simultaneously"
	);
	parser.addOption(multipleInstances);

	// -o --stdout-logging
	const QCommandLineOption consoleLogging(
		QStringList() << "o" << "stdout-logging",
		"print log messages to stdout"
	);
	parser.addOption(consoleLogging);

	// -f --file-logging <filename>
	const QCommandLineOption logFile(
		QStringList() << "f" << "log-file",
		"store log output into a <logfile>",
		"logfile"
	);
	parser.addOption(logFile);

	// -l --log-level WARNING,CRITICAL,FATAL,INFO,DEBUG
#if QT_VERSION > QT_VERSION_CHECK(5, 5, 0)
	QString levels = "WARNING,CRITICAL,FATAL,INFO,DEBUG";
#else
	QString levels = "WARNING,CRITICAL,FATAL,DEBUG";
#endif
	const QCommandLineOption logLevel(
		QStringList() << "l" << "log-level",
		"set log level show: " + levels,
		"level"
	);
	parser.addOption(logLevel);

	parser.process(*this);

	// init logging
	if (parser.isSet(logLevel)) {
		QString level = parser.value(logLevel).trimmed();
		if (level == LEVEL_CRIT) {
			SpectatorLogging::logLevel = QtCriticalMsg;
		} else if (level == LEVEL_DEBUG) {
			SpectatorLogging::logLevel = QtDebugMsg;
#if QT_VERSION > QT_VERSION_CHECK(5, 5, 0)
		} else if (level == LEVEL_INFO) {
			SpectatorLogging::logLevel = QtInfoMsg;
#endif
		} else if (level == LEVEL_FATAL) {
			SpectatorLogging::logLevel = QtFatalMsg;
		} else if (level == LEVEL_WARN) {
			SpectatorLogging::logLevel = QtWarningMsg;
		} else {
			SpectatorLogging::logLevel = QtFatalMsg;
			log_application.fatal("Unknown level '" + level.toLocal8Bit() + "'");
		}
	}

	SpectatorLogging::stdout = parser.isSet(consoleLogging);
	qInstallMessageHandler(spectatorMessageHandler);
	if (parser.isSet(logFile)) {
		SpectatorLogging::spectatorLogFile.setFileName(parser.value(logFile));

		if (!SpectatorLogging::spectatorLogFile.open(QIODevice::Append | QIODevice::Text)) {
			log_application.fatal("Failed to open the log file.");
		}
	}

	this->forceSystrayMode = parser.isSet(forceSystray);
	this->noSystrayMode = parser.isSet(noSystray);
	// Start guarding if not -m is specified
	if (!parser.isSet(multipleInstances)) {
		this->proceed = this->guard.checkAndLock();
	}

	if (this->forceSystrayMode)
		log_settings.debug() << "Forcing to use systray mode";
	if (this->noSystrayMode)
		log_settings.debug() << "Forcing not to use systray mode";

}

int SpectatorApplication::exec()
{
	if(this->forceSystrayMode && this->noSystrayMode) {
		QTextStream out(stdout);
		out << "Can't use --no-systray and --force-systray options together!" << endl;
		return 1;
	}

	this->loadSettings();
	this->createTrayIcon();

	// Main window should intialized last
	this->createMainWindow();

	// Connect guard listener
	if (this->guard.locked) {
		QObject::connect(&this->guard.listener, SIGNAL(newConnection()), this->mainWindow.data(), SLOT(handleGuardListenerConnected()));
	}

	// toggle main window on click
	QObject::connect(this->trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this->mainWindow, SLOT(toggleWindow(QSystemTrayIcon::ActivationReason)));

	this->updateTranslations();
	return QApplication::exec();
}

void SpectatorApplication::createTrayIcon()
{
	// Create icon
	const QIcon icon(":/icons/img/trayicon-gray.png");

	// Create systemtray with the icon
	this->trayIcon = new SpectatorSystemTray(icon, this);

	// tooltip
	this->trayIcon->setToolTip("Spectator");

	// prepare refreshing timer
	this->trayIconTimer = new QTimer(this);
	this->trayIconTimer->setSingleShot(false);
	this->trayIconTimer->setInterval(5000);
	QObject::connect(this->trayIconTimer, SIGNAL(timeout()), this, SLOT(refreshTrayIcon()));
	this->trayIconTimer->start();
}

void SpectatorApplication::createMainWindow()
{
	this->mainWindow = new MainWindow(NULL, this->trayIcon);
	QObject::connect(this->mainWindow, SIGNAL(translationNeeded()), this, SLOT(updateTranslations()));

	// TODO share menu  in a nicer way
	this->trayIcon->setContextMenu(this->mainWindow->getMenu());
	this->mainWindow->useSettings(this->settings);

	// Update the translations before showing systray popup
	this->updateTranslations();
	if (this->mainWindow->isInSystrayMode()) {
		this->mainWindow->showRunningInSystrayPopup();
	}
}

void SpectatorApplication::loadSettings()
{
	settings.load();
	settings.forceSystrayMode = this->forceSystrayMode;
	settings.noSystrayMode = this->noSystrayMode;
}

void SpectatorApplication::updateTranslations()
{
	// Remove translators if needed
	if (this->qtTranslator) {
		delete this->qtTranslator;
	}
	if (this->spectatorTranslator) {
		delete this->spectatorTranslator;
	}

	// Manage translations
	this->qtTranslator = new QTranslator(this);
	log_settings.debug() << "Installing translator:" << "qtbase_" + settings.language;
	log_settings.debug() << "Translator installed:" << this->qtTranslator->load("qtbase_" + settings.language, ":/translations/lang");
	qApp->installTranslator(this->qtTranslator);

	this->spectatorTranslator = new QTranslator(this);
	log_settings.debug() << "Installing translator:" << "spectator_" + settings.language;
	log_settings.debug() << "Translator installed:" << this->spectatorTranslator->load("spectator_" + settings.language, ":/translations/lang");
	qApp->installTranslator(this->spectatorTranslator);

	const QString &locale = settings.language == "cs" ? "cs_CZ": "en_US";
	log_settings.debug() << "Setting locale: " << locale;
	QLocale::setDefault(QLocale(locale));
	this->mainWindow->setLocale(QLocale(locale));
}

void SpectatorApplication::refreshTrayIcon()
{
	// try to show systemtray icon
	// note that some environments might pass the testSystrayMode() after a few seconds
	// the start of the desktop environment
	if (this->settings.systrayMode && this->mainWindow->testSystrayMode()) {
		this->mainWindow->setSystrayMode(true);
	}

}

SpectatorApplication::~SpectatorApplication()
{
	// Close the logging file if it is open
	if (SpectatorLogging::spectatorLogFile.isOpen())
		SpectatorLogging::spectatorLogFile.close();

	if (this->spectatorTranslator)
		delete this->spectatorTranslator;
	if (this->qtTranslator)
		delete this->qtTranslator;
	if (this->trayIconTimer)
		delete this->trayIconTimer;
	if (this->mainWindow)
		delete this->mainWindow;
	if (this->trayIcon)
		delete this->trayIcon;
}

SpectatorGuard::SpectatorGuard(const QString& username) :
	listener(),
	locked(false),
	status(GUARD_MEMORY_KEY + username),
	checkLock(GUARD_LOCK_KEY + username, 1, QSystemSemaphore::Open),
	username(username)
{
}

bool SpectatorGuard::checkAndLock()
{
	log_application.debug() << "Guard for username" << this->username;
	log_application.debug() << "Trying to acquire guard lock." << this->checkLock.key();

	if (!this->checkLock.acquire())
		return false;
	log_application.debug() << "Guard lock obtained." << this->checkLock.key();

	// cleanup after crashed process
	{
		QSharedMemory(this->status.key()).attach();
	}

	bool result = this->status.create(1);
	if (result) {
		log_application.debug() << "Lock acquired. Guarding." << this->status.nativeKey();
	} else {
		log_application.critical() << "Failed to obtain the process guard." << this->status.nativeKey();
		log_application.debug() << this->status.errorString();
	}

	if (!this->checkLock.release())
			return false;

	log_application.debug() << "Guard lock released." << this->checkLock.key();

	const QString &serverName = this->getServerName();
	if (result) {
		this->locked = true;

		// cleanup after crashed process
		QLocalServer::removeServer(serverName);

		if (this->listener.listen(serverName)) {
			log_application.debug() << "Listening." << this->listener.serverName();
		} else {
			log_application.warning() << "Failed to listen." << serverName;
			log_application.warning() << this->listener.errorString();
		}
	} else {
		if (this->status.error() == QSharedMemory::AlreadyExists) {
			// Try to notify running instance
			QLocalSocket socket;
			// connecting should be sufficient for now
			// no other info is passed to the running client
			log_application.debug() << "Notifying running instance." << serverName;
			socket.connectToServer(serverName, QIODevice::ReadWrite);
			socket.waitForConnected(100);
		}
	}

	return result;
}

SpectatorGuard::~SpectatorGuard()
{
	if (this->status.isAttached()) {
		log_application.debug() << "Stopping guarding." << this->status.nativeKey();
	}
	if (this->listener.isListening()) {
		log_application.debug() << "Stopping listening." << this->listener.serverName();
	}
	// Release everything just to be sure.
	this->status.detach();
	this->checkLock.release();
	this->listener.close();
}

const QString SpectatorGuard::getServerName()
{
	return LISTEN_SERVER_NAME + this->username;
}
