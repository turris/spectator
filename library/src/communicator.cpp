/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spectator/communicator.h"
#include "spectator/logging.h"

#include <QSslConfiguration>
#include <QFile>
#include <QSslKey>

#define MSG_TERMINATOR "]]>]]>"

Connector::Connector(const QString &server, int port, const QString &tokenPath) :
		socket(this),
		server(server),
		port(port),
		tokenPath(tokenPath),
		state(Communicator::CONNECTOR_STARTED) {}

void Connector::initiateConnection()
{
	// load configuration
	if (this->setConfiguration(this->tokenPath)) {
		this->state = Communicator::CONNECTOR_CONFIGURED;
	} else {
		this->state = Communicator::CONNECTOR_ERROR;
		return;
	}


	this->socket.setSslConfiguration(this->configuration);
	this->socket.setSocketOption(QAbstractSocket::KeepAliveOption, 1);

	//this->socket->setCiphers("HIGH:!LOW:!MEDIUM:!SSLv2:!aNULL:!eNULL:!DES:!3DES:!AES128:!CAMELLIA128");

	// connect signals
	QObject::connect(&this->socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(handleSocketError(QAbstractSocket::SocketError)));
	QObject::connect(&this->socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(handleSocketStateChange(QAbstractSocket::SocketState)));
	QObject::connect(&this->socket, SIGNAL(connected()), this, SLOT(handleSocketConnected()));
	QObject::connect(&this->socket, SIGNAL(disconnected()), this, SLOT(handleSocketDisconnected()));
	QObject::connect(&this->socket, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(handleSslErrors(QList<QSslError>)));
	QObject::connect(&this->socket, SIGNAL(readyRead()), this, SLOT(handleReadReady()));
	QObject::connect(&this->socket, SIGNAL(encrypted()), this, SLOT(handleEncrypted()));
	QObject::connect(&this->chatTimer, SIGNAL(timeout()), this, SLOT(handleChatTimeOut()));

	if (Communicator::CONNECTOR_CONFIGURED == this->state) {
		log_connection.debug() << "Starting to connect...";
		this->socket.connectToHostEncrypted(server, port);
	}
}

bool Connector::setConfiguration(const QString &tokenPath)
{
	// open the file and read the certificates and a key
	QFile tokenFile(tokenPath);
	if(!tokenFile.open(QIODevice::ReadOnly))
	{
		emit this->report(Communicator::RT_ERROR, tr("Failed to open the token file."));
		log_connection.critical() << "Failed to open the token file:" << tokenFile.errorString();
		return false;
	}
	const QList<QSslCertificate> &certs = QSslCertificate::fromDevice(&tokenFile);
	if (certs.size() != 2) {
		emit this->report(Communicator::RT_ERROR, tr("Failed to read the token file."));
		log_connection.critical() << "Failed to read the token file. The file has an incorrect format.";
		return false;
	}
	const QSslCertificate &clientCert = certs[0];
	const QSslCertificate &CACert = certs[1];
	tokenFile.seek(0);
	const QSslKey clientKey(&tokenFile, QSsl::Rsa);
	tokenFile.close();

	if (clientCert.isNull()) {
		emit this->report(Communicator::RT_ERROR, tr("Failed to load client certificate."));
		log_connection.critical() << "Failed to load client certificate.";
		return false;
	}
	log_connection.debug() << "Client certificate: CN"
		<< clientCert.subjectInfo(QSslCertificate::CommonName).at(0)
		<< "Expires" << clientCert.expiryDate().toString("yyyy-MM-dd");

	if (clientKey.isNull()) {
		emit this->report(Communicator::RT_ERROR, tr("Failed to load client key."));
		log_connection.critical() << "Failed to load client key.";
		return false;
	}
	log_connection.debug() << "Client key: size" << clientKey.length();

	if (CACert.isNull()) {
		emit this->report(Communicator::RT_ERROR, tr("Failed to load CA certificate."));
		log_connection.critical() << "Failed to load CA certificate.";
		return false;
	}
	log_connection.debug() << "CA certificate: CN"
		<< CACert.subjectInfo(QSslCertificate::CommonName).at(0)
		<< "Expires" << clientCert.expiryDate().toString("yyyy-MM-dd");

	// set the config parameters
	this->configuration.setProtocol(QSsl::SecureProtocols);
	this->configuration.setLocalCertificate(clientCert);
	this->configuration.setPrivateKey(clientKey);
	this->configuration.setCaCertificates(QList<QSslCertificate>() << CACert);

	return true;
}

void Connector::connectChat(QSharedPointer<Chats::Base> &chat)
{
	QObject::connect(this, SIGNAL(readMessage(const QString &)), chat.data(), SLOT(readMessage(const QString &)));
	QObject::connect(chat.data(), SIGNAL(finished(bool)), this, SLOT(handleChatFinished(bool)));
	QObject::connect(chat.data(), SIGNAL(writeMessage(const QString &)), this, SLOT(handleWriteMessage(const QString &)));
	QObject::connect(chat.data(), SIGNAL(report(Communicator::ReportType, const QString &)), this, SLOT(handleReport(Communicator::ReportType, const QString &)));
	QObject::connect(chat.data(), SIGNAL(communicationStatusUpdate(Communicator::ConnectorStates)), this, SLOT(handleStateChange(Communicator::ConnectorStates)));
	QObject::connect(chat.data(), SIGNAL(netconfErrorsOccured(const QList<NetconfError> &)), this, SLOT(handleNetconfErrors(const QList<NetconfError> &)));
}

bool Connector::startChat(QSharedPointer<Chats::Base> &chat, bool requireCapabilities)
{
	if (!this->capabilityCheck(chat)) {
		if (requireCapabilities) {
			log_connection.debug() << "Chat won't be started due to it lacks some of the requirements.";
			return false;
		} else {
			log_connection.debug() << "Chat will be started although it lacks some of the requirements.";
		}
	}
	log_connection.debug() << "Starting Chat" << chat->getName();
	this->connectChat(chat);
	this->resetChatTimer();
	chat->talk();
	return true;
}

void Connector::performInitialChat(QSharedPointer<Chats::Initial> &chat)
{
	QObject::connect(chat.data(), SIGNAL(exportCapabilities(const QList<Capability> &)), this, SLOT(handleStoreCapabilities(const QList<Capability> &)));
	QSharedPointer<Chats::Base> spChat = chat;
	this->performChat(spChat, Communicator::CONNECTOR_ANY, false);
}

void Connector::performChat(QSharedPointer<Chats::Base> &chat, Communicator::ConnectorStates requiredState, bool requireCapabilities)
{
	if (this->chatQueue.empty()) {
		if (requiredState == Communicator::CONNECTOR_ANY || this->state == requiredState) {
			this->chatQueue.append(QueuedChat(chat, requiredState, requireCapabilities));
			// start immediately
			if (!this->startChat(chat, requireCapabilities)) {
				// chat fails to start it needs to be removed from the queue
				this->popChatQueue();
			}
		} else {
			emit this->report(Communicator::RT_ERROR, tr("Incorrect communication state."));
		}
	} else {
		log_connection.debug() << "Queued Chat" << chat->getName();
		this->chatQueue.push_back(QueuedChat(chat, requiredState, requireCapabilities));
	}
}

void Connector::resetChatTimer()
{
	this->chatTimer.stop();
	this->chatTimer.start(CHAT_TIMEOUT);
}

void Connector::popChatQueue() {
	if (!this->chatQueue.isEmpty()) {
		this->chatQueue.pop_front();
	}
}

void Connector::handleWriteMessage(const QString &msg)
{
	QString res = msg;
#ifdef MORE_DEBUG
	log_connection.debug() << "Sending a message:\n" << res;
#else
	log_connection.debug() << "Sending a message:" << res.length() << "characters";
#endif
	res += MSG_TERMINATOR "\n";
	if (this->socket.state() == QAbstractSocket::ConnectedState && this->socket.isEncrypted()) {
		if (0 > this->socket.write(QByteArray(res.toStdString().c_str()))) {
			log_connection.critical() << "Failed to write into the socket.";
		}
	} else {
		log_connection.critical() << "Can't write message, socket not connected.";
	}
}

void Connector::startNextChat()
{
	while (!this->chatQueue.empty()) {
		if (this->state == chatQueue[0].state) {
			// start queued chat
			if (!this->startChat(this->chatQueue[0].chat, this->chatQueue[0].capabilityRequired)) {
				// chat fails to start it needs to be removed from the queue
				this->popChatQueue();
			} else {
				// chat successfully started
				break;
			}
		} else {
			emit this->report(Communicator::RT_ERROR, tr("Incorrect communication state."));
			this->popChatQueue();
		}
	}
}

void Connector::handleChatFinished(bool success)
{
	this->chatTimer.stop();
	log_connection.debug() << "Chat finished" << (success ? "successfully.": "unsuccessfully.");
	// Popup current chat
	this->popChatQueue();
	// Try to start a new one
	this->startNextChat();
}

Connector::~Connector()
{
	log_connection.debug() << "Closing connection...";
}

void Connector::handleSocketError(QAbstractSocket::SocketError error)
{
	Q_UNUSED(error)
	emit this->report(Communicator::RT_ERROR, tr("Socket error occured."));
	log_connection.critical() << "Socket Error:" << this->socket.errorString();
	this->state = Communicator::CONNECTOR_ERROR;
	emit this->disconnect();
}

void Connector::handleSslErrors(const QList<QSslError> &errors)
{
	emit this->report(Communicator::RT_ERROR, tr("SSL error occured."));
	log_connection.critical() << "SSL Errors:";
	for (QList<QSslError>::const_iterator i(errors.begin()); i != errors.end(); ++i) {
		log_connection.critical() << (int)i->error() << i->errorString();
		if (i->error() == QSslError::HostNameMismatch) {
			this->socket.ignoreSslErrors(QList<QSslError>() << *i);
			return;
		}
	}
	this->state = Communicator::CONNECTOR_ERROR;
	emit this->disconnect();
}

void Connector::handleSocketStateChange(QAbstractSocket::SocketState state)
{
	log_connection.debug() << "New socket state:" << state;
}

void Connector::handleStateChange(Communicator::ConnectorStates newState)
{
	log_connection.debug() << "New communicator state:" << newState;
	this->state = newState;
	if (Communicator::CONNECTOR_READY) {
		emit connected();
	}
}

void Connector::handleSocketConnected()
{
	log_connection.debug() << "Socket Connected.";
}

void Connector::handleSocketDisconnected()
{
	emit this->report(Communicator::RT_SUCCESS, tr("Disconnected from the router."));
	log_connection.debug() << "Socket Disconnected.";
}

void Connector::handleReadReady()
{
	this->readBuffer.append(this->socket.readAll());
	if (this->readBuffer.contains(MSG_TERMINATOR)) {
		QString msg(this->readBuffer);
		msg.replace(QRegExp(QRegExp::escape(MSG_TERMINATOR)), "");
#ifdef MORE_DEBUG
		log_connection.debug() << "Recived a message:\n" << msg;
#else
		log_connection.debug() << "Recived a message:" << msg.length() << "characters";
#endif
		emit this->readMessage(msg);
		this->readBuffer.clear();
	}
}

void Connector::handleEncrypted()
{
	emit this->report(Communicator::RT_SUCCESS, tr("Connected to the router."));
	log_connection.debug() << "Socket Encrypted.";
}

void Connector::handleReport(Communicator::ReportType type, const QString &msg)
{
	emit this->report(type, msg);
}

void Connector::handleChatTimeOut()
{
	if (Communicator::CONNECTOR_TIMEOUT != this->state) {
		this->state = Communicator::CONNECTOR_TIMEOUT;
		emit this->disconnected();
	}
}

void Connector::handleStoreCapabilities(const QList<Capability> &capabilities)
{
	this->capabilities = capabilities;
	foreach (const Capability &capability, this->capabilities) {
		log_connection.debug() << "capability obtained" << capability.base << capability.revision;
	}
}

void Connector::handleNetconfErrors(const QList<NetconfError> &errors)
{
	foreach (const NetconfError &error, errors) {
		QString msg = "Netconf error occured:\n";
		msg += "type: " + error.type + "\n";
		msg += "tag: " + error.tag + "\n";
		msg += "severity: " + error.serverity + "\n";
		msg += "app-tag: " + error.appTag + "\n";
		msg += "path: " + error.path + "\n";
		msg += "message: " + error.message + "\n";
		msg += "info: \n";
		foreach (const NetconfErrorInfoElement &iError, error.info) {
			msg += "  " + iError.name + ": " + iError.content + "\n";
		}
		log_connection.critical() << msg.toStdString().c_str();
	}
}

bool Connector::ready()
{
	return Communicator::CONNECTOR_READY == this->state;
}

bool Connector::capabilityCheck(QSharedPointer<Chats::Base> &chat)
{
	bool result = true;
	// Check whether the connector has all required capabilities
	foreach (const Capability &requiredCapability, chat->getRequiredCapabilities()) {
		bool has = false;
		foreach (const Capability &ownedCapability, this->capabilities) {
			if (ownedCapability.covers(requiredCapability)) {
				has = true;
				break;
			}
		}
		if (has) {
			log_connection.debug() << "server has required capability" << requiredCapability.base << requiredCapability.revision << "for chat" << chat->getName();
		} else {
			log_connection.warning() << "server is missing capability" << requiredCapability.base << requiredCapability.revision << "for chat" << chat->getName();
			result = false;
		}
	}
	return result;
}
