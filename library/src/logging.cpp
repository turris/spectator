/*
 * Spectator - a desktop app which communicates with router Turris using NETCONF protocol
 * Copyright (C) 2016 CZ.NIC, z. s. p. o. <http://www.nic.cz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spectator/logging.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QTextStream>
#include <QHash>
#include <QSet>

namespace SpectatorLogging {

QtMsgType logLevel = QtWarningMsg;
QFile spectatorLogFile;
bool stdout = false;

}

void spectatorMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	static QHash<QtMsgType, QSet<QtMsgType> > levelCoverage;
	if (levelCoverage.isEmpty()) {
		levelCoverage[QtDebugMsg] = QSet<QtMsgType>() << QtFatalMsg << QtCriticalMsg << QtWarningMsg << QtDebugMsg;
		levelCoverage[QtWarningMsg] = QSet<QtMsgType>() << QtFatalMsg << QtCriticalMsg << QtWarningMsg;
		levelCoverage[QtCriticalMsg] = QSet<QtMsgType>() << QtFatalMsg << QtCriticalMsg;
		levelCoverage[QtFatalMsg] = QSet<QtMsgType>() << QtFatalMsg;
#if QT_VERSION > QT_VERSION_CHECK(5, 5, 0)
		levelCoverage[QtInfoMsg] = QSet<QtMsgType>() << QtFatalMsg << QtCriticalMsg << QtWarningMsg << QtInfoMsg;
		levelCoverage[QtDebugMsg] << QtInfoMsg;
#endif
	}

	if (!levelCoverage[SpectatorLogging::logLevel].contains(type)) {
		return;
	}

	QString pid = "[" + QString::number(QCoreApplication::applicationPid()) + "]";
	QString typeStr;
	switch (type) {
		case QtDebugMsg:
			typeStr = LEVEL_DEBUG;
			break;
#if QT_VERSION > QT_VERSION_CHECK(5, 5, 0)
		case QtInfoMsg:
			typeStr = LEVEL_INFO;
			break;
#endif
		case QtCriticalMsg:
			typeStr = LEVEL_CRIT;
			break;
		case QtWarningMsg:
			typeStr = LEVEL_WARN;
			break;
		case QtFatalMsg:
			typeStr = LEVEL_FATAL;
			break;
		default:
			break;
	}

	if (SpectatorLogging::spectatorLogFile.isWritable()) {
		QString time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
		QString fileMsg = QString("%1 %2 %3(%4): %5\n").arg(time, pid, context.category, typeStr, msg);
		SpectatorLogging::spectatorLogFile.write(fileMsg.toLocal8Bit());
		SpectatorLogging::spectatorLogFile.flush();
	}
	if (SpectatorLogging::stdout || type == QtFatalMsg) {
		QString outMsg = QString("%1(%2): %3\n").arg(context.category, typeStr, msg);
		QTextStream(stdout) << outMsg;
	}

	if (QtFatalMsg == type) {
		abort();
	}
}

Q_LOGGING_CATEGORY(spectatorConnection, "spectator.connection")
Q_LOGGING_CATEGORY(spectatorSettings, "spectator.settings")
Q_LOGGING_CATEGORY(spectatorNotifications, "spectator.notifications")
Q_LOGGING_CATEGORY(spectatorNethist, "spectator.nethist")
Q_LOGGING_CATEGORY(spectatorControl, "spectator.control")
Q_LOGGING_CATEGORY(spectatorStats, "spectator.stats")
Q_LOGGING_CATEGORY(spectatorUpdater, "spectator.updater")
Q_LOGGING_CATEGORY(spectatorNeighbours, "spectator.neighbours")
Q_LOGGING_CATEGORY(spectatorApplication, "spectator.application")
